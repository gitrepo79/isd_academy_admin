<?php

namespace App\Exports;

use App\SignUp;
use App\FormType;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SignAllUpsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection

    */
    use Exportable;
    protected $request;

    public function __construct($request)
    {
        if($request){
            $this->request = $request;
        }
    }
   

    public function collection()
    {
        $no = 0;
        $adminComment=array();
       
        $signUps = SignUp::all();
        
        foreach ($signUps as $signUp)
        {
        
        if(!empty($signUp->sport->name)){
         $sport=$signUp->sport->name;
        }else{
             $sport="None";
        }

        $parent_name=$signUp->user->name;
        $email=$signUp->user->email;
        $mobile=$signUp->user->mobile_no;
        $date_of_requirment=$signUp->date_of_requirment;
        $message=$signUp->message;
        if(!empty($signUp->player_id)){
         $player_name=$signUp->player->name; 
        }else{
        	$player_name="None";
        }
        if(!empty($signUp->player->dob)){
         $dob=$signUp->player->dob; 
        }
        else{
        	$dob="None";
        }

         if(!empty($signUp->form_type_id)){
          $form_type=FormType::where('id',$signUp->form_type_id)->pluck('name')->first();
        
        }
        
        else{
             $form_type="";
        }

        if(!empty($signUp->admin_comments)){
        $comments=json_decode($signUp->admin_comments,true);
        foreach($comments as $key => $comment){
         if($comment['id']==$signUp->id){
            $adminComment[]=$comment['comments'];

          }
          
        }
        }

        else{

          	$adminComment=null;
          
        }
      

         $output[] = [
                    ++$no,

                    $parent_name,
                    $email,
                    $mobile,
                    $player_name,
                    $dob,
                    $sport,
                    $date_of_requirment,
                    $message,
                    $adminComment,
                    $form_type,
                ];

        
        unset($adminComment);
        }
         
         
        return collect($output);
    }

     public function headings(): array
    {
        return [
            'SNo', 'Parent Name', 'Email', 'Contact #', 'Player Name', 'DOB','Sport', 'Date of Requirements', 'Message','Admin Comments', 'Form Type',
        ];
    }
}
