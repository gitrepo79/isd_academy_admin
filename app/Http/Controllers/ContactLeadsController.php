<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Sport;
use App\Inquiries;
use Auth;
use Carbon\Carbon;
use DB;

class ContactLeadsController extends Controller
{
    public function index()
    {
        $leads = DB::table('contact_leads')->orderBy('id', 'DESC')->paginate(15);

        // dd($leads);
        // $types=FormType::pluck('name','id');
        $sports = Sport::all();
        return view('admin.contact_leads.index',compact('leads'));
    }
}
