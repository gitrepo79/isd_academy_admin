<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exports\SignUpsExport;

use Maatwebsite\Excel\Facades\Excel;

use App\Sport;

use App\SignUp;
use App\User;
use App\Player;
use App\Program;
use Carbon\Carbon;
use App\Inquiries;

class HomeController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

//        $this->middleware('auth');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Contracts\Support\Renderable

     */

    public function index(Request $request)

    {
        $sports=Sport::where('is_active',STATUS_ENABLED)->pluck('name','id');
       return view('home',['sports'=>$sports]);

    }


    public function about(Request $request)

    {

        return view('pages.about');

    }
    
    public function terms(Request $request)

    {

        return view('pages.terms');

    }
    
    public function policy(Request $request)

    {

        return view('pages.policy');

    }

     public function thankYou($formType)
    {
       return view('pages.thank_you',['formType'=>$formType]);
    }



    public function footlab(Request $request)

    {
        //return view('pages.about');
        return view('pages.footlab');

    }

    public function isdAcademy(Request $request)

    {
        return view('pages.isd_academy');

    }


    public function dubaiFitnessChallenge(Request $request)

    {
        //return view('pages.about');
        return view('pages.dubai_fitness_challenge');

    }



    // Sports

    public function football(Request $request)

    {
        
        return view('pages.football');

    }

    public function footballVenue(Request $request)

    {
        
        return view('pages.football_venue');

    }



    public function tennis(Request $request)

    {

        return view('pages.tennis');

    }

    public function tennisCamp(Request $request)

    {

        return view('pages.tennis_camp');

    }

    public function tennisAdults(Request $request)

    {

        return view('pages.tennis_adults');

    }

    public function tennisFee(Request $request)

    {

        return view('pages.tennis_fees');

    }

    public function tennisSchedule(Request $request)

    {

        return view('pages.tennis_schedule');

    }

    public function tennisDetails(Request $request)

    {

        return view('pages.tennis_details');

    }

    public function tennisVenue(Request $request)

    {
        
        return view('pages.tennis_venue');

    }

    public function tennisProgram(Request $request)

    {
        $sport_id=Sport::where('name','Tennis')->pluck('id')->first();
        $sports=Sport::where('is_active',STATUS_ENABLED)->pluck('name','id');
         $programs=Program::where('sport_id',4)->where('is_active',STATUS_ENABLED)->pluck('name','id');
         $player_types=[1=>'Adult',2=>'child'];
        return view('programs.tennis_program',['sport_id'=>$sport_id,
            'sports'=>$sports,'programs'=>$programs,'player_types'=>$player_types]);

    }



    public function rugby(Request $request)

    {

        return view('pages.rugby');

    }



    public function padel(Request $request)

    {

        return view('pages.padel');

    }



    public function athletics(Request $request)

    {

        return view('pages.athletics');

    }

    public function athleticsCamp(Request $request)

    {

        return view('pages.athletics_camp');

    }

    public function athleticsVenue(Request $request)

    {
        
        return view('pages.athletics_venue');

    }

    public function athleticsProgram(Request $request)

    {
        $sport_id=Sport::where('name','Athletics')->pluck('id')->first();
        $sports=Sport::where('is_active',STATUS_ENABLED)->pluck('name','id');
         $programs=Program::where('sport_id',4)->where('is_active',STATUS_ENABLED)->pluck('name','id');
         $player_types=[1=>'Adult',2=>'child'];
        return view('programs.athletics_program',['sport_id'=>$sport_id, 'sports'=>$sports,'programs'=>$programs,'player_types'=>$player_types]);

    }


    public function isdAthletics(Request $request)

    {

        return view('pages.isd_athletics');

    }


    public function athleticsDetails(Request $request)

    {

        return view('pages.athletics_details');

    }

    public function athleticsFee(Request $request)

    {

        return view('pages.athletics_fees');

    }

    public function athleticsSchedule(Request $request)

    {

        return view('pages.athletics_schedule');

    }

    public function athleticsClassicMeets()

    {
       $events=[1=>'100 m',2=>'200 m',3=>'300 m',4=>'400 m',5=>'800 m',6=>'1500 m',7=>'3000 m'];
        $event_dates=['2021-03-26'=>"26th March",'2021-04-02'=>"2nd April"];
        $event_types=[1=>"1 event AED 60 ",2=>"2 events AED 90 "];
        return view('pages.athletics_classic_meets',['events'=>$events,
            'event_dates'=>$event_dates,'event_types'=>$event_types]);

    }

    public function eupepsia(Request $request)

    {

        return view('pages.eupepsia');

    }

    

    public function venueHire()

    {

       $sports=Sport::where('is_active',STATUS_ENABLED)->pluck('name','id');

       return view('venue_hire',['sports'=>$sports]);

    }



    public function academyRegistration()

    {

        $sports=Sport::where('is_active',STATUS_ENABLED)->pluck('name','id');
        $programs=Program::where('sport_id',4)->where('is_active',STATUS_ENABLED)->pluck('name','id');
        $player_types=[1=>'Adult',2=>'child'];
        return view('pages.academy_registration',['sports'=>$sports,'programs'=>$programs,
            'player_types'=>$player_types]);

    }
    
    



     /* Dashboard Paged */

    public function registration(Request $request)
    {
        $this->validateFormData($request);
        if(isset($request['g-recaptcha-response']) && !empty($request['g-recaptcha-response'])){
            
            $secret=env("NOCAPTCHA_SECRET");
           
            $verifyResponse =   file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$request['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            if($responseData->success){
                       
        if( $request->form_type_id==TYPE_VENUE){
         $date=date('Y-m-d',strtotime($request->date_of_requirement));
         
         $user=User::where('email',$request->email)->first();
         if(empty($user)){
         $user = new User();
         $user->name = $request->name;
         $user->email = $request->email;
         $user->mobile_no = $request->mobile;
         $user->is_active = STATUS_ENABLED;
         $user->save();
        
         }
        $user_id=$user->id;
        
        $signUp=new SignUp();
        $signUp->sport_id = $request->sport_id;
        $signUp->user_id = $user_id;
        $signUp->form_type_id =$request->form_type_id;
        $signUp->date_of_requirment = $date;
        $signUp->is_active = STATUS_ENABLED;
        $signUp->save();
        
      }

      elseif($request->form_type_id==TYPE_ACADEMY || TYPE_POPUP_FORM){
         $dob=date('Y-m-d',strtotime($request->dob));
         $user=User::where('email',$request->email)->first();
         if(empty($user)){
         $user = new User();
         $user->name = $request->name;
         $user->email = $request->email;
         $user->mobile_no = $request->mobile;
         $user->is_active =STATUS_ENABLED;
         $user->save();
        
         }
        $user_id=$user->id;

        $player = new Player();
        $player->user_id =  $user_id;
        $player->name =  $request->player_name;
        $player->dob =  $dob;
        $player->is_active = STATUS_ENABLED;
        $player->save();
        
        $signUp=new SignUp();
        $signUp->sport_id = $request->sport_id;
        $signUp->user_id = $user_id;
        $signUp->player_id = $player->id;
        $signUp->program_id = $request->program;
        $signUp->is_adult = $request->player_types;
        $signUp->form_type_id =$request->form_type_id;
        $signUp->source =$request->source;
        $signUp->is_active = STATUS_ENABLED;
        $signUp->save();
        }

        elseif($request->form_type_id==TYPE_CONTACT){
        
         $this->validateFormData($request);
         $user=User::where('email',$request->email)->first();
         if(empty($user)){
         $user = new User();
         $user->name = $request->name;
         $user->email = $request->email;
         $user->mobile_no = $request->mobile;
         $user->is_active =STATUS_DISABLED;
         $user->save();
        
         }
        $user_id=$user->id;
        
        $signUp=new SignUp();
        $signUp->sport_id = $request->sport_id;
        $signUp->user_id = $user_id;
        $signUp->message = $request->message;
        $signUp->form_type_id =$request->form_type_id;
        $signUp->is_active = STATUS_ENABLED;
        $signUp->save();
        }


      elseif($request->form_type_id==TYPE_ATHLETES_POPUP_FORM ){
        
         $dob=date('Y-m-d',strtotime($request->date_of_birth));
         $user=User::where('email',$request->email)->first();
         if(empty($user)){
         $user = new User();
         $user->name = $request->name;
         $user->email = $request->email;
         $user->mobile_no = $request->mobile;
         $user->is_active =STATUS_ENABLED;
         $user->save();
        
         }
        $user_id=$user->id;

        $player = new Player();
        $player->user_id =  $user_id;
        $player->name =  $request->name;
        $player->dob =  $dob;
        $player->is_active = STATUS_ENABLED;
        $player->save();
        
        $signUp=new SignUp();
        $signUp->user_id = $user_id;
        $signUp->player_id = $player->id;
        $signUp->form_type_id =$request->form_type_id;
        $signUp->is_active = STATUS_ENABLED;
        $signUp->save();
        }
        elseif($request->form_type_id==TYPE_ATHLETES_MEET_FORM){
         
         $dob=date('Y-m-d',strtotime($request->date_of_birth));
         $user=User::where('email',$request->email)->first();
         if(empty($user)){
         $user = new User();
         $user->name = $request->name;
         $user->email = $request->email;
         $user->mobile_no = $request->mobile;
         $user->nationality = $request->nationality;
         $user->is_active =STATUS_ENABLED;
         $user->save();
        
         }
        $user_id=$user->id;

        $player = new Player();
        $player->user_id =  $user_id;
        $player->name =  $request->name;
        $player->dob =  $dob;
        $player->is_active = STATUS_ENABLED;
        $player->save();
        
        $events=$request->events;
        foreach ($events as $key => $event) {
            $eventName[]=['eventName'=>$event];
        }
        $event_dates=$request->event_date;
        foreach ($event_dates as $key => $date) {
            $eventDate[]=['eventDate'=>$date];
        }
        
        $signUp=new SignUp();
        $signUp->user_id = $user_id;
        $signUp->player_id = $player->id;
        $signUp->events = json_encode($eventName);
        $signUp->event_date = json_encode($eventDate);
        $signUp->event_type =$request->event_type;
        $signUp->form_type_id =$request->form_type_id;
        $signUp->is_active = STATUS_ENABLED;
        $signUp->save();
        }


        return redirect()->route('form.submission.thankyou',[$request->form_type_id]);
         }

        }

    }
    
    
    public function inquiryform(Request $request)
    {
        // $this->validateFormData($request);
        // if(isset($request['g-recaptcha-response']) && !empty($request['g-recaptcha-response'])){
            
            // $secret=env("NOCAPTCHA_SECRET");
           
            // $verifyResponse =   file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$request['g-recaptcha-response']);
            // $responseData = json_decode($verifyResponse);
            // if($responseData->success){
                       
    //     if( $request->form_type_id==TYPE_VENUE){
    //      $date=date('Y-m-d',strtotime($request->date_of_requirement));
         
    //      $user=User::where('email',$request->email)->first();
    //      if(empty($user)){
    //      $user = new User();
    //      $user->name = $request->name;
    //      $user->email = $request->email;
    //      $user->mobile_no = $request->mobile;
    //      $user->is_active = STATUS_ENABLED;
    //      $user->save();
        
    //      }
    //     $user_id=$user->id;
        
    //     $signUp=new SignUp();
    //     $signUp->sport_id = $request->sport_id;
    //     $signUp->user_id = $user_id;
    //     $signUp->form_type_id =$request->form_type_id;
    //     $signUp->date_of_requirment = $date;
    //     $signUp->is_active = STATUS_ENABLED;
    //     $signUp->save();
        
    //   }

    //   elseif($request->form_type_id==TYPE_ACADEMY || TYPE_POPUP_FORM){
    //      $dob=date('Y-m-d',strtotime($request->dob));
    //      $user=User::where('email',$request->email)->first();
    //      if(empty($user)){
    //      $user = new User();
    //      $user->name = $request->name;
    //      $user->email = $request->email;
    //      $user->mobile_no = $request->mobile;
    //      $user->is_active =STATUS_ENABLED;
    //      $user->save();
        
    //      }
    //     $user_id=$user->id;

    //     $player = new Player();
    //     $player->user_id =  $user_id;
    //     $player->name =  $request->player_name;
    //     $player->dob =  $dob;
    //     $player->is_active = STATUS_ENABLED;
    //     $player->save();
        
    //     $signUp=new SignUp();
    //     $signUp->sport_id = $request->sport_id;
    //     $signUp->user_id = $user_id;
    //     $signUp->player_id = $player->id;
    //     $signUp->program_id = $request->program;
    //     $signUp->is_adult = $request->player_types;
    //     $signUp->form_type_id =$request->form_type_id;
    //     $signUp->source =$request->source;
    //     $signUp->is_active = STATUS_ENABLED;
    //     $signUp->save();
    //     }

    //     elseif($request->form_type_id==TYPE_CONTACT){
        
    //      $this->validateFormData($request);
    //      $user=User::where('email',$request->email)->first();
    //      if(empty($user)){
    //      $user = new User();
    //      $user->name = $request->name;
    //      $user->email = $request->email;
    //      $user->mobile_no = $request->mobile;
    //      $user->is_active =STATUS_DISABLED;
    //      $user->save();
        
    //      }
    //     $user_id=$user->id;
        
    //     $signUp=new SignUp();
    //     $signUp->sport_id = $request->sport_id;
    //     $signUp->user_id = $user_id;
    //     $signUp->message = $request->message;
    //     $signUp->form_type_id =$request->form_type_id;
    //     $signUp->is_active = STATUS_ENABLED;
    //     $signUp->save();
    //     }


    //   elseif($request->form_type_id==TYPE_ATHLETES_POPUP_FORM ){
        
    //      $dob=date('Y-m-d',strtotime($request->date_of_birth));
    //      $user=User::where('email',$request->email)->first();
    //      if(empty($user)){
    //      $user = new User();
    //      $user->name = $request->name;
    //      $user->email = $request->email;
    //      $user->mobile_no = $request->mobile;
    //      $user->is_active =STATUS_ENABLED;
    //      $user->save();
        
    //      }
    //     $user_id=$user->id;

    //     $player = new Player();
    //     $player->user_id =  $user_id;
    //     $player->name =  $request->name;
    //     $player->dob =  $dob;
    //     $player->is_active = STATUS_ENABLED;
    //     $player->save();
        
    //     $signUp=new SignUp();
    //     $signUp->user_id = $user_id;
    //     $signUp->player_id = $player->id;
    //     $signUp->form_type_id =$request->form_type_id;
    //     $signUp->is_active = STATUS_ENABLED;
    //     $signUp->save();
    //     }
    //     elseif($request->form_type_id==TYPE_ATHLETES_MEET_FORM){
         
    //      $dob=date('Y-m-d',strtotime($request->date_of_birth));
    //      $user=User::where('email',$request->email)->first();
    //      if(empty($user)){
    //      $user = new User();
    //      $user->name = $request->name;
    //      $user->email = $request->email;
    //      $user->mobile_no = $request->mobile;
    //      $user->nationality = $request->nationality;
    //      $user->is_active =STATUS_ENABLED;
    //      $user->save();
        
    //      }
    //     $user_id=$user->id;

    //     $player = new Player();
    //     $player->user_id =  $user_id;
    //     $player->name =  $request->name;
    //     $player->dob =  $dob;
    //     $player->is_active = STATUS_ENABLED;
    //     $player->save();
        
    //     $events=$request->events;
    //     foreach ($events as $key => $event) {
    //         $eventName[]=['eventName'=>$event];
    //     }
    //     $event_dates=$request->event_date;
    //     foreach ($event_dates as $key => $date) {
    //         $eventDate[]=['eventDate'=>$date];
    //     }
        
    //     $signUp=new SignUp();
    //     $signUp->user_id = $user_id;
    //     $signUp->player_id = $player->id;
    //     $signUp->events = json_encode($eventName);
    //     $signUp->event_date = json_encode($eventDate);
    //     $signUp->event_type =$request->event_type;
    //     $signUp->form_type_id =$request->form_type_id;
    //     $signUp->is_active = STATUS_ENABLED;
    //     $signUp->save();
    //     }
    
        $inquiries = new Inquiries();
        
        $inquiries->name = $request->name;
        $inquiries->email = $request->email;
        $inquiries->number = $request->number;
        $inquiries->date = $request->date;
        $inquiries->sport_id = $request->sport;
        $inquiries->message = $request->message;
        $inquiries->is_active = 1;
        $inquiries->save();

        // return Redirect::back()->withMessage('Inquiry Send!');
        return redirect()->route('pages.isd_academy');
        //  }

        // }

    }

     private function validateFormData(Request $request) {
       
        $request->validate([
            
            'name'  => 'required|max:50|regex:/^[\pL\s\-]+$/u',
            'player_name'  => 'nullable|max:50|regex:/^[\pL\s\-]+$/u',
            'email'  => 'required|email',
            'mobile'  => 'required|numeric|min:10',
            'g-recaptcha-response'=>'required',
            
        ]);
    }
}

