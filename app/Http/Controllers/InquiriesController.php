<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Sport;
use App\Inquiries;
use Auth;
use Carbon\Carbon;

class InquiriesController extends Controller
{
    public function index()
    {
        $Inquiries = Inquiries::orderBy('id', 'DESC')->paginate(15);
        // $types=FormType::pluck('name','id');
        $sports = Sport::all();
        return view('admin.pages.inquiry-form',['inquires'=>$Inquiries,'sports'=>$sports]);
    }
    
    public function edit(Inquiries $id, Request $request){
        
        // dd($id);
        $sports_all=Sport::pluck('name','id');
        
        return view('admin.inquiries.edit',['id'=>$id, 'sports_all' => $sports_all]);
    }
    
    public function update(Inquiries $id, Request $request){
        
        // dd($id);
        
        // $this->validateFormData($request);
        
        if(!empty($id->admin_comments)){
         $comments=json_decode($id->admin_comments,true);
         foreach($comments as $key=>$commentAdmin) {
           if ($commentAdmin['comments'] != null) {
            
            $data[]=['comments'=>$commentAdmin['comments'],'admin_name'=>$commentAdmin['admin_name'],'posted_on'=>$commentAdmin['posted_on'],'id'=>$commentAdmin['id']];
           }
           
         }
        }
        if(!empty($request->admin_comment)){
        $posted_on=Carbon::now()->toDateTimeString();
        $data[]=['comments'=>$request->admin_comment,'admin_name'=>Auth::user()->name,'posted_on'=>$posted_on,'id'=>$id->id
        ];
        }
        
        //  $id->name = $request->name;
        //  $id->sport_id = $request->sport_id;
        //  $id->email = $request->email;  
        //  $id->date = $request->dob;
        //  $id->number = $request->number;
        //  $id->message = $request->message;
         if(!empty($data)){
          $id->admin_comments = json_encode($data);   
         }
         $id->save();
         
      return redirect()->route('admin.inquiry.index')->with('success','Form Updated Successfully');
        
    }
    
    public function destroy(Inquiries $Inquiries)
    {
        $Inquiries->delete();
    
        	return redirect()->route('admin.inquiry.index')->with('success','Deleted Successfully');
    	
    	
    }
    
      public function searchInquiries(Request $request)
    {
       
         $name = $request->name;
          $email = $request->email; 
          $mobile = $request->mobile;
        //   $sports = $request->sport_id;
          $date = $request->date;
          
          $inquires = new Inquiries();
          
          if(!empty($name)){
              $inquires = $inquires->where('name',$name);
          }
          if(!empty($email)){
              $inquires = $inquires->where('email',$email);
          }
          if(!empty($mobile)){
              $inquires = $inquires->where('number',$mobile);
          }
        //   if(!empty($sports)){
        //       $Inquiries = $Inquiries->where('sport_id',$sports);
        //   }
          if(!empty($date)){
              $inquires = $inquires->whereDate('date', '=', $date);
          }
          
          
          $inquires = $inquires->get();
          
        //   dd($inquires);
          

        return view('admin.pages.inquiry-form',['inquires' => $inquires,'name' => $name,'email' => $email,'mobile' => $mobile,'date' =>$date]);
    }
    
    
     private function validateFormData(Request $request) {
       
        // $request->validate([
        //     'name'  => 'required|max:50',
        //     'email'  => 'required|email',
        //     'dob'  => 'required',
        //     'number'  => 'required|numeric|min:10',
        //     'sport_id' => 'required',
        //     'message' => 'required'
           
            
        // ]);
    }
}