<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SignUp;
use App\FormType;
use App\User;
use App\Sport;
use App\Player;
use App\Exports\SignAllUpsExport;
use App\Exports\SignUpsExport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use Carbon\Carbon;

class SignUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $signUps=SignUp::orderBy('id', 'DESC')->where('form_type_id','!=',TYPE_ATHLETES_MEET_FORM)->paginate(15);
        $types=FormType::pluck('name','id');
        $sports=Sport::all();
        return view('admin.pages.dashboard',['signUps'=>$signUps,'types'=>$types,'sports'=>$sports]);
    }

    public function indexTennis()
    {
        $signUps=SignUp::orderBy('id', 'DESC')->where('sport_id',4)->paginate(15);
        $types=FormType::pluck('name','id');
        $sports=Sport::where('is_active',STATUS_ENABLED)->get();
        return view('admin.pages.tennis',['signUps'=>$signUps,'types'=>$types,'sports'=>$sports]);
    }
    
    public function indexPopUp(){

        $signUps=SignUp::orderBy('id', 'DESC')->where('form_type_id',TYPE_POPUP_FORM)
        ->paginate(15);
        $sports=Sport::where('is_active',STATUS_ENABLED)->get();
        return view('admin.pages.pop-form',['signUps'=>$signUps,'sports'=>$sports]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAthleticsMeet()
    {
       $signUps=SignUp::orderBy('id', 'DESC')->where('form_type_id',TYPE_ATHLETES_MEET_FORM)->paginate(15);
       $events=[1=>'100 m',2=>'200 m',3=>'300 m',4=>'400 m',5=>'800 m',6=>'1500 m',7=>'3000 m'];
       $event_dates=[1=>"26th March",2=>"2nd April"];
       $types=FormType::where('is_active',STATUS_ENABLED)->get();
       return view('admin.pages.athletics_meet',['signUps'=>$signUps,
        'events'=>$events,'event_dates'=>$event_dates,'types'=>$types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function exportData(Request $request)
    {
       
       $signUps=SignUp::where('form_type_id',$request->submit)->get();
       if(count($signUps)==0)
       {
        return redirect()->route('admin.dashboard')->with('error',"Data Is Empty");
       }
       else{
       $date = date('Y-m-d');
       return Excel::download(new SignUpsExport($request), 'signup_'.$date.'.xlsx'); 
       }
        
    }

      public function exportAllData(Request $request)
    {
       
       $signUps=SignUp::all();
       if(count($signUps)==0)
       {
        return redirect()->route('admin.dashboard')->with('error',"Data Is Empty");
       }
       else{
       $date = date('Y-m-d');
       return Excel::download(new SignAllUpsExport($request), 'signup_'.$date.'.xlsx'); 
       }
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function searchSignUps(Request $request)
    {
       
        
        $name = $request->name;
        $email=$request->email;
        $mobile = $request->mobile;
        $type = $request->type;

        $date =$request->date_of_requirement;
        if(!empty($date)){
        $date_of_requirement=Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');	
        }
        else{
        	 $date_of_requirement="";
        }
        $signUp = signUp::where('is_active',STATUS_ENABLED);
        $signUp->whereHas('User',function($signUp) use ($email,$mobile,$name) {
        	 if(!empty($email)){
               $signUp->where('email',$email);
        	 }
        	 if(!empty($mobile)){
        	 $signUp->where('mobile_no',$mobile);	
        	 }
        	 if(!empty($name)){
        	 $signUp->where('name',$name)->where('type','!=',USER_TYPE_ADMIN);	
        	 }
             });
             if(!empty($date_of_requirement))
             {
             	$signUp->where('date_of_requirment',$date_of_requirement);
             }
             if(!empty($type))
             {
             	$signUp->where('form_type_id',$type);
             }
            $signUps=$signUp->paginate(15);
        
           $types=FormType::pluck('name','id');
        

        return view('admin.pages.dashboard',['signUps'=>$signUps,'types'=>$types,'mobile'=>$mobile,'request'=>$request]);
    }

     public function searchTennisSignUps(Request $request)
    {
       
        
        $name = $request->name;
        $email=$request->email;
        $mobile = $request->mobile;
        $type = $request->type;

        $date =$request->date_of_requirement;
        if(!empty($date)){
        $date_of_requirement=Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');	
        }
        else{
        	 $date_of_requirement="";
        }
        
         $signUp = signUp::where('sport_id',4);
         $signUp->whereHas('User',function($signUp) use ($email,$mobile,$name) {
        	 if(!empty($email)){
               $signUp->where('email',$email);
        	 }
        	 if(!empty($mobile)){
        	 $signUp->where('mobile_no',$mobile);	
        	 }
        	 if(!empty($name)){
        	 $signUp->where('name',$name)->where('type','!=',USER_TYPE_ADMIN);	
        	 }
             });
             if(!empty($date_of_requirement))
             {
             	$signUp->where('date_of_requirment',$date_of_requirement);
             }
            $signUps=$signUp->paginate(15);
        
        $types=FormType::pluck('name','id');
        

        return view('admin.pages.tennis',['signUps'=>$signUps,
           'name'=>$name,'type'=>$type,'types'=>$types,'mobile'=>$mobile,'email'=>$email,
           'date_of_requirement'=>$date_of_requirement]);
    }

    public function searchAthleticsMeet(Request $request)
    {
       
        
        $keyword = $request->keyword;
         $signUp = signUp::where('form_type_id','=',TYPE_ATHLETES_MEET_FORM);
         $signUp->whereHas('User',function($signUp) use ($email,$mobile,$name) {
        	 if(!empty($email)){
               $signUp->where('email',$email);
        	 }
        	 if(!empty($mobile)){
        	 $signUp->where('mobile_no',$mobile);	
        	 }
        	 if(!empty($name)){
        	 $signUp->where('name',$name)->where('type','!=',USER_TYPE_ADMIN);	
        	 }
             });
            $signUps=$signUp->paginate(15);
            
            return view('admin.pages.athletics_meet',['signUps'=>$signUps,
           'keyword'=>$keyword]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(SignUp $signUp)
    {
       
       $sports_all=Sport::pluck('name','id');
       $types=FormType::where('is_active',STATUS_ENABLED)->pluck('name','id');
        return view('admin.signups.edit',['signUp'=>$signUp,'types'=>$types,'sports_all'=>$sports_all]);
    }

    public function update(Request $request, SignUp $signUp)
    {
         $this->validateFormData($request);
        
         if(!empty($request->date_of_requirment)){
          $date=date('Y-m-d',strtotime($request->date_of_requirment));
             
         }
         else{
            $date=null;
         }
          if(!empty($request->dob)){
            $dob=date('Y-m-d',strtotime($request->dob));
         }
         else{
            $dob=null;
         }
        
        if(!empty($signUp->admin_comments)){
         $comments=json_decode($signUp->admin_comments,true);
         foreach($comments as $key=>$commentAdmin) {
           if ($commentAdmin['comments'] != null) {
            
            $data[]=['comments'=>$commentAdmin['comments'],'admin_name'=>$commentAdmin['admin_name'],'posted_on'=>$commentAdmin['posted_on'],'id'=>$commentAdmin['id']];
           }
           
         }
        }
        if(!empty($request->admin_comment)){
        $posted_on=Carbon::now()->toDateTimeString();
        $data[]=['comments'=>$request->admin_comment,'admin_name'=>Auth::user()->name,'posted_on'=>$posted_on,'id'=>$signUp->id
        ];
        }
        
        

         $signUp->form_type_id =$request->form_type_id;
         $signUp->sport_id =$request->sport_id;
         $signUp->date_of_requirment = $date;
         if(!empty($data)){
          $signUp->admin_comments = json_encode($data);   
         }
         $signUp->save();
         
         if(!empty($signUp->player_id)){
         $player=Player::find($signUp->player_id);
         $player->name = $request->player_name;
         $player->dob = $dob;
         $player->save();
         }

         $user=User::find($signUp->user_id);
         $user->name = $request->name;
         $user->email = $request->email;
         $user->mobile_no = $request->mobile;
         $user->save();
         
         if($request->form_type_id==TYPE_POPUP_FORM){
         return redirect()->route('admin.popup.index')->with('success','Form Updated Successfully');
         }
         else{
             return redirect()->route('admin.dashboard')->with('success','Form Updated Successfully');
         }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SignUp $signUp)
    {
    	$url=\Request::segment(2);
        $signUp->delete();
        if($url=="athletics_meet"){
        	
    	   return redirect()->route('admin.athletics.meet.index')->with('success','Form Deleted Successfully');
    	}
        elseif($url=="tennis"){
            
           return redirect()->route('admin.tennis.index')->with('success','Form Deleted Successfully');
        }
        elseif($url=="pop-up"){
            
           return redirect()->route('admin.popup.index')->with('success','Form Deleted Successfully');
        }
    	else{
          return redirect()->route('admin.dashboard')->with('success','Form Deleted Successfully');
    	}
      
    }

    private function validateFormData(Request $request) {
       
        $request->validate([
            'name'  => 'required|max:50|regex:/^[\pL\s\-]+$/u',
            'player_name'  => 'nullable|max:50|regex:/^[\pL\s\-]+$/u',
            'email'  => 'required|email',
            'mobile'  => 'required|numeric|min:10',
           
            
        ]);
    }
}
