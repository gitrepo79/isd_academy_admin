<?php



namespace App;



use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Inquiries extends Model

{
        use SoftDeletes;
        
        protected $table = 'inquiries';

  protected $dates = ['deleted_at'];

   protected $fillable = [

        'sport_id', 'name', 'email', 'number', 'date','message','is_active'

    ];

     public function sport()

    {

        return $this->belongsTo('App\Sport');

    }
}