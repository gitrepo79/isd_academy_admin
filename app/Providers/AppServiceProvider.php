<?php



namespace App\Providers;



use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Schema;

use App\Sport;


class AppServiceProvider extends ServiceProvider

{

    /**

     * Register any application services.

     *

     * @return void

     */

    public function register()

    {

        //

    }



    /**

     * Bootstrap any application services.

     *

     * @return void

     */

    public function boot()

    {

     Schema::defaultStringLength(191);
     view()->composer('*', function ($view) {
     $sports=Sport::where('is_active',STATUS_ENABLED)->pluck('name','id');
     $view->with('sports',$sports); 
        
    });  

    }

}

