<?php



namespace App;



use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SignUp extends Model

{
   use SoftDeletes;

  protected $dates = ['deleted_at'];

   protected $fillable = [

        'sport_id', 'user_id', 'player_id', 'date_of_requirment', 'time_of_requirment','form_type_id',

    

    ];



    public function player()

    {

        return $this->belongsTo('App\Player');

    }
    
     public function sport()

    {

        return $this->belongsTo('App\Sport');

    }

    public function program()

    {

        return $this->belongsTo('App\Program');

    }


    public function user()

    {

        return $this->belongsTo('App\User');

    }

    public function formType(){

        return $this->belongsTo('App\FormType');

    }

    public function admin($id){
      return User::where('id',$id)->pluck('name')->first();
    }


}

