<?php



namespace App;



use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Notifications\Notifiable;

use Illuminate\Support\Facades\Auth;



class User extends Authenticatable

{

    use Notifiable;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'name', 'email', 'password', 'gender', 'email_secondary', 'mobile_no', 'other_contact_no', 'nationality', 'address', 'hear_about_us'

    ];



    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [

        'password', 'remember_token',

    ];



    /**

     * The attributes that should be cast to native types.

     *

     * @var array

     */

    protected $casts = [

        'email_verified_at' => 'datetime',

    ];



    public static function getAuthenticatedUser()

    {

        if(Auth::check()) { // Check is User LoggedIn

            if (Auth::user()->type == USER_TYPE_ADMIN) { // Check is User an Admin

                if(!empty(session('parent_id'))){

                    // Return Parent User Object

                    return User::find(session('parent_id'))->first();

                    

                   

                } else {

                    // Return Admin User Object

                    return Auth::user();

                }

            } else {

                // Return User Object

                return Auth::user();



            }

        } else {

            // Return false if User not LoggedIn

            return false;

        }

    }

    public function signUps()

    {

        return $this->hasMany('App\SignUp');

    }

  



}

