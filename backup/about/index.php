<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "About Page";
    $keywords = "";
    $desc = "";
    $pageclass = "aboutpg homepg";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="main-section">
	
	<section class="home-container about-page">
		<div class="homepage-slider">

			<div class="item" style="background-image: url('/assets-web/images/banners/about-banner.jpg');">
				
				<div class="inner-content --tennis">
				</div>

				<article>
					<h1 class="maintitle mb-20">
						ABOUT
					</h1>

					<p class="about-desc d-none d-xl-block">ISD is Dubai’s new sports destination with world-class playing venues for football, rugby, tennis, padel, track & field and much more. Within easy reach of all Dubai communities, ISD is your premiere venue of choice, to get your children active, to play or to watch your favorite sport with friends or to host your next tournament.<br /><br />

					ISD’S state-of-the art facilities, among the world’s best, are designed, developed and maintained at certified professional standards and made accessible to everyone, from children and youth to amateurs and professionals.  <br /><br />

					With 4 FIFA standard football pitches, 2 Rugby standard pitches, 1 Olympic standard track, 4 outdoor tennis courts, 5 indoor padel courts and the UAE’s only 2 indoor tennis and multisport courts, coupled with Eupepsia’s advanced sports science and wellness clinic and gourmet cafés and restaurants, ISD caters to all your athletic needs.<br /><br />

					Centrally located in Dubai and highly accessible within a 10-minute drive to all major Dubai communities, ISD is your ideal venue for sports and entertainment, providing athletic training & competition, entertainment and a gateway to optimal performance and wellness.</p>
				</article>

			</div>

		</div>
	</section>

	<section class="--sports-box about d-block d-xl-none">
		<div class="about-content">
			<h2 class="title mb-40">ABOUT ISD</h2>
			<p class="desc">ISD is Dubai’s new sports destination with world-class playing venues for football, rugby, tennis, padel, track & field and much more. Within easy reach of all Dubai communities, ISD is your premiere venue of choice, to get your children active, to play or to watch your favorite sport with friends or to host your next tournament.<br /><br />

			ISD’S state-of-the art facilities, among the world’s best, are designed, developed and maintained at certified professional standards and made accessible to everyone, from children and youth to amateurs and professionals.  <br /><br />

			With 4 FIFA standard football pitches, 2 Rugby standard pitches, 1 Olympic standard track, 4 outdoor tennis courts, 5 indoor padel courts and the UAE’s only 2 indoor tennis and multisport courts, coupled with Eupepsia’s advanced sports science and wellness clinic and gourmet cafés and restaurants, ISD caters to all your athletic needs.<br /><br />

			Centrally located in Dubai and highly accessible within a 10-minute drive to all major Dubai communities, ISD is your ideal venue for sports and entertainment, providing athletic training & competition, entertainment and a gateway to optimal performance and wellness.</p>
		</div>
	</section>

</section>

<?php include ($path.'/inc/footer.php'); ?>