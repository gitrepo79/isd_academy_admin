<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "Athletics Page";
    $keywords = "";
    $desc = "";
    $pageclass = "athleticpg inner-page";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/running-banner.jpg');">

	<div class="inner-content --running">
	</div>

	<div class="vertical-heading">
		<h2 class="title">Athletics</h2>
	</div>

	<div class="content-section">
		<p class="mobile-content">
			The best track & field venue located in the heart of the city, the Athletics Center has a 9-lane Olympic standard track, long jump and high jump facilities as well capacity for all throwing disciplines. 
		</p>
		<div class="container-wrapper">
			<div class="row">

				<div class="col-xl-4">
					<div class="box --round-box">
						<article>
							<h2 class="title">academy</h2>

							<p class="desc d-none d-sm-block"> Delivered by international competitive athletes, the UAE’s most comprehensive program for athletic development, Ultimate Athletics offers all disciplines and caters to athletes of all levels, from beginners to those pursuing track & field professionally.</p>

							<a href="#" class="book-btn">Register</a>
						</article>
					</div>
				</div>

				<div class="col-xl-4">
					<div class="box --round-box">
						<article>
							<h2 class="title">MEMBERSHIP</h2>

							<p class="desc d-none d-sm-block">Stay active with a membership at the Athletics Center to use the track and outdoor gym.  Packages are monthly, 3 months and annual.</p>

							<a href="#" class="book-btn">Book</a>
						</article>
					</div>
				</div>

				<div class="col-xl-4">
					<div class="box --round-box">
						<article>
							<h2 class="title"> leagues <small class="fs-large">&amp; tournaments</small></h2>

							<p class="desc d-none d-sm-block">The Athletics Center Dubai boasts the best track & field facilities in Dubai and is strategically located in the heart of the city, accessible to all communities.  Call us today for venue bookings to host your events.</p>

							<a href="#" class="book-btn">Enquire</a>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<?php include ($path.'/inc/footer.php'); ?>