<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "Football Page";
    $keywords = "";
    $desc = "";
    $pageclass = "footballpg inner-page";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/football-banner.jpg');">

	<div class="inner-content --football">
	</div>

	<div class="vertical-heading">
		<h2 class="title">Football</h2>
	</div>

	<div class="content-section">
		<p class="mobile-content">
			Boasting a selection of world-class pitches to rent with friends, the strongest football academy in the country, LaLiga Academy, and hosting local and international tournaments, The Football Center Dubai is your football venue of choice.
		</p>
		<div class="container-wrapper">
			<div class="row">
				<div class="col-xl-4">
					<div class="box --round-box">
						<article>
							<h2 class="title">rental</h2>

							<p class="desc d-none d-sm-block">Want to play a match or host your own tournament? You can rent one of the Football Center Dubai’s world-class pitches. </p>

							<a href="#" class="book-btn">BOOK</a>
						</article>
					</div>
				</div>

				<div class="col-xl-4">
					<div class="box --round-box">
						<article>
							<h2 class="title">academy</h2>

							<p class="desc d-none d-sm-block">LaLiga Academy offers young players across the UAE an immersive, professional football experience, with a superior methodology recognized as the gold standard across the world.</p>

							<a href="#" class="book-btn">Register</a>
						</article>
					</div>
				</div>

				<div class="col-xl-4">
					<div class="box --round-box">
						<article>
							<h2 class="title"> leagues <small class="fs-large">&amp; tournaments</small></h2>

							<p class="desc d-none d-sm-block">The Football Center Dubai boasts the best facilities in the region to make your event an unforgettable experience.</p>

							<a href="#" class="book-btn">Enquire</a>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<?php include ($path.'/inc/footer.php'); ?>