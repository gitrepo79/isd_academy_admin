<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "Footlab Page";
    $keywords = "";
    $desc = "";
    $pageclass = "footlabpg homepg";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="main-section">
	
	<section class="home-container">
		<div class="homepage-slider">

			<div class="item" style="background-image: url('/assets-web/images/banners/footlab-banner.jpg');">
				
				<div class="inner-content --football">
				</div>

				<article>
					<h1 class="maintitle mb-20">
						Footlab <br /> Dubai
					</h1>

					<p class="fs-large fc-white d-none d-xl-block">The region’s first Footlab now at the ISD in Dubai. <br /><br />

					Footlab is the world’s first indoor football performance park enabling footballers to test key skills and receive stats.<br /><br />

					It offers a blend of performance and user experience that  produces high precision, combining advanced technology and football.<br /><br />

					A truly worldwide-unique football platform, created to  improve technical and tactical ability, physical fitness,  team play and social skills.<br /><br />

					Each player will experience a 2 hour session at Footlab after which they will receive a stats report about their performance.</p>
				</article>

			</div>

		</div>
	</section>

	<section class="--sports-box about d-block d-xl-none">
		<div class="about-content">
			<h2 class="title mb-40">Footlab Dubai</h2>
			<p class="desc">The region’s first Footlab now at the ISD in Dubai. <br /><br />

			Footlab is the world’s first indoor football performance park enabling footballers to test key skills and receive stats.<br /><br />

			It offers a blend of performance and user experience that  produces high precision, combining advanced technology and football.<br /><br />

			A truly worldwide-unique football platform, created to  improve technical and tactical ability, physical fitness,  team play and social skills.<br /><br />

			Each player will experience a 2 hour session at Footlab after which they will receive a stats report about their performance.</p>
		</div>
	</section>

</section>

<?php include ($path.'/inc/footer.php'); ?>