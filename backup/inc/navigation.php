<header class="primary-header">
	<a href="/" class="header-logo animsition-link">
		<img src="/assets-web/images/logo-white-icon.svg" alt="" class="icon">
		<img src="/assets-web/images/logo-white-text.svg" alt="" class="text">
	</a>

	<!-- Mobile Navigation Button Start-->
	<div class="mobile-nav-btn">
	    <span class="lines"></span>
	    <span class="lines"></span>
	    <span class="lines"></span>
	    <span class="lines"></span>
	</div>
	<!-- Mobile Navigation Button End-->

	<nav class="primary-navigation">
		<ul class="unstyled">
			<li>
				<a href="/about" class="animsition-link">About</a>
			</li>

			<li class="drop-menu">
				<a href="#">Sports</a>

				<ul class="unstyled dropdown hidden">
					<li>
						<a href="/football" class="animsition-link">Football</a>
					</li>
					<li>
						<a href="#" class="animsition-link">Tennis</a>
					</li>
					<li>
						<a href="/rugby" class="animsition-link">Rugby</a>
					</li>
					<li>
						<a href="#" class="animsition-link">Padel</a>
					</li>
					<li>
						<a href="/athletics" class="animsition-link">Athletics</a>
					</li>
				</ul>
			</li>

			<li>
				<a href="/footlab" class="animsition-link">Footlab</a>
			</li>

			<li>
				<a href="https://eupepsia.com/programs/medicalClinic" target="_blank" class="animsition-link">Eupepsia <small>sports science & wellness</small></a>
			</li>

			<li>
				<a href="#" class="animsition-link">Dining</a>
			</li>

			<li>
				<a href="#" class="animsition-link">Venue <small>Rental</small></a>
			</li>
		</ul>
	</nav>

</header>