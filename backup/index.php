<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "Home Page";
    $keywords = "";
    $desc = "";
    $pageclass = "homepg";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="main-section" data-0="transform: translateX(0px)" data-4000="transform: translateX(-2150px)" data-scroll-speed="4">
	
	<section class="first-container home-container">
		<div class="homepage-slider">

			<div class="item" style="background-image: url('/assets-web/images/banners/football-banner.jpg');">
				
				<div class="inner-content --football">
				</div>

				<article>
					<h1 class="maintitle">
						compete <br class="d-none d-lg-block" /> with <br class="d-none d-xl-block" /> yourself
					</h1>
				</article>

			</div>

			<div class="item" style="background-image: url('/assets-web/images/banners/tennis-banner.jpg');">
				
				<div class="inner-content --tennis">
					
				</div>

				<article>
					<h1 class="maintitle">
						compete <br class="d-none d-lg-block" /> with <br class="d-none d-xl-block" /> yourself
					</h1>
				</article>
			</div>

			<div class="item" style="background-image: url('/assets-web/images/banners/padel-banner.jpg');">
				
				<div class="inner-content --padel">
					
				</div>

				<article>
					<h1 class="maintitle">
						compete <br class="d-none d-lg-block" /> with <br class="d-none d-xl-block" /> yourself
					</h1>
				</article>
			</div>

			<div class="item" style="background-image: url('/assets-web/images/banners/rugby-banner.jpg');">
				
				<div class="inner-content --rugby">
					
				</div>

				<article>
					<h1 class="maintitle">
						compete <br class="d-none d-lg-block" /> with <br class="d-none d-xl-block" /> yourself
					</h1>
				</article>

			</div>

			<div class="item" style="background-image: url('/assets-web/images/banners/running-banner.jpg');">
				
				<div class="inner-content --running">
					
				</div>

				<article>
					<h1 class="maintitle">
						compete <br class="d-none d-lg-block" /> with <br class="d-none d-xl-block" /> yourself
					</h1>
				</article>
			</div>

		</div>
	</section>

	<section class="--sports-box about">
		<div class="about-content">
			<h2 class="title">Inspiratus Sports District</h2>
			<p class="desc">ISD is Dubai’s new sports destination offering the community world class playing venues for football, rugby, tennis, padel, track & field and much more. <br> <br> Easily accessible by all Dubai Communities, ISD is your venue of choice to get your children active, to play or to watch your favorite sport with friends or to host your next tournament.</p>
		</div>
	</section>

	<section class="--sports-box football" style="background-image: url('/assets-web/images/banners/football-banner.jpg');">
		
		<div class="vertical-heading">
			<h2 class="title">Football</h2>
		</div>

		<div class="bottom-content">
			<p class="desc --white">
				Do you play football? Enjoy the Beautiful Game at The Football Center. The Football Center located at Dubai Sports City boasts one of the region’s best sports facilities with first-rate football pitches, academy, entertainment, and extensive indoor and outdoor grounds -for your best football experience!
			</p>
		</div>

		<a href="/football" class="readmore-btn"></a>

	</section>

	<section class="--sports-box tennis" style="background-image: url('/assets-web/images/banners/tennis-banner.jpg');">
		
		<div class="vertical-heading">
			<h2 class="title">Tennis</h2>
		</div>

		<div class="bottom-content">
			<p class="h3 fc-white">
				A world-class offering coming soon. <br> Stay tuned!
			</p>
		</div>

		<a href="#" class="readmore-btn"></a>

	</section>

	<section class="--sports-box rugby" style="background-image: url('/assets-web/images/banners/rugby-banner.jpg');">
		
		<div class="vertical-heading">
			<h2 class="title">Rugby</h2>
		</div>

		<div class="bottom-content">
			<p class="desc --white">
				Enjoy playing rugby at the state-of-the-art Rugby Sports Park and learning the sport at the hands of highly qualified coaches. The rugby facilities feature two full-size IRB-standard, floodlit grass pitches, as well as one IRB-standard, outdoor artificial turf pitch and one IRB-standard, air-conditioned, indoor artificial turf pitch.
			</p>
		</div>

		<a href="/rugby" class="readmore-btn"></a>

	</section>

	<section class="--sports-box padel" style="background-image: url('/assets-web/images/banners/padel-banner.jpg');">
		
		<div class="vertical-heading">
			<h2 class="title">Padel</h2>
		</div>

		<div class="bottom-content">
			<p class="h3 fc-white">
				A world-class offering coming soon. <br> Stay tuned!
			</p>
		</div>

		<a href="#" class="readmore-btn"></a>

	</section>

	<section class="--sports-box running" style="background-image: url('/assets-web/images/banners/running-banner.jpg');">
		
		<div class="vertical-heading">
			<h2 class="title">ATHLETICS</h2>
		</div>

		<div class="bottom-content">
			<p class="desc --white">
				Develop your skills at Dubai’s top venue for Athletics, with a best in-class training methodology built upon expert coach-led, athlete-driven environment.  The program emphasizes personalized growth and development within a team environment, where athletes are motivated to work on improving their personal best at each session.
			</p>
		</div>

		<a href="/athletics" class="readmore-btn"></a>

	</section>

</section>

<?php include ($path.'/inc/footer.php'); ?>