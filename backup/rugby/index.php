<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "Rugby Page";
    $keywords = "";
    $desc = "";
    $pageclass = "rugbypg inner-page";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/rugby-banner.jpg');">

	<div class="inner-content --rugby">
	</div>

	<div class="vertical-heading">
		<h2 class="title">Rugby</h2>
	</div>

	<div class="content-section">
		<p class="mobile-content">
			If Rugby is your sport, get into the game at our state-of-the-art facilities at the Rugby Center.  Home to the Dubai Knight Eagles Rugby Club for competitive teams and offering High Performance Skills training for individuals, we cater to all your rugby needs, with programs for youth and adults. 
		</p>
		<div class="container-wrapper">
			<div class="row">

				<div class="col-xl-4 offset-xl-3">
					<div class="box --round-box">
						<article>
							<h2 class="title">academy</h2>

							<p class="desc d-none d-sm-block">From January 2021 enhance your skills with our high performance skills development program. </p>

							<a href="#" class="book-btn">Register</a>
						</article>
					</div>
				</div>

				<div class="col-xl-4">
					<div class="box --round-box">
						<article>
							<h2 class="title"> leagues <small class="fs-large">&amp; tournaments</small></h2>

							<p class="desc d-none d-sm-block">With top-of-the line pitches and a club house within easy access of all Dubai communities, the Rugby Center can host your next tournament or league.</p>

							<a href="#" class="book-btn">Enquire</a>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<?php include ($path.'/inc/footer.php'); ?>