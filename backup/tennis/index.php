<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "Tennis Page";
    $keywords = "";
    $desc = "";
    $pageclass = "tennispg inner-page";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/football-banner.jpg');">

	<div class="inner-content --tennis">
	</div>

	<div class="vertical-heading">
		<h2 class="title">tennis</h2>
	</div>

	<div class="content-section">
		<div class="container-wrapper">
			<div class="row">
				<div class="col-xl-4">
					<div class="box --round-box">
						<h2 class="title">pitch <br /> rental</h2>

						<p class="desc d-none d-sm-block">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. lobortis nisl ut </p>

						<a href="#" class="book-btn">Book</a>
					</div>
				</div>

				<div class="col-xl-4">
					<div class="box --round-box">
						<h2 class="title">pitch <br /> rental</h2>

						<p class="desc d-none d-sm-block">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. lobortis nisl ut </p>

						<a href="#" class="book-btn">Book</a>
					</div>
				</div>

				<div class="col-xl-4">
					<div class="box --round-box">
						<h2 class="title">pitch <br /> rental</h2>

						<p class="desc d-none d-sm-block">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. lobortis nisl ut </p>

						<a href="#" class="book-btn">Book</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<?php include ($path.'/inc/footer.php'); ?>