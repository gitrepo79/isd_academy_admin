<?php



// USER TYPE

define('USER_TYPE_ADMIN', 1);
define('USER_TYPE_MANAGER', 2);
define('USER_TYPE_PARENT', 9);

// GENDER

define('GENDER_MALE', 1);
define('GENDER_FEMALE', 2);



//form type

define('TYPE_VENUE', 1);
define('TYPE_ACADEMY',2);
define('TYPE_CONTACT',3);
define('TYPE_ATHLETES_POPUP_FORM',4);
define('TYPE_ATHLETES_MEET_FORM',5);
define('TYPE_POPUP_FORM', 6);


define('TYPE_DOWNLOAD_ALL',6);
define('TYPE_REGISTER', 7);

define('GOAL_KEEPER', 1);
define('NOT_GOAL_KEEPER',0);



// STATUS

define('STATUS_ENABLED', 1);
define('STATUS_DISABLED', 0);



define('STATUS_NOT_PAID',1);

define('STATUS_PARTIAL_PAYMENT',2);

define('STATUS_PAID',3);

define('STATUS_READY_TO_PAY',4);

define('STATUS_INVOICE_ACCEPTED',5);

define('STATUS_REFUNDED',6);

define('STATUS_PARTIAL_PAYMENT_PENDING',7);

define('STATUS_PAYMENT_DEFAULTED',8);

define('STATUS_SPONSORED',9);

define('STATUS_MAKE_UP',10);

define('STATUS_PENDING',11);





define('WALLET_STATUS_USED', 1);

define('WALLET_STATUS_NOT_USED', 2);

define('WALLET_STATUS_CANCEL', 3);



define('AMOUNT_TYPE_DEBIT', 1);

define('AMOUNT_TYPE_CREDIT', 2);



define('SEASON_TERM', 1);

define('WITHDAYS', 2);

define('WITHOUTDAYS', 3);



// Pitch Size
define('PITCH_SIZE_QUARTER',  1);
define('PITCH_SIZE_HALF', 2);
define('PITCH_SIZE_FULL', 3);
//booking
define('BOOKING_STATUS_PENDING',   1);
define('BOOKING_STATUS_CONFIRMED', 2);
define('BOOKING_STATUS_CANCELLED', 3);
define('BOOKING_STATUS_DONE',      4);
define('BOOKING_STATUS_TBC',       5);