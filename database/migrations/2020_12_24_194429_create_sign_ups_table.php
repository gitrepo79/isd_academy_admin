<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignUpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sign_ups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sport_id');
            $table->integer('user_id');
            $table->integer('player_id')->nullable();
            $table->integer('form_type_id');
            $table->date('date_of_requirment')->nullable();
            $table->time('time_of_requirment')->nullable();
            $table->tinyInteger('is_active')->default(STATUS_ENABLED);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sign_ups');
    }
}
