<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       $this->call(EmiratesTableDataSeeder::class);
       $this->call(DayTableDataSeeder::class);
       $this->call(LocationTableDataSeeder::class);
       $this->call(AgeTableDataSeeder::class);
    }
}
