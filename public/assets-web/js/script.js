// JavaScript Document
$(function(){

    //*****************************
    // Reset Href
    //*****************************
    $('[href="#"]').attr("href","javascript:;");

    //*****************************
    // Smooth Scroll
    //*****************************
    function goToScroll(e){
        $('html, body').animate({
            scrollTop: $("."+e).offset().top
        }, 1000);
    }

    //*****************************
    // Lazy Load
    //*****************************
    $(window).scroll(function(){
        lazzyload();
    });
    
    //*****************************
    // Mobile Navigation
    //*****************************
    if ($(window).width() < 991) {
        $('.header-navigation').removeClass('nav-active');
       $('.mobile-nav-btn').click(function() {
        $('.mobile-nav-btn, .header-navigation, .blur-section').toggleClass('nav-active');
        //$('.overlay-bg').fadeIn();
    });
    }
    else {
       $('.header-navigation').addClass('nav-active');
    }

    
    
    // $('.overlay-bg').click(function(){
    //     $('.mobile-nav-btn, .mobile-nav, .app-container').removeClass('active');
    //     $(this).fadeOut();
    // });


    //*****************************
    // Close Funtion
    //*****************************
    $('.close-this').click(function(){
        $(this).parent().hide();
    });


    //*****************************
    // Scroll Funtion
    //*****************************

    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();

        if (scroll >= 130) {
            $('.mobile-contact').addClass('active');
        } else {
            $('.mobile-contact').removeClass('active');
        }

        if (scroll >= 100) {
            $('header.ph').addClass('fixed');
        } else {
            $('header.ph').removeClass('fixed');
        }
    });


    //*****************************
    // Slick Slider
    //*****************************
	var respsliders = {
      1: {slider : '.gallery-slider'}
    };

    $.each(respsliders, function() {

        $(this.slider).slick({

            arrows: true,
            dots: false,
            infinite: true,
            autoplay:true,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 575,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });

    //*****************************
    // File Browser
    //*****************************
    xfilebrowse('.file-upload');
    function xfilebrowse(tgt){  
        $(tgt+' input[type=file]').each(function() {
            $(this).wrap('<div class="upldwrap" />');
            $(this).parent().append('<span class="browse">Choose Files</span> <label class="filelabel">Upload your files</label>');
            $(this).css('opacity', 0);
            $(this).on('change', function() {
                var txt = $(this).val();
                if(txt !== ''){
                    txt = txt.replace(/\\/g, '/').replace(/.*\//, '');
                    $(this).siblings('.filelabel').html(txt);
                }else{
                    $(this).siblings('.filelabel').html('No File Selected');
                }                
            })
        });
    }
    

    //*****************************
    // Fancybox
    //*****************************
    $('[data-fancybox="gallery"]').fancybox({
        // Options will go here
    });

    //*****************************
    // Form Animation
    //*****************************
    $('.form-field').on('focus blur',function(i){
        $(this).parents('.control-group').toggleClass('focused','focus'===i.type||this.value.length>0)
    }).trigger('blur');

    //*****************************
    // Tabbing
    //*****************************
    $('[data-targetit]').on('click',function () {
        $(this).siblings().removeClass('current');
        $(this).addClass('current');
        var target = $(this).data('targetit');
        $('.'+target).siblings('[class^="tabs"]').removeClass('show');
        $('.'+target).addClass('show');
        //$('.slick-slider').slick('setPosition', 0);
    });

    //*****************************
    // Modal
    //*****************************
    $('[data-targetmodal]').on('click',function () {
        $('body').addClass('modal-open');
        var target = $(this).data('targetmodal');
        $('.'+target).addClass('show');
    });

    $('.modal-close').click(function() {
        $('.x-modal').removeClass('show');
        $('body').removeClass('modal-open');
    });

    //*****************************
    // Accordian
    //*****************************
    $('.accordian-head').on('click',function () {
        $(this).parents().toggleClass('open');
        $(this).siblings('.accordian-body').slideToggle();
    });

    //*****************************
    // Copyright Year
    //*****************************
    now=new Date;thecopyrightYear=now.getYear();if(thecopyrightYear<1900)thecopyrightYear=thecopyrightYear+1900;$("#cur-year").html(thecopyrightYear);

    //*****************************
    // Set Map
    //*****************************
    $("address.setmap").each(function(){
        var embed ="<iframe frameborder='0' scrolling='no' marginheight='0' height='100%' width='100%' marginwidth='0' src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( $(this).text() ) +"&amp;output=embed'></iframe>";
        $(this).html(embed);
    });

});