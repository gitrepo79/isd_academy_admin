<?php $no = 1; ?>
@extends('layouts.admin')
@section('content')
	<h3 class="pagetitle">Contact Form</h3>
	<section class="box">
		<div class="box-header">
			<p class="box-title">Leads</p>
		</div>
		<div class="box-body">

			<table class="table --bordered --hover">
				<thead>
					<tr>
						<th width="5%">
							<center>S No</center>
						</th>
						<th><center>Parent Name</center></th>
						<th><center>Parent Email</center></th>
						<th><center>Parent Phone</center></th>
						<th><center>Student Name</center></th>
						<th><center>DOB</center></th>
						<th><center>Sport</center></th>
						<th><center>Country</center></th>
						<th>
							<center>Created At</center>
						</th>
						{{-- <th>
                        <center>Action</center>
                    </th> --}}
					</tr>
				</thead>
				<tbody>
					@foreach ($leads as $key => $lead)
						<tr>
							<td>
								<center>{{ $no++ }}</center>
							</td>
							<td>
								<center>{{ $lead->parent_name }}</center>
							</td>
							<td>
								<center>{{ $lead->parent_email }}</center>
							</td>
							<td>
								<center>{{ $lead->parent_phone }}</center>
							</td>
							<td>
								<center>{{ $lead->child_name }}</center>
							</td>
							<td>
								<center>{{ $lead->child_dob }}</center>
							</td>
							<td>
								<center>{{ $lead->sport_id }}</center>
							</td>
							<td>
								<center>{{ $lead->city }}</center>
							</td>
							<td>
								<center>
									<p>
										{{ \Carbon\Carbon::parse($lead->created_at)->format('d/m/Y H:i') }}
									</p>
								</center>
							</td>
							{{-- <td>
								<center>
                            <a href="{{ route('admin.signUps.edit',[$signUp->id]) }}"
                                class="btn --btn-small bg-secondary fc-white">Edit</a>
                            <button type="button" class="deleteItem btn --btn-small bg-danger fc-white"
                                data-delete-id="{{ $signUp->id }}" id="deleteItem">Delete</button>
                        </center>
							</td> --}}
						</tr>
					@endforeach
				</tbody>

			</table>
			<ul class="pagination">
				{{ $leads->links() }}
			</ul>
		</div>
	</section>
	{{-- <script type="text/javascript">
    var dateToday = new Date();
$( "#date_of_requirement" ).datepicker({
});
</script> --}}
	{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
	{!! Form::close() !!}
	{{-- <script>
    var deleteResourceUrl = '{{ url("/pop-up") }}';
$(document).ready(function () {

$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));

$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script> --}}
@endsection
