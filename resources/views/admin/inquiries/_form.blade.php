
<div class="box-body">
    <div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Name', ['class'=>'form-label',]) !!}
        {!! Form::text('name',$id->name, ['class'=>'form-field','readonly' => 'true','maxlength'=>'50']) !!}
        @if ($errors->has('name'))
        <span class="error-msg">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
   
    <div class="control-group{{ $errors->has('sport_id') ? ' has-error' : '' }}">
        {!! Form::label('sport', 'Sport', ['class'=>'form-label']) !!}
        {!! Form::select('sport_id',$sports_all,$id->sport_id ?? null, ['class'=>'form-field','readonly' => 'true','placeholder' => 'Select Sports','id'=>'sport']) !!}
        @if ($errors->has('sport_id'))
        <span class="form-error" role="alert">
            <strong>{{ $errors->first('sport_id') }}</strong>
        </span>
        @endif
    </div>
    
      <div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label('email', 'Email', ['class'=>'form-label',]) !!}
        {!! Form::email('email',$id->email, ['class'=>'form-field','readonly' => 'true']) !!}
        @if ($errors->has('email'))
        <span class="error-msg">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>
    
     <div class="control-group{{ $errors->has('dob') ? ' has-error' : '' }}">
        {!! Form::label('dob', 'Date', ['class'=>'form-label']) !!}
        {!! Form::text('dob',$id->date ?? null, ['class'=>'form-field','disabled' => 'disabled','id'=>'dob','autocomplete'=>'off']) !!}
        @if ($errors->has('dob'))
        <span class="error-msg">
            <strong>{{ $errors->first('dob') }}</strong>
        </span>
        @endif
    </div>
    
    <div class="control-group{{ $errors->has('number') ? ' has-error' : '' }}">
        {!! Form::label('number', 'Mobile', ['class'=>'form-label',]) !!}
        {!! Form::text('number', $id->number, ['class'=>'form-field','readonly' => 'true','maxlength'=>'10']) !!}
        @if ($errors->has('number'))
        <span class="error-msg">
            <strong>{{ $errors->first('number') }}</strong>
        </span>
        @endif
    </div>
    
       <div class="control-group{{ $errors->has('message') ? ' has-error' : '' }}">
        {!! Form::label('message','Message', ['class'=>'form-label']) !!}
        {!! Form::textarea('message',$id->message,['class' => 'form-field','readonly' => 'true']); !!}
        @if ($errors->has('message'))
        <span class="error-msg">
            <strong>{{ $errors->first('message') }}</strong>
        </span>
        @endif
    </div>
    
    <div class="control-group{{ $errors->has('admin_comments') ? ' has-error' : '' }}">
        {!! Form::label('admin_comments','Admin Comments', ['class'=>'form-label']) !!}
        {!! Form::textarea('admin_comment',null,['class' => 'form-field']); !!}
        @if ($errors->has('admin_comments'))
        <span class="error-msg">
            <strong>{{ $errors->first('admin_comments') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group">
        {!! Form::label('previous_comments','Previous Comments', ['class'=>'form-label']) !!}
        @if(!empty($id->admin_comments))
        @php $comments=json_decode($id->admin_comments,true); @endphp
        @foreach($comments as $key => $comment)
        @if($comment['id']==$id->id)
        
        <strong>Comment: </strong>{{ $comment['comments']}}<br>
        <strong>Posted By: </strong>{{ ($comment['admin_name'])}}<br>
        
        
        @endif
        @endforeach
        
        @endif
    </div>
    
    
    <div class="control-group">
        <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white" id="submit">
        <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
        
    </div>
</div>
<script>
var dateToday = new Date();
$( "#dob" ).datepicker({
   dateFormat: 'yy-mm-dd',
               changeMonth: true,
            changeYear: true,
            showButtonPanel: true,

});

</script>