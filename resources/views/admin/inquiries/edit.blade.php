@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Edit Inquiries Form</h3>
<section class="box">
    {!! Form::model($id, ['route' => ['admin.inquiry.update', $id->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    @include('admin.inquiries._form',['submitBtnText' => 'Update'])
    {!! Form::close() !!}
</section>
@endsection