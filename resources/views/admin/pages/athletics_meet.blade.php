@inject('arrays','App\Http\Utilities\Arrays')
@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Dashboard</h3>
<section class="box">
  <div class="box-header">
    <p class="box-title">Registration</p>
  </div>
  <div class="box-body">
    {!! Form::open(['route' => 'admin.athletics.meet.search', 'files' => 'true', 'method' => 'GET', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    <div class="row">
      
      <div class="col-md-3">
        <div class="control-group">
          {!! Form::text('keyword',$keyword ?? null, ['class'=>'form-field','placeholder' => 'Search By Keyword']) !!}
        </div>
      </div>
      
    <div class="col-md-6">
      <button type="submit" class="btn --btn-small --btn-primary">Search</button>
    </div>
  </div>
    
    {!! Form::close() !!}
    {!! Form::open(['route' => 'admin.export.data', 'method' => 'POST', 'enctype'=>"multipart/form-data"]) !!}
    <button class="btn --btn-small --btn-primary" type="submit" name="submit" value="5">Export Data</button>
    {!! Form::close() !!}
    <table class="table --bordered --hover">
      <thead>
        <tr>
          <th><center>S No</center></th>
          <th>User Details</th>
          <th><center>Event Dates</center></th>
          <th><center>Message</center></th>
          <th><center>Form Type</center></th>
          <th><center>Admin Comments</center></th>
          <th><center>Created At</center></th>
          <th><center>Action</center></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($signUps as $key=>$signUp)
        
        <tr>
          <td><center>{{ $signUps->firstItem() + $key }}</center></td>
          <td>
            
            <strong>User Name:</strong> {{ $signUp->user->name }} <br>
            <strong>Email: </strong> {{ $signUp->user->email }} <br>
            <strong>Contact: </strong> {{ $signUp->user->mobile_no }} <br>
             <strong>Nationality: </strong> {{ $signUp->user->nationality }} <br>
            <strong>Player Name: </strong> @if(!empty($signUp->player_id)){{ $signUp->player->name }}@endif <br>
            <strong>Player DOB: </strong> @if(!empty($signUp->player_id)){{ $signUp->player->dob }} @endif
            
            
          </td>
          <td><center>@if(!empty($signUp->event_date))
            @php $event_dates=json_decode($signUp->event_date,true); @endphp
            
            
            @foreach($event_dates as $key => $date)
            
            {{ $date['eventDate']}}<br>
            
            @endforeach
            @endif
          </center></td>
          <td><center>@if(!empty($signUp->events))
            @php $events=json_decode($signUp->events,true); @endphp
            
            
            @foreach($events as $key => $event)
            
            {{ $event['eventName']}}<br>
            
            @endforeach
            @endif
          </center></td>
          <td><center>{{"Athletes Classic Meets Form"}}</center></td>
          <td><center>
            @if(!empty($signUp->admin_comments))
            @php $comments=json_decode($signUp->admin_comments,true); @endphp
            
            
            @foreach($comments as $key => $comment)
            @if($comment['id']==$signUp->id)
            {{ $comment['comments']}}
            <b>Posted By:</b>{{ $comment['admin_name']}}<br>
            <b>Updated At:</b>{{$comment['posted_on']}}<br>
            @endif
            @endforeach
            
            @endif
          </center></td>
          <td><center>{{  \Carbon\Carbon::parse($signUp->created_at)->format('d/m/Y H:i')}}</center></td>
          <td><center>
            <a href="{{ route('admin.signUps.edit',[$signUp->id]) }}" class="btn --btn-small bg-secondary fc-white">Edit</a>
            <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $signUp->id }}" id="deleteItem">Delete</button>
          </td>
        </tr>
        @endforeach
      </tbody>
      
    </table>
    <ul class="pagination">
      {{$signUps->links()}}
    </ul>
  </div>
</section>
<script type="text/javascript">
var dateToday = new Date();
$( "#date_of_birth" ).datepicker({
});
</script>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url("/athletics-meet")}}';
$(document).ready(function () {

$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));

$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection