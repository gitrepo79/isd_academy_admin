@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Dashboard</h3>
<section class="box">
    <div class="box-header">
        <p class="box-title">Registration</p>
    </div>
    <div class="box-body">
        {!! Form::open(['route' => 'admin.export.all.data', 'method' => 'POST', 'enctype'=>"multipart/form-data"]) !!}
        <button class="btn --btn-small --btn-primary" type="submit" name="submit" value="6">Export All Data</button>
        {!! Form::close() !!}
        <br>
        {!! Form::open(['route' => 'admin.signUps.search', 'files' => 'true', 'method' => 'GET', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
        <div class="row">
            
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('name',$request->name ?? null, ['class'=>'form-field','placeholder' => 'Search By Name']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('email',$request->email ?? null, ['class'=>'form-field','placeholder' => 'Search By Email']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('mobile',$request->mobile ?? null, ['class'=>'form-field','placeholder' => 'Search By Mobile']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::select('type',$types,$request->type ?? null, ['class'=>'form-field','placeholder' => 'Search By Form Type']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('date_of_requirement',$request->date_of_requirement ?? null, ['class'=>'form-field','id'=>'date_of_requirement', 'placeholder'=>'Player Date of Requirement','autocomplete'=>'off']) !!}
                </div>
            </div>
        </div>
        <button type="submit" class="btn --btn-small --btn-primary">Search</button>
        <a href="{{route('admin.dashboard')}}"><button type="button" class="btn --btn-small --btn-danger">Clear</button></a>
        {!! Form::close() !!}
        {!! Form::open(['route' => 'admin.export.data', 'method' => 'POST', 'enctype'=>"multipart/form-data"]) !!}
        <button class="btn --btn-small --btn-primary" type="submit" name="submit" value="1">Export Venu Hire Registration Data</button>
        <button class="btn --btn-small --btn-primary" type="submit" name="submit" value="2">Export Academy Registration Data</button>
        <button class="btn --btn-small --btn-primary" type="submit" name="submit" value="3">Export Contact Form Data</button>
        {!! Form::close() !!}
        
        <div class="table-responsive">
            <table class="table --bordered --hover">
                <thead>
                    <tr>
                        <th width="5%"><center>S No</center></th>
                        <th width="15%">User Details</th>
                        <th width="40%"><center>Message</center></th>
                        <th width="30%"><center>Admin Comments</center></th>
                        <th width="10%"><center>Created At</center></th>
                        <th width="10%"><center>Action</center></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($signUps as $key=>$signUp)
                    
                    <tr>
                        <td><center>{{ $signUps->firstItem() + $key }}</center></td>
                        <td>
                            <strong>Sports: </strong>
                            @if(!empty($signUp->sport_id))
                            {{$signUp->sport->name}}
                            @endif <br>
                            
                            <strong>User Name:</strong> {{ $signUp->user->name }} <br>
                            <strong>Email: </strong> {{ $signUp->user->email }} <br>
                            <strong>Contact: </strong> {{ $signUp->user->mobile_no }} <br>
                           
                            <strong>Player Name: </strong> @if(!empty($signUp->player_id)){{ $signUp->player->name }}@endif <br>
                            <strong>Player DOB: </strong> @if(!empty($signUp->player_id)){{ $signUp->player->dob }}<br>

                            <strong>Player Type: </strong> @if($signUp->is_adult == 1 && $signUp->sport_id==4){{"Adult" }} @elseif($signUp->is_adult == 1 && $signUp->sport_id==5){{"Adult" }}@endif
                            @endif <br>
                            <strong>Source: </strong> {{ $signUp->source }}<br>
                            <strong>Form Type: </strong>
                            @if(!empty($signUp->form_type_id))
                            {{$signUp->formType->name}}
                            @endif <br>
                            @if($signUp->form_type_id==TYPE_VENUE)
                            <strong>Date of Requirement: </strong>
                            {{ $signUp->date_of_requirment}}
                             @endif
                        </td>
                        
                        <td><center>{{ $signUp->message }}</center></td>
                        <td><center>
                            @if(!empty($signUp->admin_comments))
                            @php $comments=json_decode($signUp->admin_comments,true); @endphp
                            
                            
                            @foreach($comments as $key => $comment)
                            @if($comment['id']==$signUp->id)
                            {{ $comment['comments']}} <br>
                            <b>Posted By: </b>{{"Admin"}}<br>
                            <b>Updated At: </b>{{$comment['posted_on']}}<br>
                            @endif
                            @endforeach
                            
                            @endif
                        </center></td>
                        <td><center>
                            <p>
                                {{  \Carbon\Carbon::parse($signUp->created_at)->format('d/m/Y H:i')}}
                            </p>
                            </center>
                        </td>
                        <td><center>
                            <a href="{{ route('admin.signUps.edit',[$signUp->id]) }}" class="btn --btn-small bg-secondary fc-white">Edit</a>
                            <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $signUp->id }}" id="deleteItem">Delete</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                
            </table>
        </div>
        <ul class="pagination">
            @if(isset($request))
            {{ $signUps->appends(Request::all())->links() }}
            @else
            {{$signUps->links()}}
            @endif
            
        </ul>
    </div>
</section>
<script type="text/javascript">
var dateToday = new Date();
$( "#date_of_requirement" ).datepicker({
});
</script>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url("/admin") }}';
$(document).ready(function () {

$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));

$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection