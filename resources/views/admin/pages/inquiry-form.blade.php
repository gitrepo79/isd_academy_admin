@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Acedemy Inquiries </h3>
<section class="box">
    
    
  <div class="box-body">
    {!! Form::open(['route' => 'admin.inquiry.search', 'files' => 'true', 'method' => 'GET', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
        <div class="row">
            
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('name',$name ?? null, ['class'=>'form-field','placeholder' => 'Search By Name']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('email',$email ?? null, ['class'=>'form-field','placeholder' => 'Search By Email']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('mobile',$mobile ?? null, ['class'=>'form-field','placeholder' => 'Search By Mobile']) !!}
                </div>
            </div>
         
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('date',$date ?? null, ['class'=>'form-field','id'=>'date_of_requirement', 'placeholder'=>'Search By Date','autocomplete'=>'off']) !!}
                </div>
            </div>

        </div>

        <button type="submit" class="btn --btn-small --btn-primary">Search</button>
        {!! Form::close() !!}
        {!! Form::open(['route' => 'admin.tennis.export.data', 'method' => 'POST', 'enctype'=>"multipart/form-data"]) !!}
        <button class="btn --btn-small --btn-primary" type="submit" name="submit" value={{TYPE_VENUE}}>Export Venu Hire Registration Data</button>
        <button class="btn --btn-small --btn-primary" type="submit" name="submit" value={{TYPE_ACADEMY}}>Export Academy Registration Data</button>
        <!-- <button class="btn --btn-small --btn-primary" type="submit" name="submit" value="3">Export Contact Form Data</button>-->
        {!! Form::close() !!} 
    
    
    
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
  
    <table class="table --bordered --hover">
      <thead>
        <tr>
          <th width="5%"><center>S No</center></th>
          <th><center>Sport</center></th>
          <th>User Detail</th>
          <th><center>Description</center></th>
          <th><center>Date</center></th>
          <th><center>Admin Comments</center></th>
          <th><center>Action</center></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($inquires as $key=>$inquires)
        
        <tr>
          <td><center>{{ $inquires->id }}</center></td>
          <td>
            @if(!empty($inquires->sport_id)){{ $inquires->sport->name }}@endif</td>
          <td>
            <strong>Name:</strong> {{ $inquires->name }} <br>
            <strong>Email: </strong> {{ $inquires->email }} <br>
            <strong>Contact: </strong> {{ $inquires->number }} <br>
            </center>
            </td>
            <td>
                {{ $inquires->message }}
          </td>
          <td><center>
            <p>
              {{  \Carbon\Carbon::parse($inquires->date)->format('d/m/Y')}}
            </p>
            </center>
          </td>
          <td>
            @if(!empty($inquires->admin_comments))
            @php $comments=json_decode($inquires->admin_comments,true); @endphp
            
            @foreach($comments as $key => $comment)
            @if($comment['id']==$inquires->id)
            <b>Comment: </b>{{ $comment['comments']}}<br>
            <b>Posted By: </b>{{"Admin"}}<br>
            <br>
            @endif
            @endforeach
            
            @endif
          </td>
          <td><center>
            <a href="{{ route('admin.inquiry.edit',[$inquires->id]) }}" class="btn --btn-small bg-secondary fc-white">Edit</a>
            <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $inquires->id }}" id="deleteItem">Delete</button>
          </td>
        </tr>
        @endforeach
      </tbody>
      
    </table>
    <ul class="pagination">
    </ul>
  </div>
</section>
<script type="text/javascript">
var dateToday = new Date();
$( "#date_of_requirement" ).datepicker({
       dateFormat: 'yy-mm-dd',
               changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
});
</script>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url()->current() }}';
$(document).ready(function () {

$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));

$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection