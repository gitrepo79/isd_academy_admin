@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">PopUp SignUps </h3>
<section class="box">
  <div class="box-header">
    <p class="box-title">Registration</p>
  </div>
  <div class="box-body">
    
    <table class="table --bordered --hover">
      <thead>
        <tr>
          <th width="5%"><center>S No</center></th>
          <th>Parent  Details</th>
          <th>Player Details</th>
          <th><center>Admin Comments</center></th>
          <th><center>Created At</center></th>
          <th><center>Action</center></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($signUps as $key=>$signUp)
        
        <tr>
          <td><center>{{ $signUps->firstItem() + $key }}</center></td>
          <td>
            <strong>Sports: </strong>
            @if(!empty($signUp->sport_id)){{ $signUp->sport->name }}@endif
            <br>
            <strong>User Name:</strong> {{ $signUp->user->name }} <br>
            <strong>Email: </strong> {{ $signUp->user->email }} <br>
            <strong>Contact: </strong> {{ $signUp->user->mobile_no }} <br>
            </center></td>
            <td><center>
            <strong>Player Name: </strong> @if(!empty($signUp->player_id)){{ $signUp->player->name }}@endif <br>
            <strong>Player DOB: </strong> @if(!empty($signUp->player_id)){{ $signUp->player->dob }} @endif
          </center>
          </td>
          
          <td><center>
            @if(!empty($signUp->admin_comments))
            @php $comments=json_decode($signUp->admin_comments,true); @endphp
            
            
            @foreach($comments as $key => $comment)
            @if($comment['id']==$signUp->id)
            {{ $comment['comments']}}<br>
            <b>Posted By:</b>{{"Admin"}}<br>
            <b>Updated At:</b>{{$comment['posted_on']}}<br>
            @endif
            @endforeach
            
            @endif
          </center></td>
          <td><center>
            <p>
              {{  \Carbon\Carbon::parse($signUp->created_at)->format('d/m/Y H:i')}}
            </p>
            </center>
          </td>
          <td><center>
            <a href="{{ route('admin.signUps.edit',[$signUp->id]) }}" class="btn --btn-small bg-secondary fc-white">Edit</a>
            <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $signUp->id }}" id="deleteItem">Delete</button>
          </td>
        </tr>
        @endforeach
      </tbody>
      
    </table>
    <ul class="pagination">
      {{$signUps->links()}}
    </ul>
  </div>
</section>
<script type="text/javascript">
var dateToday = new Date();
$( "#date_of_requirement" ).datepicker({
});
</script>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url("/pop-up") }}';
$(document).ready(function () {

$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));

$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection