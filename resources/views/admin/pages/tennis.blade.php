@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Tennis</h3>
<section class="box">
    <div class="box-header">
        <p class="box-title">Registration</p>
    </div>
    <div class="box-body">
        {!! Form::open(['route' => 'admin.tennis.search', 'files' => 'true', 'method' => 'GET', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
        <div class="row">
            
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('name',$name ?? null, ['class'=>'form-field','placeholder' => 'Search By Name']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('email',$email ?? null, ['class'=>'form-field','placeholder' => 'Search By Email']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('mobile',$mobile ?? null, ['class'=>'form-field','placeholder' => 'Search By Mobile']) !!}
                </div>
            </div>
            <!-- <div class="col-md-3">
                <div class="control-group">
                    {!! Form::select('type',$types,$type ?? null, ['class'=>'form-field','placeholder' => 'Search By Form Type']) !!}
                </div>
            </div> -->
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('date_of_requirement',$date_of_requirement ?? null, ['class'=>'form-field','id'=>'date_of_requirement', 'placeholder'=>'Player Date of Requirement','autocomplete'=>'off']) !!}
                </div>
            </div>

        </div>

        <button type="submit" class="btn --btn-small --btn-primary">Search</button>
        {!! Form::close() !!}
        {!! Form::open(['route' => 'admin.tennis.export.data', 'method' => 'POST', 'enctype'=>"multipart/form-data"]) !!}
        <button class="btn --btn-small --btn-primary" type="submit" name="submit" value={{TYPE_VENUE}}>Export Venu Hire Registration Data</button>
        <button class="btn --btn-small --btn-primary" type="submit" name="submit" value={{TYPE_ACADEMY}}>Export Academy Registration Data</button>
        <!-- <button class="btn --btn-small --btn-primary" type="submit" name="submit" value="3">Export Contact Form Data</button>-->
        {!! Form::close() !!} 

        <table class="table --bordered --hover">
            <thead>
                <tr>
                    <th width="5%"><center>S No</center></th>
                    <th width="15%">User Details</th>
                    <th width="5%"><center>Date of Requirements</center></th>
                    <th width="25%"><center>Message</center></th>
                    <th width="10%"><center>Form Type</center></th>
                    <th width="30%"><center>Admin Comments</center></th>
                    <th width="10%"><center>Created At</center></th>
                    <th width="10%"><center>Action</center></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($signUps as $key=>$signUp)
                 
                <tr>
                    <td><center>{{ $signUps->firstItem() + $key }}</center></td>
                    <td>
                        <strong>Sports: </strong>
                        @if(!empty($signUp->sport_id)){{ $signUp->sport->name }}@endif  
                         <br>

                        <strong>User Name:</strong> {{ $signUp->user->name }} <br>
                        <strong>Email: </strong> {{ $signUp->user->email }} <br>
                        <strong>Contact: </strong> {{ $signUp->user->mobile_no }} <br>
                        @if($signUp->form_type_id==TYPE_ACADEMY)
                        <strong>Player Name: </strong> @if(!empty($signUp->player_id)){{ $signUp->player->name }}@endif <br>
                        <strong>Player DOB: </strong> @if(!empty($signUp->player_id)){{ $signUp->player->dob }} @endif
                        <strong>Program: </strong> @if($signUp->program_id != 0){{ $signUp->program->name }}@endif <br>
                        <strong>Player Type: </strong> @if($signUp->is_adult == 1 && $signUp->sport_id==4){{"Adult" }} @elseif($signUp->is_adult == 1 && $signUp->sport_id==5){{"Adult" }}@endif 
                        @endif 
                        @if($signUp->form_type_id==TYPE_ATHLETES_POPUP_FORM)
                        <strong>Athlete dob: </strong> @if(!empty($signUp->player_id)){{ $signUp->player->dob }}@endif 

                        @endif
                        
                    </td>
                    <td><center>{{ $signUp->date_of_requirment}}</center></td>
                    <td><center>{{ $signUp->message }}</center></td>
                    <td><center>@if(!empty($signUp->form_type_id==TYPE_VENUE))
                        {{"Venu Hire"}}
                    @elseif(!empty($signUp->form_type_id==TYPE_ACADEMY)){{"Academy Registration"}} @elseif(!empty($signUp->form_type_id==TYPE_CONTACT)){{"Contact Form"}} 
                    @elseif(!empty($signUp->form_type_id==TYPE_ATHLETES_POPUP_FORM)){{"Athletes_PopUp Form"}} @endif 
                  </center></td>
                    <td><center>

                      @if(!empty($signUp->admin_comments))
                     @php $comments=json_decode($signUp->admin_comments,true); @endphp
        
                       
                       @foreach($comments as $key => $comment)
                       @if($comment['id']==$signUp->id)
                          {{ $comment['comments']}}
                           <b>Posted By:</b>{{"Admin"}}<br>
                           <b>Updated At:</b>{{$comment['posted_on']}}<br>
                          @endif
                        @endforeach
                        
                        @endif
                    </center></td>
                     <td><center>
                            <p>
                                {{  \Carbon\Carbon::parse($signUp->created_at)->format('d/m/Y H:i')}}
                            </p>
                         </center>
                     </td>

                    <td><center>
                        <a href="{{ route('admin.signUps.edit',[$signUp->id]) }}" class="btn --btn-small bg-secondary fc-white">Edit</a>

                        <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $signUp->id }}" id="deleteItem">Delete</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        
        </table>

        <ul class="pagination">
            {{$signUps->links()}}
        </ul>
    </div>
</section>


<script type="text/javascript">
var dateToday = new Date();
$( "#date_of_requirement" ).datepicker({
});
</script>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
  {!! Form::close() !!}
  <script>
  var deleteResourceUrl = '{{ url("/tennis")}}';
  $(document).ready(function () {
  
  $('.deleteItem').click(function (e) {
  e.preventDefault();
  var deleteId = parseInt($(this).data('deleteId'));
  
  $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
  if (confirm('Are you sure?')) {
  $('#deleteForm').submit();
  }
  });
  });
  </script>
@endsection