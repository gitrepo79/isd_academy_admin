
<div class="box-body">
    <div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Name', ['class'=>'form-label',]) !!}
        {!! Form::text('name',$signUp->user->name, ['class'=>'form-field','required' => 'required','maxlength'=>'50']) !!}
        @if ($errors->has('name'))
        <span class="error-msg">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
   
    <div class="control-group{{ $errors->has('sport_id') ? ' has-error' : '' }}">
        {!! Form::label('sport', 'Sport', ['class'=>'form-label']) !!}
        {!! Form::select('sport_id',$sports_all,$sport_id ?? null, ['class'=>'form-field','required' => 'required','placeholder' => 'Select Sports','id'=>'sport']) !!}
        @if ($errors->has('sport_id'))
        <span class="form-error" role="alert">
            <strong>{{ $errors->first('sport_id') }}</strong>
        </span>
        @endif
    </div>
    
    @if($signUp->form_type_id==TYPE_ACADEMY || $signUp->form_type_id==TYPE_POPUP_FORM)
    <div class="control-group{{ $errors->has('player_name') ? ' has-error' : '' }}">
        {!! Form::label('player_name', 'Player Name', ['class'=>'form-label']) !!}
        {!! Form::text('player_name',$signUp->player->name ?? null, ['class'=>'form-field','required' => 'required','maxlength'=>'50']) !!}
        @if ($errors->has('player_name'))
        <span class="error-msg">
            <strong>{{ $errors->first('player_name') }}</strong>
        </span>
        @endif
    </div>
    @endif
    @if($signUp->form_type_id==TYPE_ACADEMY  || $signUp->form_type_id==TYPE_POPUP_FORM || 
    $signUp->form_type_id==TYPE_ATHLETES_POPUP_FORM)
    <div class="control-group{{ $errors->has('dob') ? ' has-error' : '' }}">
        {!! Form::label('dob', 'Player DOB', ['class'=>'form-label']) !!}
        {!! Form::text('dob',$signUp->player->dob ?? null, ['class'=>'form-field','required' => 'required','id'=>'dob','autocomplete'=>'off']) !!}
        @if ($errors->has('dob'))
        <span class="error-msg">
            <strong>{{ $errors->first('dob') }}</strong>
        </span>
        @endif
    </div>

    @endif
    @if($signUp->form_type_id == TYPE_VENUE)
    <div class="control-group{{ $errors->has('date_of_requirment') ? ' has-error' : '' }}">
        {!! Form::label('date_of_requirment', 'Date Of Requirment', ['class'=>'form-label']) !!}
        {!! Form::text('date_of_requirment', null, ['class'=>'form-field','id'=>'date_of_requirment']) !!}
        @if ($errors->has('date_of_requirment'))
        <span class="error-msg">
            <strong>{{ $errors->first('date_of_requirment') }}</strong>
        </span>
        @endif
    </div>
    @endif
    <div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label('email', 'Email', ['class'=>'form-label',]) !!}
        {!! Form::email('email',$signUp->user->email, ['class'=>'form-field','required' => 'required']) !!}
        @if ($errors->has('email'))
        <span class="error-msg">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>
    <div class="control-group{{ $errors->has('mobile_no') ? ' has-error' : '' }}">
        {!! Form::label('mobile', 'Mobile', ['class'=>'form-label',]) !!}
        {!! Form::text('mobile', $signUp->user->mobile_no, ['class'=>'form-field','required' => 'required','maxlength'=>'10']) !!}
        @if ($errors->has('mobile'))
        <span class="error-msg">
            <strong>{{ $errors->first('mobile') }}</strong>
        </span>
        @endif
    </div>
 
    <div class="control-group{{ $errors->has('form_type_id') ? ' has-error' : '' }}">
        {!! Form::label('form_type_id','Form Type Id', ['class'=>'form-label']) !!}
        {!! Form::select('form_type_id',$types,null,['class' => 'form-field']); !!}
        @if ($errors->has('form_type_id'))
        <span class="error-msg">
            <strong>{{ $errors->first('form_type_id') }}</strong>
        </span>
        @endif
    </div>
    <div class="control-group{{ $errors->has('admin_comments') ? ' has-error' : '' }}">
        {!! Form::label('admin_comments','Admin Comments', ['class'=>'form-label']) !!}
        {!! Form::textarea('admin_comment',null,['class' => 'form-field']); !!}
        @if ($errors->has('admin_comments'))
        <span class="error-msg">
            <strong>{{ $errors->first('admin_comments') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group"">
        {!! Form::label('previous_comments','Previous Comments', ['class'=>'form-label']) !!}
        @if(!empty($signUp->admin_comments))
        @php $comments=json_decode($signUp->admin_comments,true); @endphp
        @foreach($comments as $key => $comment)
        @if($comment['id']==$signUp->id)
        <strong>{{ $signUp->admin($comment['admin_name'])}}</strong> ({{$comment['posted_on']}}):<br>
        {{ $comment['comments']}}<br>
        @endif
        @endforeach
        
        @endif
    </div>
    
    <div class="control-group">
        <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white" id="submit">
        <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
        
    </div>
</div>
<script>
var dateToday = new Date();
$( "#dob" ).datepicker({

});
$( "#date_of_requirment" ).datepicker({

});

</script>