@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Edit Sign Up Form</h3>
<section class="box">
    {!! Form::model($signUp, ['route' => ['admin.signUps.update', $signUp->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    @include('admin.signups._form',['submitBtnText' => 'Update'])
    {!! Form::close() !!}
</section>
@endsection