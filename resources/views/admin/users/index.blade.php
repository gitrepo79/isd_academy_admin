<?php $no = 1; ?>
@extends('layouts.admin')

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Seasons
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Season</li>
      </ol>
    </section>
    <!-- Main content -->
  <section class="content">

  <div class="row">
        <div class="col-xs-12">
           {{-- session message --}}
  @if(Session::has('success') || Session::has('error') || Session::has('info'))
    <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                    @elseif(Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <strong>Error!</strong> {{ Session::get('error') }}
                    </div>
                    @elseif(Session::has('info'))
                        <div class="alert alert-info" role="alert">
                            <strong>Info!</strong> {{ Session::get('info') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
       @endif
  {{--  End of message --}}
          <div class="box">
            <div class="box-header">
             <a href="{{route('admin.seasons.create')}}"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>Add Season</button></a>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th><center>S NO</center></th>
                  <th><center>Season</center></th>
                  <th><center>Start Date</center></th>
                  <th><center>End Date</center></th>
                  <th><center>Is Active</center></th>
                  <th><center>Terms</center></th>
                  <th><center>Action</center></th>
                 </tr>
                </thead>
                <tbody>
               @foreach ($seasons as $season)
                <tr>
                  <td><center>{{ $no++ }}</center></td>
                  <td><center>{{ $season->name }}</center></td>
                  <td><center>{{ $season->start_date->format('d/m/Y')}}</center></td>
                  <td><center>{{ $season->end_date->format('d/m/Y')}}</center></td>
                  <td><center>  
                  @if($season->is_active)
                  <p>Active</p>
                  @else
                  <p>InActive</p>
                  @endif</center></td>
                  <td><center><a href="{{ route('admin.season.terms.index',[$season->id]) }}">Terms</a></center></td>
                  <td><center><a href="{{ route('admin.seasons.edit',[$season->id]) }}"><button type="button" class="btn btn-warning btn-sm">Edit</button></a>
                  <button type="button" class="deleteItem btn btn-danger btn-sm" data-delete-id="{{ $season->id }}" id="deleteItem">Delete</button></td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                 
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
    {!! Form::close() !!}

 <script>
        var deleteResourceUrl = '{{ url()->current() }}';
        $(document).ready(function () {
          
            $('.deleteItem').click(function (e) {
                e.preventDefault();
                var deleteId = parseInt($(this).data('deleteId'));
               
                $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);

                if (confirm('Are you sure?')) {
                    $('#deleteForm').submit();
                }
            });

        });
    </script>    
@endsection


