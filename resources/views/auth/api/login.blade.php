@extends('layouts.app')
@section('pageClass', 'athleticpg inner-page')
@section('title', 'Venue Registration')
@section('content')
<section class="inner-page-section" style="background-image: url('{{asset('assets-web/images/banners/default-banner.jpg')}}');">
    <div class="inner-content --registration">
    </div>
    <div class="vertical-heading">
        <h2 class="title">Login Admin</h2>
    </div>
    <div class="content-section">
        <article class="container-wrapper">
            <div class="row">
                <section class="col-xl-5 offset-xl-4 col-lg-6 offset-lg-3">
                    
                    <div class="box --registration-box">
                        <form class="default-form --registration-form">
                            <div class="control-group">
                                <input id="email" type="email" class="form-field @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required placeholder="{{ __('E-Mail Address') }}" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="control-group">
                                <input id="password" type="password" class="form-field @error('password') is-invalid @enderror" name="password" required  placeholder="{{ __('Password') }}">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="control-group">
                                <button type="button" id="login" class="btn --btn-primary">
                                {{ __('Submit') }}
                                </button>
                            </div>
                            <form>
                            </div>
                        </div>
                    </article>
                </div>
                <script type="text/javascript">
                $(document).ready(function(){
                
                $('#login').click(function(){
                
                var email=$('#email').val();
                var password=$("#password").val();
                $.ajax({
                type: 'POST',
                url: 'https://bookings.isddubai.com/api/v1/users/login',
                data: {email:email,password:password},
                dataType: 'json',
                success: function (data) {
                console.log(data);
                var status = data.status;
                var errormsg = data.msg;
                var id= data.id;
                if(status==200)
                {
                  url= '{{ route("user.bookings",":id")}}';
                  url = url.replace(':id', id);
                  window.location.href = url;
                }
                else if(status==500){
                alert(errormsg);
                }
                },
                error: function (data) {
                console.log(data);
                }
                });
                });
                });
                </script>
            </section>
            @endsection