@extends('layouts.app')
@section('pageClass', 'athleticpg inner-page')
@section('title', 'Venue Registration')
@section('content')

<section class="sec-padding">
    <div class="container-wrapper">
        <h2 class="maintitle">Login Admin</h2>

        <div class="content-section">
            
            <div class="row">
                <section class="col-xl-5 col-lg-6">
                    
                    <div class="box --registration-box">
                        {!! Form::open(['route' => 'login', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form --registration-form"]) !!}
                        <div class="control-group">
                            <input id="email" type="email" class="form-field @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required placeholder="{{ __('E-Mail Address') }}" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="control-group">
                            <input id="password" type="password" class="form-field @error('password') is-invalid @enderror" name="password" required  placeholder="{{ __('Password') }}">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="control-group">
                            <button type="submit" class="btn --btn-primary">
                            {{ __('Submit') }}
                            </button>
                        </div>
                       
                        {!! Form::close() !!}
                    </div>
                </section>
            </div>

        </div>
    </div>
</section>


@endsection