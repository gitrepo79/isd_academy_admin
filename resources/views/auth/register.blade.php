@extends('layouts.app')
@section('pageClass', 'athleticpg inner-page')
@section('title', 'Registration')
@section('content')
<section class="inner-page-section" style="background-image: url('{{asset('assets-web/images/banners/default-banner.jpg')}}');">
    <div class="inner-content --registration">
    </div>
    <div class="vertical-heading">
        <h2 class="title">Registration</h2>
    </div>
    <div class="content-section">
        <article class="container-wrapper">
            <div class="row">
                <section class="col-xl-5 offset-xl-4 col-lg-6 offset-lg-3">
                    
                    <div class="box --registration-box">
                        <form class="default-form --registration-form">
                            <div class="control-group">
                                <input type="text" class="form-field" name="name" value="{{ old('name') }}" required placeholder="Enter Name" id="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="control-group">
                                <input id="email" type="email" class="form-field @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required placeholder="{{ __('E-Mail Address') }}" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="control-group">
                                <input id="contacts" type="text" class="form-field @error('contacts') is-invalid @enderror" name="contacts" value="{{ old('contacts') }}" required placeholder="{{ __('Contact No') }}" autofocus>
                                @error('contacts')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="control-group">
                                <input id="password" type="password" class="form-field @error('password') is-invalid @enderror" name="password" required  placeholder="{{ __('Password') }}">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="control-group">
                                <input id="password-confirm" type="password" class="form-field" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirmation Password') }}">
                            </div>
                            
                            <div class="control-group">
                                <button type="button" class="btn --btn-primary" id="register">
                                {{ __('Submit') }}
                                </button>
                            </div>

                             <p class="maindesc mb-0 fc-white" >Already Have account?Login Here <a href="/login" class="fc-primary td-underline">Login</a></p>
                            
                        </form>
                    </div>
                </section>
            </div>
        </article>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
    
    $('#register').click(function(){

    var name=$("#name").val();
    var email=$('#email').val();
    var password=$("#password").val();
    var contacts=$('#contacts').val();

   $.ajax({
    type: 'POST',
    url: 'https://bookings.isddubai.com/api/v1/users/register',
    data: {name:name,email:email,password:password,contacts:contacts},
    dataType: 'json',
    success: function (data) {
    console.log(data);
    var status = data.status;
    var errormsg = data.msg;
    var id= data.id;
    if(status==200) 
    {
        var url = '{{ route("user.email.verification", ":id") }}';
        url = url.replace(':id', id);
        window.location.href = url;
    }
    else if(status==500){
      alert(errormsg);
    }
    },
    error: function (data) {
    console.log(data);
    }
    });
    });
    });
    </script>
</section>
@endsection