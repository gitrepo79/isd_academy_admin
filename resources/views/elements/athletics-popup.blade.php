<section class="x-modal --popup-form hidden show">
	<div class="modal-content">
		<a href="javascript:;" class="close-btn modal-close">
			<i class="icon xicon-close"></i>
		</a>
		<div class="inner-content">
			
			{!! Form::open(['route' => 'form.submission', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
			<div class="control-group mb-10">
				{!! Form::label('name', 'Name', ['class'=>'form-label']) !!}
				{!! Form::text('name', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Enter your Name']) !!}
				@if ($errors->has('name'))
				<span class="form-error">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>
			<div class="control-group mb-10">
				{!! Form::label('email', 'Email', ['class'=>'form-label']) !!}
				{!! Form::text('email', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Enter your Email']) !!}
				@if ($errors->has('email'))
				<span class="form-error">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
				@endif
			</div>
			<div class="control-group mb-10">
				{!! Form::label('mobile', 'Number', ['class'=>'form-label']) !!}
				{!! Form::number('mobile', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Enter Phone Number']) !!}
				@if ($errors->has('mobile'))
				<span class="form-error">
					<strong>{{ $errors->first('mobile') }}</strong>
				</span>
				@endif
			</div>
			<div class="control-group mb-10">
				<label class="form-label">Athlete's DOB</label>
				
				{!! Form::text('dob', null, ['class'=>'form-field','required' => 'required','id'=>'dob', 'placeholder'=>'Athletes DOB','autocomplete'=>'off']) !!}
				@if ($errors->has('dob'))
				<span class="form-error">
					<strong>{{ $errors->first('dob') }}</strong>
				</span>
				@endif
			</div>
			<div class="control-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
				<div class="control-group mb-10">
					<div class="g-recaptcha" data-sitekey="6LfHdHgaAAAAAH-4pwDW3GL0ZghugONnNEn-xcSJ"></div>
					@if ($errors->has('g-recaptcha-response'))
					<span class="help-block">
						<strong>{{ $errors->first('g-recaptcha-response') }}</strong>
					</span>
					@endif
				</div>
			</div>
			<div class="control-group mb-0">
				{!! Form::hidden('form_type_id',TYPE_ATHLETES_POPUP_FORM, ['class'=>'form-field']) !!}
				<input type="submit" class="btn --btn-primary" value="Submit">
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</section>