@component('mail::message')
<p>Kindly find the Invoice in attachment.</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
