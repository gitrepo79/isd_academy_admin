<<<<<<< HEAD
@component('mail::message')
<p>Hi,<br/>
A popup form has been submitted with the following details:</p>
<p>Name:{{ $data['full_name'] }}</p>
<p>Email: {{ $data['email'] }}.</p>
<p>Mobile_no: {{ $data['mobile_no'] }}.</p>
<p>Program: {{ $data['program'] }}.</p>
<p>City: {{ $data['city'] }}.</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
