<section class="topbar-nav">

    <nav class="topbar-navigation clearfix">
        <!-- <ul class="unstyled inline float-left">
				<li>
						<a href="#">Administrator</a>
				</li>
		</ul> -->
        <ul class="unstyled inline float-right text-right">
            <li>
                <a href="{{route('logout')}}">
                    <i class="xxicon icon-log-out"></i>
                    Log Out
                </a>
            </li>
        </ul>
    </nav>
</section>
<section class="side-bar">
    <div class="header-logo">
        <img src="/admin-assets-web/images/header-logo.png" alt="" />
    </div>
    <div class="header-user">
        <span class="user-img">
            <img src="/admin-assets-web/images/user.jpg" alt="">
        </span>
        <span class="username">
            Administrator
        </span>
    </div>
    <nav class="primary-navigation">
        <ul class="unstyled">
            <li>
                <a href="{{route('admin.dashboard')}}" class="active">
                    <i class="xxicon icon-home"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.athletics.meet.index')}}">
                    <i class="xxicon icon-home"></i>
                    <span>Athletics Meet</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.tennis.index')}}">
                    <i class="xxicon icon-home"></i>
                    <span>Tennis</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.popup.index')}}">
                    <i class="xxicon icon-home"></i>
                    <span>PopUp SignUps</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.inquiry.index')}}">
                    <i class="xxicon icon-home"></i>
                    <span>Academy Inquiries</span>
                </a>
            </li>
            </li>
            <li>
                <a href="{{route('admin.lead.index')}}">
                    <i class="xxicon icon-home"></i>
                    <span>Contact Leads</span>
                </a>
            </li>
        </ul>
    </nav>
</section>
