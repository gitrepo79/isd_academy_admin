<?php
$route=Request::segment(1);	
?>
<div class="" style="background: black;">
    <div class="container-wrapper">
        <div class="row align-items-center">
            <div class="col-md-12">
                <ul class="unstyled inline social-links text-md-right text-right">
                    <li>
                        <a href="https://www.facebook.com/InspiratusSportsDistrict" target="_blank">
                            <i class="fa fa-facebook icon"></i>
                        </a>
                    </li>
                        <li>
                        <a href="https://www.instagram.com/ISD_DUBAI/" target="_blank">
                            <i class="fa fa-instagram icon"></i>
                        </a>
                    </li>
                 
                </ul>
            </div>
        </div>
    </div>
</div>
<header class="primary-header">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-3">
				@if(!empty($route) && $route=="isd-academy")
					<a href="/" class="header-logo --innerpage animsition-link">
						<img src="/assets-web/images/logos/isddubai-academy-color.svg" alt="">
					</a>
				@else
					<a href="/" class="header-logo --homepage animsition-link">
						<img src="/assets-web/images/logos/isddubai-color.svg" alt="">
					</a>

					<a href="/" class="header-logo --innerpage animsition-link">
						<img src="/assets-web/images/logos/isddubai-color.svg" alt="">
					</a>
				@endif

			</div>
			<div class="col-9 text-right">
				<!-- <div class="register-button">
					<a href="#" class="btn --btn-secondary">Login/register</a>
				</div> -->

				<div class="primary-navigation">

					<nav class="header-navigation">
						<ul class="unstyled">
							<li>
								<a href="/about" class="animsition-link">about</a>
							</li>

							<li>
								<a href="#">academies</a>

								<ul class="unstyled dropdown">
									<li>
										<a href="https://ae.laligacademy.com/" class="animsition-link">Football</a>
									</li>

									<li>
										<a href="/tennisacademy" class="animsition-link">Tennis</a>
									</li>

									<li>
										<a href="/athleticsacademy" class="animsition-link">Athletics</a>
									</li>

									{{-- <li>
										<a href="#">Padel</a>
									</li> --}}
								</ul>
							</li>

							{{--<li>
								<a href="/isd-academy" class="animsition-link">ISD Academy</a>
							</li>--}}

							{{--<li>
								<a href="#">camps</a>

								<ul class="unstyled dropdown">

									<li>
										<a href="/tennisacademy" class="animsition-link">Tennis</a>
									</li>
									
									<li>
										<a href="/athleticsacademycamps" class="animsition-link">Athletics</a>
									</li>

									<li>
										<a href="#">Padel</a>
									</li>
								</ul>
							</li> --}}

							<li>
								<a href="#">venue rentals</a>

								<ul class="unstyled dropdown">
									<li>
										<a href="/football-venuehire" class="animsition-link">Football</a>
									</li>

									<li>
										<a href="/tennis-venuehire" class="animsition-link">Tennis</a>
									</li>

									<li>
										<a href="/athletics-venuehire" class="animsition-link">Athletics</a>
									</li>

								</ul>
							</li>

							<li>
								<a href="/eupepsia" class="animsition-link">eupepsia</a> 
							</li>

							<li>
								<a href="/footlab" class="animsition-link">footlab</a>
							</li>
						</ul>
					</nav>
					
					<!-- Mobile Navigation Button Start-->
					{{-- <div class="mobile-nav-btn d-lg-none">
					    <span class="lines"></span>
					    <span class="lines"></span>
					    <span class="lines"></span>
					    <span class="lines"></span>
					</div> --}}
					<!-- Mobile Navigation Button End-->
				</div>
				

			</div>
		</div>
	</div>
</header>