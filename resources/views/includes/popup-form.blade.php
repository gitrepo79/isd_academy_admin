<?php
$route=Request::segment(1);	
?>
<section class="popup-form x-modal hidden">
	<div class="modal-content">
		<a href="javascript:;" class="close-btn modal-close">
			<i class="icon xicon-close"></i>
		</a>
		<div class="inner-content">
			@if(!empty($route) && $route=="tennisacademy" || $route=="tennisacademy-fees" || $route=="tennisacademy-schedule")
				<h2 class="title fc-white mb-10">Register Now</h2>
				<picture class="d-block mb-30">
					<img src="https://isddubai.com/assets-web/images/tennis-popup-banner.jpg" alt="">
				</picture>
			@elseif(!empty($route) && $route=="athleticsacademy" || $route=="athleticsacademy-fees" || $route=="athleticsacademy-schedule")
				<picture class="d-block mb-30">
					<img src="https://isddubai.com/assets-web/images/athletics-popup-banner.jpg" alt="">
				</picture>
			@else
				<h4 class="title fc-white mb-10">Enquire Now</h4>
				<picture class="d-block mb-30">
					<img src="https://isddubai.com/assets-web/images/popup-banner.jpg" alt="">
				</picture>
			@endif

			{!! Form::open(['route' => 'form.submission', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form --registration-form"]) !!}
			<div class="control-group{{ $errors->has('sport_id') ? ' has-error' : '' }}">
				<select class="form-field" required="required" id="sport" name="sport_id">
					<option value="">Select Academy</option>
					<option value="3">Athletics</option>
					<option value="4">Tennis</option>
				</select>
				{{-- {!! Form::select('sport_id',$sports,$sport_id ?? null, ['class'=>'form-field','required' => 'required','placeholder' => 'Select Sports','id'=>'sport']) !!} --}}
				@if ($errors->has('sport_id'))
				<span class="form-error" role="alert">
					<strong>{{ $errors->first('sport_id') }}</strong>
				</span>
				@endif
			</div>
			
			<div class="control-group{{ $errors->has('player_name') ? ' has-error' : '' }}">
				{!! Form::text('player_name', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Player Name','id'=>'player_name','autocomplete'=>'off','maxlength'=>'50']) !!}
				@if ($errors->has('player_nam'))
				<span class="form-error">
					<strong>{{ $errors->first('player_name') }}</strong>
				</span>
				@endif
			</div>
			
			<div class="control-group{{ $errors->has('dob') ? ' has-error' : '' }}">
				{!! Form::date('dob', null, ['class'=>'form-field','required' => 'required','id'=>'dob', 'placeholder'=>'Player DOB','autocomplete'=>'off']) !!}
				@if ($errors->has('dob'))
				<span class="form-error">
					<strong>{{ $errors->first('dob') }}</strong>
				</span>
				@endif
			</div>
			
			<div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
				{!! Form::text('name', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Parent/Guardian Name', 'id'=>'parent_name','autocomplete'=>'off']) !!}
				@if ($errors->has('name'))
				<span class="form-error">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>
			<div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
				{!! Form::text('email', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Parent/ Guardian Email', 'id'=>'email']) !!}
				@if ($errors->has('email'))
				<span class="form-error">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
				@endif
			</div>
			<div class="control-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
				{!! Form::text('mobile', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Parent/Guardian Mobile','id'=>'mobile','maxlength'=>'10']) !!}
				@if ($errors->has('mobile'))
				<span class="form-error">
					<strong>{{ $errors->first('mobile') }}</strong>
				</span>
				@endif
			</div>

			<div class="control-group{{ $errors->has('source') ? ' has-error' : '' }}">
				<select class="form-field" required="required" id="source" name="source">
					<option value="">How did you hear about us?</option>
		            <option value="Facebook">Facebook</option>
		            <option value="Instagram">Instagram</option>
		            <option value="Google Search Ad">Google Search Ad</option>
		            <option value="Google Banner Image">Google Banner Image</option>
		            <option value="Email Newsletter">Email Newsletter</option>
		            <option value="A Friend">A Friend</option>
		            <option value="Other">Other</option>
				</select>
				{{-- {!! Form::select('source',$sports,$source ?? null, ['class'=>'form-field','required' => 'required','placeholder' => 'Select Sports','id'=>'sport']) !!} --}}
				@if ($errors->has('source'))
				<span class="form-error" role="alert">
					<strong>{{ $errors->first('source') }}</strong>
				</span>
				@endif
			</div>


			<div class="control-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
				<div class="g-recaptcha" data-sitekey="{{env('NOCAPTCHA_SITEKEY')}}"></div>
				@if ($errors->has('g-recaptcha-response'))
				<span class="help-block">
					<strong>{{ $errors->first('g-recaptcha-response') }}</strong>
				</span>
				@endif
				
			</div>
			
			
			<div class="control-group mb-10">
				<button type="submit" class="btn --btn-secondary">
				{{ __('Submit') }}
				</button>
			</div>
			{!! Form::hidden('form_type_id',TYPE_POPUP_FORM, ['class'=>'form-field']) !!}
			{!! Form::close() !!}
		</div>
	</div>
</section>


@if(!empty($route) && $route=="tennisacademy" || $route=="tennisacademy-fees" || $route=="tennisacademy-schedule")

	<a href="https://wa.me/971582608408" class="whatsapp" target="_blank">
		<img src="{{ asset('assets-web/images/whatsapp-icon.png')}}" alt="">
	</a>

@elseif(!empty($route) && $route=="athleticsacademy" || $route=="athleticsacademy-fees" || $route=="athleticsacademy-schedule")

	<a href="https://wa.me/971582858239" class="whatsapp" target="_blank">
		<img src="{{ asset('assets-web/images/whatsapp-icon.png')}}" alt="">
	</a>

@else
	
@endif