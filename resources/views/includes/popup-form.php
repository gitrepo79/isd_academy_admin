<div class="popup-form x-modal hidden">
    <div class="modal-content">
        <a href="javascript:;" class="close-btn modal-close">
            <i class="xicon xicon-close"></i>
        </a>

        <div class="inner-content">
            <h2 class="title fc-white">Register Now1</h2>
            <picture class="d-block mb-30">
                <img src="https://isddubai.com/assets-web/images/popup-banner.jpg" alt="">
            </picture>

            <form action="#" class="default-form --popup-form">
                <div class="control-group">
                    <label for="name" class="form-label">Sports</label>
                    <select name="sports" id="sports" class="form-field">
                        <option value="football">Football</option>
                        <option value="tennis">Tennis</option>
                    </select>
                </div>

                <div class="control-group">
                    <label for="name" class="form-label">Player Name</label>
                    <input type="text" class="form-field" placeholder="Enter Player Name" name="player_name" id="player_name" required>
                </div>

                <div class="control-group">
                    <label for="email" class="form-label">Player DOB</label>
                    <input type="text" class="form-field" placeholder="Enter Player DOB" name="dob" id="dob" required>
                </div>

                <div class="control-group">
                    <label for="email" class="form-label">Parent/ Guardian Name</label>
                    <input type="text" class="form-field" placeholder="Parent/Guardian Name" name="parent_name" id="parent_name" required>
                </div>

                <div class="control-group">
                    <label for="email" class="form-label">Parent/ Guardian Email</label>
                    <input type="text" class="form-field" placeholder="Parent/ Guardian Email" name="parent_email" id="parent_email" required>
                </div>

                <div class="control-group">
                    <label for="email" class="form-label">Parent/Guardian Mobile</label>
                    <input type="text" class="form-field" placeholder="Parent/Guardian Mobile" name="parent_mobile" id="parent_mobile" required>
                </div>

                <div class="control-group mt-20 mb-0">
                    <button type="submit" class="btn --btn-secondary">
                        Register Now
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="box --registration-box">
	{!! Form::open(['route' => 'form.submission', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form --registration-form"]) !!}
	<div class="control-group{{ $errors->has('sport_id') ? ' has-error' : '' }}">
		{!! Form::select('sport_id',$sports,$sport_id ?? null, ['class'=>'form-field','required' => 'required','placeholder' => 'Select Sports','id'=>'sport']) !!}
		@if ($errors->has('sport_id'))
		<span class="form-error" role="alert">
			<strong>{{ $errors->first('sport_id') }}</strong>
		</span>
		@endif
	</div>
	
	<div class="control-group{{ $errors->has('programs') ? ' has-error' : '' }}" style="display: none;" id="program">
		{!! Form::select('program',$programs,null, ['class'=>'form-field','placeholder' => 'Select Program']) !!}
		@if ($errors->has('programs'))
		<span class="form-error" role="alert">
			<strong>{{ $errors->first('programs') }}</strong>
		</span>
		@endif
	</div>

	<div class="control-group{{ $errors->has('player_types') ? ' has-error' : '' }}" style="display: none;" id="player_types">
		{!! Form::select('player_types',$player_types,null, ['class'=>'form-field','placeholder' => 'Select Player Type']) !!}
		@if ($errors->has('player_types'))
		<span class="form-error" role="alert">
			<strong>{{ $errors->first('player_types') }}</strong>
		</span>
		@endif
	</div>
    
	<div class="control-group{{ $errors->has('player_name') ? ' has-error' : '' }}">
		{!! Form::text('player_name', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Player Name','id'=>'player_name','autocomplete'=>'off']) !!}
		@if ($errors->has('player_nam'))
		<span class="form-error">
			<strong>{{ $errors->first('player_name') }}</strong>
		</span>
		@endif
	</div>
	
	<div class="control-group{{ $errors->has('dob') ? ' has-error' : '' }}">
		{!! Form::text('dob', null, ['class'=>'form-field','required' => 'required','id'=>'dob', 'placeholder'=>'Player DOB','autocomplete'=>'off']) !!}
		@if ($errors->has('dob'))
		<span class="form-error">
			<strong>{{ $errors->first('dob') }}</strong>
		</span>
		@endif
	</div>
	
	<div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
		{!! Form::text('name', null, ['class'=>'form-field', 'placeholder'=>'Parent/Guardian Name', 'id'=>'parent_name','autocomplete'=>'off']) !!}
		@if ($errors->has('name'))
		<span class="form-error">
			<strong>{{ $errors->first('name') }}</strong>
		</span>
		@endif
	</div>
	<div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
		{!! Form::text('email', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Parent/ Guardian Email', 'id'=>'email']) !!}
		@if ($errors->has('email'))
		<span class="form-error">
			<strong>{{ $errors->first('email') }}</strong>
		</span>
		@endif
	</div>
	<div class="control-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
		{!! Form::text('mobile', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Parent/Guardian Mobile','id'=>'mobile']) !!}
		@if ($errors->has('mobile'))
		<span class="form-error">
			<strong>{{ $errors->first('mobile') }}</strong>
		</span>
		@endif
	</div>

       <div class="control-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
            <div class="g-recaptcha" data-sitekey="{{env('NOCAPTCHA_SITEKEY')}}"></div>
            @if ($errors->has('g-recaptcha-response'))
                <span class="help-block">
                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                </span>
            @endif
       
    </div>
	
	
	<div class="control-group mb-10">
		<button type="submit" class="btn --btn-primary">
		{{ __('Register') }}
		</button>
	</div>
	{!! Form::hidden('form_type_id',TYPE_ACADEMY, ['class'=>'form-field']) !!}
	
	<?php /*<p class="maindesc fc-white mb-0">Already have an account? <a href="/login" class="fc-white td-underline">Login</a></p> */?>
	{!! Form::close() !!}
</div>