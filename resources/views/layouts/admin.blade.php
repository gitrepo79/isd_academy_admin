<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <title>ADMIN | ISD DUBAI</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="google-site-verification" content="0KJIv5gJ4UY7MLLDkRYbpMVYtKiL_LW-v89Yaz2pXqg" />
    
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets-web/css/style.css')}}"/>

    
    <style>
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .pre-loading {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('asset('admin-assets-web/images/loader.gif')}}') center no-repeat #fff;
    }
    </style>

    <script type="text/javascript" src="{{ asset('admin-assets-web/js/functions.js')}}"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
    
</head>
<body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div class="pre-loading"></div>
    
    <main class="app-container">
        @include('includes.admin_navigation')
        <section class="content-wrapper">
            <div> @yield('content')</div>
            <!-- /.content -->
        </section>
        <footer class="primary-footer">
            
        </footer>
    </main>
    <!-- <div class="overlay-bg"></div> -->
    
    <script type="text/javascript" src="{{ asset('admin-assets-web/js/script.js')}}"></script>
    <script>
    $(window).load(function() {
    // Animate loader off screen
    $(".pre-loading").fadeOut("slow");;
    });
    </script>
</body>
</html>