<?php
$route=Request::segment(1);	
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<meta name="keywords" content="@yield('keywords')" />
	<meta name="description" content="@yield('description')" />
	<meta name="theme-color" content="#482e74" />
	<meta name="facebook-domain-verification" content="4fbhqwtaambjmdrqmdfwaa59a5s2dq" />
	<link rel="icon" type="image/x-icon" href="/assets-web/images/favicon.ico" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="canonical" href="{{ url()->current() }}" />
	<meta name="google-site-verification" content="0KJIv5gJ4UY7MLLDkRYbpMVYtKiL_LW-v89Yaz2pXqg" />
	
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/assets-web/css/style.css"/>   
    <!-- Add icon library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


	<style>
        .bg-init,
        .animsition-overlay-slide {
            display: block;
            background-image: url('/assets-web/images/logos/isddubai-white.svg');
            background-size: 10%;
            background-position: center;
            background-repeat: no-repeat;
        }
    </style>
    
    
<style>

.primary-navigation .header-navigation{
    right: 10px !important;
}

.social-links .icon {
    height: 40px;
    width: 40px;
    display: grid;
    align-items: center;
    font-size: 15px;
    justify-content: center;
    text-decoration: none;
    margin: 10px 5px;
    border-radius: 50%;
}

.social-links .icon.fa:hover {
    opacity: 0.7;
}

.social-links .icon.fa-facebook {
    background: #3B5998;
    color: white;
}

.social-links .icon.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.social-links .icon.fa-linkedin {
  background: #007bb5;
  color: white;
}

.social-links .icon.fa-youtube {
  background: #bb0000;
  color: white;
}

.social-links .icon.fa-instagram {
  background: #E1306C;
  color: white;
}

.social-links .icon.fa-pinterest {
  background: #cb2027;
  color: white;
}

.social-links .icon.fa-snapchat-ghost {
  background: #fffc00;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

.social-links .icon.fa-skype {
  background: #00aff0;
  color: white;
}

.social-links .icon.fa-android {
  background: #a4c639;
  color: white;
}

.social-links .icon.fa-dribbble {
  background: #ea4c89;
  color: white;
}

.social-links .icon.fa-vimeo {
  background: #45bbff;
  color: white;
}

.social-links .icon.fa-tumblr {
  background: #2c4762;
  color: white;
}

.social-links .icon.fa-vine {
  background: #00b489;
  color: white;
}

.social-links .icon.fa-foursquare {
  background: #45bbff;
  color: white;
}

.social-links .icon.fa-stumbleupon {
  background: #eb4924;
  color: white;
}


</style>

	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '470356704130813');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=470356704130813&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-188680531-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-188680531-1');
	</script>
	
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<!-- tracking code -->
	<script type='text/javascript'>
	window.smartlook||(function(d) {
	var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
	var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
	c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
	})(document);
	smartlook('init', '4e027d71682fae2b7690f0231406963dae359aea');
	</script>
	<!-- End tracking code -->
	<!-- Snap Pixel Code -->
	<script type='text/javascript'>
	(function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function() {a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)}; a.queue=[];var s='script';r=t.createElement(s);r.async=!0; r.src=n;var u=t.getElementsByTagName(s)[0]; u.parentNode.insertBefore(r,u);})(window,document, 'https://sc-static.net/scevent.min.js'); snaptr('init', '96694a80-860b-40e7-a6d3-78d96c772227', { 'user_email': '__INSERT_USER_EMAIL__' }); snaptr('track', 'PAGE_VIEW');
	</script> <!-- End Snap Pixel Code -->
</head>
<body class="@yield('pageClass') bg-init">
	<!--[if lt IE 8]>
	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!--<div class="pre-loading"></div>-->
	
	<main class="app-container animsition-overlay">
		@include('includes.navigation')
		
		@yield('content')

		@include('includes.footer')

		@if(!empty($route) && $route=="isd-academy")

		@else
			@include('includes.popup-form')
		@endif

	</main>
	


	<?php /* <script type="text/javascript" src="http://web.simmons.edu/~grovesd/comm244/demo/skrollr/examples/road-trip-simple/js/skrollr.min.js"></script> */ ?>
	<script type="text/javascript" src="{{ asset('assets-web/js/functions.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets-web/js/script.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<!-- <script src="/temp-js/main.js"></script> -->

	<script>
        $(window).load(function() {
            // Animate loader off screen
            $(".pre-loading").fadeOut("slow");

            setTimeout(function () { $(".popup-form").addClass('show'); }, 15000);
        });

        

        $(document).ready(function() {
            $('.animsition-overlay').animsition({
                inClass: 'overlay-slide-in-top',
                outClass: 'overlay-slide-out-bottom',
                overlay : true,
                overlayClass : 'animsition-overlay-slide',
                overlayParentElement : 'body'
            })
            .one('animsition.inStart',function(){
                $('body').removeClass('bg-init');
            })

            .one('animsition.inEnd',function(){
            });
        });
    </script>

	<script>
	$(window).load(function() {
	// Animate loader off screen
	// $(".pre-loading").fadeOut("slow");
	// if ($(window).width() >= 1200) {
	// var s = skrollr.init();
	// }
	// skrollr.init();
	// var _skrollr = skrollr.get(); // get() returns the skrollr instance or undefined
	// var windowWidth = $(window).width();
	// if ( windowWidth <= 640 && _skrollr !== undefined ) {
	//   skrollr.init().destroy();
	// }
	});
	var dateToday = new Date();
	$( "#date_of_requirement" ).datepicker();

	var dateBirth = new Date();
	$( "#dob" ).datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: new Date('2020-12-31')
	});
	
	</script>
	@yield('footer-scripts')
</body>
</html>