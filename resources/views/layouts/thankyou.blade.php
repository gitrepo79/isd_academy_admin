<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<title>@yield('title') - ISD</title>
	<meta name="title" content="page_title">
	<meta name="keywords" content="@yield('keywords')" />
	<meta name="description" content="@yield('description')" />
	<link rel="icon" type="image/x-icon" href="/assets-web/images/favicon.ico" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="canonical" href="{{ url()->current() }}" />
	<link rel="stylesheet" type="text/css" href="/assets-web/css/style.css"/>
	<meta name="google-site-verification" content="0KJIv5gJ4UY7MLLDkRYbpMVYtKiL_LW-v89Yaz2pXqg" />
	
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/assets-web/css/style.css"/>
	
	<style>
        .bg-init,
        .animsition-overlay-slide {
            display: block;
            background-image: url('/assets-web/images/logos/isddubai-white.svg');
            background-size: 10%;
            background-position: center;
            background-repeat: no-repeat;
        }
    </style>

	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	
	<script type="text/javascript" src="{{ asset('assets-web/js/functions.js')}}"></script>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '470356704130813');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=470356704130813&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	<!-- Global site tag (gtag.js) - Google Analytics + Google Ad -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-755D7DKZNG"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'G-755D7DKZNG');
	gtag('config', 'AW-432844860');
	</script>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<!-- tracking code -->
	<script type='text/javascript'>
	window.smartlook||(function(d) {
	var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
	var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
	c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
	})(document);
	smartlook('init', '4e027d71682fae2b7690f0231406963dae359aea');
	</script>
	<!-- End tracking code -->
	<!-- Snap Pixel Code for thankyou -->
	<script type='text/javascript'> (function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function() {a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)}; a.queue=[];var s='script';r=t.createElement(s);r.async=!0; r.src=n;var u=t.getElementsByTagName(s)[0]; u.parentNode.insertBefore(r,u);})(window,document, 'https://sc-static.net/scevent.min.js'); snaptr('init', '96694a80-860b-40e7-a6d3-78d96c772227', { 'user_email': '__INSERT_USER_EMAIL__' }); snaptr('track', 'SIGN_UP' );
	</script>
	<!-- End Snap Pixel Code -->
</head>
<body class="@yield('pageClass') bg-init">
	<!--[if lt IE 8]>
	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!--<div class="pre-loading"></div>-->
	
	<main class="app-container animsition-overlay">
		@include('includes.navigation')
		
		@yield('content')

		@include('includes.footer')
	</main>

	<?php /* <script type="text/javascript" src="http://web.simmons.edu/~grovesd/comm244/demo/skrollr/examples/road-trip-simple/js/skrollr.min.js"></script> */ ?>
	<script type="text/javascript" src="{{ asset('assets-web/js/functions.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets-web/js/script.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<!-- <script src="/temp-js/main.js"></script> -->

	<script>
		$(window).load(function() {
            // Animate loader off screen
            $(".pre-loading").fadeOut("slow");;
        });

        $(document).ready(function() {
            $('.animsition-overlay').animsition({
                inClass: 'overlay-slide-in-top',
                outClass: 'overlay-slide-out-bottom',
                overlay : true,
                overlayClass : 'animsition-overlay-slide',
                overlayParentElement : 'body'
            })
            .one('animsition.inStart',function(){
                $('body').removeClass('bg-init');
            })

            .one('animsition.inEnd',function(){
            });
        });
	
	</script>
	@yield('footer-scripts')
</body>
</html>