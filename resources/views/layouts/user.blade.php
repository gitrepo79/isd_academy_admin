<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <title>ISD DUBAI | Dashboard</title>
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <noscript id="deferred-styles">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets-web/css/style.css') }}"/>
    </noscript>
    <script>
    var loadDeferredStyles = function() {
    var addStylesNode = document.getElementById("deferred-styles");
    var replacement = document.createElement("div");
    replacement.innerHTML = addStylesNode.textContent;
    document.body.appendChild(replacement)
    addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
    webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
    else window.addEventListener('load', loadDeferredStyles);
    </script>
    <style>
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .pre-loading {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    /*background: url('/admin-assets-web/images/loader.gif') center no-repeat #fff;*/
    }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://frontendscript.com/demo/jquery-timepicker-24-hour-format/dist/wickedpicker.min.css">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://frontendscript.com/demo/jquery-timepicker-24-hour-format/dist/wickedpicker.min.js"></script>
    
</head>
<body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    {{--<div class="pre-loading"></div>--}}
    <main class="app-container">
        <section class="topbar-nav">
            <nav class="topbar-navigation clearfix">
                <ul class="unstyled inline float-right text-right">
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="xxicon icon-log-out"></i>
                            Log Out
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </nav>
        </section>
        <section class="side-bar">
            <div class="header-logo">
                <img src="{{ asset('admin-assets-web/images/header-logo.png') }}" alt="" />
            </div>
            <div class="header-user">
                <span class="user-img">
                    <img src="{{ asset('admin-assets-web/images/user.jpg') }}" alt="">
                </span>
                <span class="username">
                  username
                </span>
            </div>
            <nav class="primary-navigation" style="position: relative; min-height: 100vh">
                <ul class="unstyled" style="position: absolute; top: 0; left: 0; height: 70%; width: 100%; overflow-y: scroll">
                   
                    <li>
                        <a href="" class="{{(Request::segment(2) == 'booking') ? 'active' : 'disable'}}"><i class="xxicon icon-user"></i>
                        <span>booking</span>
                        </a>
                    </li>
      
                </ul>
            </nav>
        </section>
        <section class="content-wrapper">
            @yield('content')
        </section>
        <footer class="primary-footer">
        </footer>
    </main>
    <script type="text/javascript" src="{{ asset('admin-assets-web/js/functions.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin-assets-web/js/script.js') }}"></script>
    
    {{--<script>
    $(window).load(function() {
    // Animate loader off screen
    $(".pre-loading").fadeOut("slow");
    });
    </script>--}}
    <script>
    setTimeout(function() {
    $('#successMessage').fadeOut('fast');
     }, 1000); // <-- time in milliseconds

    setTimeout(function() {
    $('#errorMessage').fadeOut('fast');
     }, 1000);
    </script>
</body>
</html>