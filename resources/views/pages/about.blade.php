@extends('layouts.app')
@section('pageClass', 'aboutpg innerpage')
@section('title', 'About ISD Dubai  | The Best Sports Facility in Dubai')
@section('description', 'Inspiratus Sports District (ISD) at Dubai Sports City is Dubai’s best sports venue, with world-class academies and playing venues for football, rugby, tennis, padel, track & field.')
@section('keywords', 'academy sports, sports academy dubai, Inspiratus Sports District, ISD Academy, ISD Dubai, ISD Athletics')
@section('content')

<!-- About ISD -->
<section class="aboutpage-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-2 order-lg-last d-none d-lg-block">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isddubai.svg" alt="">
				</picture>
			</div>

			<div class="col-lg-10">
				<h2 class="maintitle">Compete with yourself</h2>
			</div>
		</div>

		<div class="content-section">
			<p class="maindesc --big">
				Inspiratus Sports District (ISD) at Dubai Sports City is Dubai’s new sports destination, with world-class playing venues for football, rugby, tennis, padel, track & field and much more. Within easy reach of all Dubai communities, ISD is your premiere venue of choice, to get your children active, to play or to watch your favorite sport with friends or to host your next tournament.
				<br><br>
				ISD’S state-of-the art facilities, among the world’s best, are designed, developed and maintained at certified professional standards and made accessible to everyone, from children and youth to amateurs and professionals.  
				<br><br>
				ISD also features a host of Eupepsia’s advanced sports science and wellness clinic, Footlab, the world’s only performance and entertainment hub, and gourmet cafés and restaurants, ISD caters to all your athletic needs.
				<br><br>
				Centrally located at the heart of new Dubai and highly accessible within a 10-minute drive to all major Dubai communities, ISD is your ideal venue for sports and entertainment, providing athletic training & competition, entertainment and a gateway to optimal performance and wellness.
			</p>

			<h2 class="mt-40 mb-20">Venue Hire</h2>

			<p class="maindesc --big">
				The facilities at ISD Dubai Sports City are designed, developed and maintained at certified professional standards. These include 4 full-size FIFA standard and UAE FA approved football pitches comprised of an indoor full-size 3G football pitch, 2 outdoor natural grass pitches, and one outdoor astroturf pitch. <br><br>
				Our 3,500-seat stadium, includes a further FIFA standard and UAE FA approved football pitch, 1 Olympic standard 9-lane running track with high, long and triple jump facilities, 4 outdoor tennis courts, 8 indoor and outdoor World Padel Tour courts and the UAE’s only 2 indoor tennis and multisport courts. <br><br>
				With outstanding grounds, changing rooms, meeting rooms and offices, ISD caters to all your sporting needs, ranging from a pitch booking, to tournament, league, customized event management. and corporate experiences.
			</p>

			<h2 class="mt-40 mb-20">Award-Winning Academies </h2>

			<p class="maindesc --big">
				Home to some of Dubai’s best academies including LaLiga Academy, ISD Athletics, ISD Tennis and ISD Rugby, ISD Dubai Sports City is championing sports development with international standard training programs that focus on whole player development, combining technical skills with mental agility and leadership qualities for players 5-18 years.  Academies also offer top athletes a pathway to professional sports careers and scholarships to universities in the US. 
			</p>

			<h2 class="mt-40 mb-20">Leagues & Tournaments</h2>

			<p class="maindesc --big">
				ISD Dubai Sports City boasts the best facilities in the region making it a venue of choice for international athletes and teams for local tournaments, international warm weather training and professional competitions.
			</p>

		</div>
	</div>
</section>

@endsection