@extends('layouts.app')
@section('pageClass', 'athleticpg innerpage')
@section('title', 'Register Now - The best Sports Academy In Dubai')
@section('description', 'Sports Academy In Dubai')
@section('content')

<section class="sec-padding">
    <div class="container-wrapper">
        <h2 class="maintitle">Registration</h2>

        <div class="content-section">
            
            <div class="row">
                <section class="col-xl-5 col-lg-6">
                    
                    <div class="box --registration-box">
						{!! Form::open(['route' => 'form.submission', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form --registration-form"]) !!}
						
						<div class="control-group{{ $errors->has('sport_id') ? ' has-error' : '' }}">
							{!! Form::select('sport_id',$sports,null, ['class'=>'form-field','required' => 'required','placeholder' => 'Select Sports','id'=>'sport']) !!}
							@if ($errors->has('sport_id'))
							<span class="form-error" role="alert">
								<strong>{{ $errors->first('sport_id') }}</strong>
							</span>
							@endif
						</div>
						
						<div class="control-group{{ $errors->has('programs') ? ' has-error' : '' }}" style="display: none;" id="program">
							{!! Form::select('program',$programs,null, ['class'=>'form-field','placeholder' => 'Select Program']) !!}
							@if ($errors->has('programs'))
							<span class="form-error" role="alert">
								<strong>{{ $errors->first('programs') }}</strong>
							</span>
							@endif
						</div>
                        
						<div class="control-group{{ $errors->has('player_name') ? ' has-error' : '' }}">
							{!! Form::text('player_name', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Player Name']) !!}
							@if ($errors->has('player_nam'))
							<span class="form-error">
								<strong>{{ $errors->first('player_name') }}</strong>
							</span>
							@endif
						</div>
						
						<div class="control-group{{ $errors->has('dob') ? ' has-error' : '' }}">
							{!! Form::text('dob', null, ['class'=>'form-field','required' => 'required','id'=>'dob', 'placeholder'=>'Player DOB','autocomplete'=>'off']) !!}
							@if ($errors->has('dob'))
							<span class="form-error">
								<strong>{{ $errors->first('dob') }}</strong>
							</span>
							@endif
						</div>
						
						<div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
							{!! Form::text('name', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Parent/Guardian']) !!}
							@if ($errors->has('name'))
							<span class="form-error">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>
						<div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
							{!! Form::text('email', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Parent/ Guardian Email']) !!}
							@if ($errors->has('email'))
							<span class="form-error">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
						<div class="control-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
							{!! Form::text('mobile', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Parent/Guardian Mobile']) !!}
							@if ($errors->has('mobile'))
							<span class="form-error">
								<strong>{{ $errors->first('mobile') }}</strong>
							</span>
							@endif
						</div>

                           <div class="control-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                <div class="g-recaptcha" data-sitekey="{{env('NOCAPTCHA_SITEKEY')}}"></div>
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                           
                        </div>
						
						
						<div class="control-group mb-10">
							<button type="submit" class="btn --btn-primary">
							{{ __('Register') }}
							</button>
						</div>
						{!! Form::hidden('form_type_id',TYPE_ACADEMY, ['class'=>'form-field']) !!}
						
						<?php /*<p class="maindesc fc-white mb-0">Already have an account? <a href="/login" class="fc-white td-underline">Login</a></p> */?>
						{!! Form::close() !!}
					</div>
                </section>
            </div>

        </div>
    </div>
</section>


<script type="text/javascript">
$(document).ready(function(){
  $('#sport').change(function(){
  	  $('#program').hide();
      var selectVal = $("#sport option:selected").val();
      if(selectVal==4 ){
      $('#program').show();
       $('#player_types').show();	
      }
      if(selectVal==5){
      $('#player_types').show();	
      }
      
  });
   $('#player_types').change(function(){
  	 
      var selectVal = $("#player_types option:selected").val();
      if(selectVal==1){
      	
       var player=$('#player_name').val();
       $('#mobile').attr('placeholder',"Player Mobile");
       $('#email').attr('placeholder','Player Email');
       $('#parent_name').hide();
  
      }
      else{
       $('#mobile').attr('placeholder',"Parent/Guardian Mobile");
       $('#email').attr('placeholder','Parent/Guardian Email');
       $('#parent_name').show();
      }
      
  });

   $("#dob").click(function(){
   	var selectVal = $("#player_types option:selected").val();
    if(selectVal==1){
    var data =$("#player_name").val();
    $("#parent_name").val(data);
   }
});

});
</script>
@endsection