@extends('layouts.app')
@section('pageClass', 'athletics innerpage')
@section('title', 'ISD Athletics Academy | Track & Field Academy for boys and girls')
@section('description', 'ISD Athletics Academy is the best track and field academy located in Dubai Sports City for boys and girls aged 4-18 taught by Olympians and USA Division 1 track and field coaches.')
@section('keywords', 'Track and field, running track, sprints runs, middle distance runs, long distance runs, hurdles, shot put, discus, javelin, long jump, triple jump, high jump, strength and conditioning, ultimate athletics dubai, athletics training in dubai, fun activities for kids, best activities for kids, sports academy near me, running classes for kids, training camps for kids, kids competitions near me, athletics in dubai, athletics, how to run faster, after school activities for kids, kids activities near me, fun activities for kids in dubai, kids activity in dubai, running track near me, best sports academy near me, training camps near me, athletics academy in dubai, best sports academy in dubai, student athletes in dubai, track sports in dubai, training camps in dubai')
@section('content')

<section class="hero-banner --video-banner">

	<video playsinline autoplay muted loop poster="https://img.youtube.com/vi/4weYXjdpGuw/maxresdefault.jpg" class="video">
		<!-- <source src="polina.webm" type="video/webm"> -->
		<source src="/assets-web/videos/ISD_Athletics_Academy.mp4" type="video/mp4">
	</video>

</section>

<!-- About Athletics -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<!-- <div class="col-lg-2 order-lg-last">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdathletics.svg" alt="">
				</picture>
			</div> -->

			<div class="col-lg-10">
				<h2 class="maintitle">
					<span class="fc-football">ISD Athletics Academy</span>
				</h2>
			</div>
		</div>

		{{-- <h4 class="maintitle --small mt-50">
			the academy
		</h4> --}}

		<div class="content-section mb-40">
			<p class="maindesc --big">
				ISD Athletics Academy at Dubai Sports City is back for the new season! We continue to raise the bar on athletics with a new, world-class platform for young athletes of all ages and abilities across a wide range of disciplines. This includes but is not limited to sprints, middle and long distance runs, hurdles, shot put, discus, javelin, long jump, triple jump, and high jump.
				<br><br>
				ISD Athletics Academy welcomes children from 5 to 18 years old of every ability level; from beginners who want to build the fundamentals and improve fitness, to advanced-level athletes with specialities in individual disciplines. 
				<br><br>
				Our comprehensive programs uplift each child's abilities by building on the athlete’s strengths while placing a focus on areas that need improvement. This allows everyone to develop elite fundamentals that are effective across multiple disciplines and sports through engaging and fun activities for kids.
				<br><br>
				The Academy provides a platform for athletes of all sporting backgrounds to elevate their abilities in the sports they already love through the application of biomechanics technique enhancements. Our programs supply top athletes with a pathway to pursue professional careers in athletics through college scholarships in the United States.
			</p>
		</div>

		<div class="row">
			<div class="col-lg-4">
				<div class="box --programs-box">
					
					<article class="inner-content">
						<h2 class="title mb-0">
							<span class="fc-primary">TERM 1</span>
						</h2>
						<p class="subtitle">
							<span class="fc-football">SEPT 05 - DEC 09</span>
						</p>
												
					</article>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="box --programs-box">

					<article class="inner-content">
						<h2 class="title mb-0">
							<span class="fc-primary">TERM 2</span>
						</h2>
						<p class="subtitle">
							<span class="fc-football">JAN 02 - MAR 24</span>
						</p>
												
					</article>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="box --programs-box">
					
					<article class="inner-content">
						<h2 class="title mb-0">
							<span class="fc-primary">TERM 3</span>
						</h2>
						<p class="subtitle">
							<span class="fc-football">APR 11 - JUN 30</span>
						</p>
												
					</article>
				</div>
			</div>
		</div>

		<div class="text-center mb-0">
			<h2 class="fc-primary">CALL US NOW FOR A FREE TRIAL: <span class="fc-football">058 285 8239</span></h2>
		</div>

		{{-- <div class="text-center">
			<a href="/athleticsacademy-fees" class="btn --btn-primary ">click here for pricing</a>

			<a href="/athleticsacademy-schedule" class="btn --btn-primary ">click here for schedule</a>
		</div> --}}

	</div>
</section>

<hr class="divider" />

{{-- Athletics Schedule --}}
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">THE schedule</span>
				</h2>
			</div>
		</div>

		<style>
			.athletics.small-text th, .athletics.small-text td {
				font-size: 14px
			}
		</style>

		<div class="table-responsive">
			<table class="table --schedule-table athletics small-text text-left">
				<tr>
					<th>
						Age Group / Start Time
					</th>
					<th>
						Sunday
					</th>
					<th>
						Monday
					</th>
					<th>
						Tuesday
					</th>
					<th>
						Wednesday
					</th>
					<th>
						Thursday
					</th>
					<th>
						Friday
					</th>
				</tr>

				<tr>
					<td>
						5-8 <br>5:00 PM
					</td>
					<td>
						General Mechanics, Endurance, Strength & Conditioning - Coach Snow
					</td>
					<td>
						General Mechanics, Endurance, Strength & Conditioning - Coach Martyn, Coach Snow
					</td>
					<td>
						General Mechanics, Endurance, Strength & Conditioning - Coach Martyn, Coach Snow
					</td>
					<td>
						General Mechanics, Endurance, Strength & Conditioning - Coach Martyn, Coach Snow
					</td>
					<td>
						General Mechanics, Endurance, Strength & Conditioning - Coach Martyn, Coach Snow
					</td>
					<td>
						General Mechanics, Endurance, Strength & Conditioning - Coach Snow
					</td>
				</tr>

				<tr>
					<td>
						9-12 <br>6:00 PM
					</td>
					<td>
						-
					</td>
					<td>
						Sports Speed & Conditioning - Coach Martyn
					</td>
					<td>
						Distance - Coach Martyn
					</td>
					<td>
						Jumps - Coach Martyn
					</td>
					<td>
						Sprints - Coach Martyn
					</td>
					<td>
						-
					</td>
				</tr>

				<tr>
					<td>
						9-12 <br> 6:00 PM
					</td>
					<td>
						Sprints - Coach Snow
					</td>
					<td>
						Jumps - Coach Snow
					</td>
					<td>
						Throws - Coach Snow
					</td>
					<td>
						Sprints - Coach Snow
					</td>
					<td>
						Throws - Coach Snow
					</td>
					<td>
						Jumps - Coach Snow Distance - Coach Snow
					</td>
				</tr>

				<tr>
					<td>
						13+ <br>7:00 PM
					</td>
					<td>
						-
					</td>
					<td>
						Sports Speed & Conditioning - Coach Martyn
					</td>
					<td>
						Distance - Coach Martyn
					</td>
					<td>
						Jumps - Coach Martyn
					</td>
					<td>
						Sprints - Coach Martyn
					</td>
					<td>
						-
					</td>
				</tr>

				<tr>
					<td>
						13+ <br>7:00 PM
					</td>
					<td>
						Sprints - Coach Snow
					</td>
					<td>
						Jumps - Coach Snow
					</td>
					<td>
						Throws - Coach Snow
					</td>
					<td>
						Sprints - Coach Snow
					</td>
					<td>
						Throws - Coach Snow
					</td>
					<td>
						Jumps - Coach Snow Distance - Coach Snow
					</td>
				</tr>


			</table>
		</div>

		
		<div class="text-center mt-0">
			<a href="#" class="btn --btn-primary" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<hr class="divider" />

<!-- Coaches -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">Our Coaches</span>
				</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				ISD Athletics’ team of professional certified coaches of Olympian, European and Team GB medallist, Martyn Bernard and US Division 1 Coach and biomechanics expert Evan Snow, deliver an unparalleled training methodology powered by the science of biomechanics to enhance the performance of athletes of all ages and abilities.
			</p>
		</div>
		
		<div class="row">
			<div class="col-lg-4">
				<picture>
					<img src="/assets-web/images/gallery/athletics/coach-martyn.jpg" alt="">
				</picture>
			</div>

			<div class="col-lg-8">

				<table class="table --schedule-table coaches">
					<tr>
						<th class="text-left" width="220px">
							Coach
						</th>
						<td>
							Martyn Bernard
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Home Town
						</th>
						<td>
							Wakefield, United Kingdom
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Accolades
						</th>
						<td>
							Finalist - 2008 Beijing Olympics High Jump <br>
							Bronze Medal – 2010 European Athletic Championships <br>
							Bronze Medal – 2010 Continental Cup <br>
							Bronze Medal – 2007 European Indoor Championships <br>
							Silver Medal – 2006 Commonwealth Games
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Professional Background
						</th>
						<td>
							B.S. Psychology
						</td>
					</tr>

				</table>
			</div>
		</div>

		<div class="row mt-60">
			<div class="col-lg-4">
				<picture>
					<img src="/assets-web/images/gallery/athletics/coach-evan.jpg" alt="">
				</picture>
			</div>

			<div class="col-lg-8">
				<table class="table --schedule-table coaches">
					<tr>
						<th class="text-left" width="220px">
							Coach
						</th>
						<td>
							Evan Snow
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Home Town
						</th>
						<td>
							Canton, Ohio, United States
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Qualifications
						</th>
						<td>
							USATF Track & Field Coach
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Professional Background
						</th>
						<td>
							B.S. Mechanical Engineering <br>
							B.S. Biomedical Engineering <br>
							Published author in the biological sciences
						</td>
					</tr>

				</table>


			</div>
		</div>

	</div>
</section>

<hr class="divider" />

{{-- Athletics Fees --}}
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">Pricing</span>
				</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Each ISD Athletics season is made up of 3 terms: Term 1 - (14 weeks), Term 2 – (12 weeks), and Term 3 - (12 weeks). All terms are charged pro rata and can be booked as per term or for the full season.
			</p>
		</div>

		<div class="Rtable --collapse --2cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				Number of Sessions
			</div>

			<div class="Rtable-cell --head">
				Rates per Session
			</div>

			<!-- 1x per week -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Number of Sessions
					</div>

					<div class="content">
						1x per week
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Rates per Session
					</div>

					<div class="content">
						AED 100
					</div>
				</div>
			</div>

			<!-- 2x per week -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Number of Sessions
					</div>

					<div class="content">
						2x per week
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Rates per Session
					</div>

					<div class="content">
						AED 85
					</div>
				</div>
			</div>

			<!-- 3x per week -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Number of Sessions
					</div>

					<div class="content">
						3x per week
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Rates per Session
					</div>

					<div class="content">
						AED 75
					</div>
				</div>
			</div>

		</div>
		
		<div class="text-center mt-40">
			<a href="#" class="btn --btn-primary" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<hr class="divider" />

<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<h2 class="maintitle">gallery</h2>

		<div class="gallery-section">

			<div class="gallery-slider">
				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img1.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img1.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img2.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img2.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img3.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img3.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img4.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img4.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img5.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img5.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img6.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img6.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img7.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img7.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img8.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img8.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img9.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img9.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img10.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img10.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img11.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img11.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img12.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img12.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img13.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img13.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img14.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img14.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img15.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img15.jpg" alt="">
					</a>
				</div>
			</div>
		</div>

	</div>
</section>

@endsection