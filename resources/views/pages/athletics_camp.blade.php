@extends('layouts.app')
@section('pageClass', 'athletics innerpage')
@section('title', 'ISD Athletics Academy | Track & Field Academy for boys and girls')
@section('description', 'ISD Athletics Academy is the best track and field academy located in Dubai Sports City for boys and girls aged 4-18 taught by Olympians and USA Division 1 track and field coaches.')
@section('keywords', 'Track and field, running track, sprints runs, middle distance runs, long distance runs, hurdles, shot put, discus, javelin, long jump, triple jump, high jump, strength and conditioning, ultimate athletics dubai, athletics training in dubai')
@section('content')

<section class="hero-banner --video-banner">

	<video playsinline autoplay muted loop poster="https://img.youtube.com/vi/4weYXjdpGuw/maxresdefault.jpg" class="video">
		<!-- <source src="polina.webm" type="video/webm"> -->
		<source src="/assets-web/videos/ISD_Athletics_Academy.mp4" type="video/mp4">
	</video>

</section>

<!-- About Athletics -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<!-- <div class="col-lg-2 order-lg-last">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdathletics.svg" alt="">
				</picture>
			</div> -->

			<div class="col-lg-10">
				<h2 class="maintitle">
					<span class="fc-football">ISD ATHLETICS ACADEMY CAMPS</span>
				</h2>
			</div>
		</div>

		{{-- <h4 class="maintitle --small mt-50">
			the academy
		</h4> --}}

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Join ISD Athletics Academy at Dubai Sports City during Spring, Summer and Winter, and enjoy a unique track and field program developed by our expert coaches Olympian, European and Team GB medallist, Martyn Bernard and US Division 1 Coach and biomechanics expert Evan Snow.
				<br><br>
				Our unique camp programs are customized for different age groups to keep children active. ISD Athletics Academy's focus is to improve speed, agility, jumping, and throwing skills while developing confidence and leadership to thrive in a competitive environment. With the help of our coaches, athletes will acquire new knowledge and techniques they will use throughout their future athletic careers
			</p>
		</div>


		<div class="text-center mb-0">
			<h2 class="fc-primary">CALL US NOW FOR A FREE TRIAL: <span class="fc-football">058 285 8239</span></h2>
		</div>

		{{-- <div class="text-center">
			<a href="/athleticsacademy-fees" class="btn --btn-primary ">click here for pricing</a>

			<a href="/athleticsacademy-schedule" class="btn --btn-primary ">click here for schedule</a>
		</div> --}}

	</div>
</section>

<hr class="divider" />

{{-- Athletics Schedule --}}
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">THE schedule</span>
				</h2>
			</div>
		</div>

		<!-- Schedule Table -->
		<h3>
			Sundays and Tuesdays
		</h3>
		<div class="table-responsive">
			<table class="table --schedule-table athletics text-left">
				<tr>
					<th>
						Days
					</th>
					<th>
						Detail
					</th>
				</tr>

				<tr>
					<td>
						9:00 am
					</td>
					<td>
						Welcome
					</td>
				</tr>

				<tr>
					<td>
						9:05 am
					</td>
					<td>
						Warm up and conditioning exercises
					</td>
				</tr>

				<tr>
					<td>
						9:15 am
					</td>
					<td>
						Agility Obstacle course
					</td>
				</tr>

				<tr>
					<td>
						10:00 am
					</td>
					<td>
						Sprints
					</td>
				</tr>

				<tr>
					<td>
						10:45 am
					</td>
					<td>
						Athletics Circuit
					</td>
				</tr>

				<tr>
					<td>
						11:00 am
					</td>
					<td>
						Snack Break
					</td>
				</tr>

				<tr>
					<td>
						11:15 am
					</td>
					<td>
						Throws (Javelin, shot put, discus)
					</td>
				</tr>

				<tr>
					<td>
						12:00 pm
					</td>
					<td>
						Pick Up
					</td>
				</tr>
			</table>
		</div>


		<h3>
			Mondays and Wednesdays
		</h3>
		<div class="table-responsive">
			<table class="table --schedule-table athletics text-left">
				<tr>
					<th>
						Days
					</th>
					<th>
						Detail
					</th>
				</tr>

				<tr>
					<td>
						9:00 am
					</td>
					<td>
						Welcome
					</td>
				</tr>

				<tr>
					<td>
						9:05 am
					</td>
					<td>
						Warm up and conditioning exercises
					</td>
				</tr>

				<tr>
					<td>
						9:15 am
					</td>
					<td>
						Long and Triple jump
					</td>
				</tr>

				<tr>
					<td>
						10:00 am
					</td>
					<td>
						Sprint/Hurdles
					</td>
				</tr>

				<tr>
					<td>
						11:00 am
					</td>
					<td>
						Snack Break
					</td>
				</tr>

				<tr>
					<td>
						11:15 am
					</td>
					<td>
						High jump
					</td>
				</tr>

				<tr>
					<td>
						12:00 pm
					</td>
					<td>
						Pick Up
					</td>
				</tr>
			</table>
		</div>



		<h3>
			Thursdays: Compete with yourself!
		</h3>
		<div class="table-responsive">
			<table class="table --schedule-table athletics text-left">
				<tr>
					<th>
						Days
					</th>
					<th>
						Detail
					</th>
				</tr>

				<tr>
					<td>
						9:00 am
					</td>
					<td>
						Welcome
					</td>
				</tr>

				<tr>
					<td>
						9:05 am
					</td>
					<td>
						Warm up and conditioning exercises
					</td>
				</tr>

				<tr>
					<td>
						9:30 am
					</td>
					<td>
						Competitions
					</td>
				</tr>

				<tr>
					<td>
						12:00 pm
					</td>
					<td>
						Pick Up
					</td>
				</tr>
			</table>
		</div>

		
		<div class="text-center mt-40">
			<a href="#" class="btn --btn-primary" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<hr class="divider" />

<!-- Coaches -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">Our Coaches</span>
				</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				ISD Athletics’ team of professional certified coaches of Olympian, European and Team GB medallist, Martyn Bernard and US Division 1 Coach and biomechanics expert Evan Snow, deliver an unparalleled training methodology powered by the science of biomechanics to enhance the performance of athletes of all ages and abilities.
			</p>
		</div>
		
		<div class="row">
			<div class="col-lg-4">
				<picture>
					<img src="/assets-web/images/gallery/athletics/coach-martyn.jpg" alt="">
				</picture>
			</div>

			<div class="col-lg-8">

				<table class="table --schedule-table coaches">
					<tr>
						<th class="text-left" width="220px">
							Coach
						</th>
						<td>
							Martyn Bernard
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Home Town
						</th>
						<td>
							Wakefield, United Kingdom
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Accolades
						</th>
						<td>
							Finalist - 2008 Beijing Olympics High Jump <br>
							Bronze Medal – 2010 European Athletic Championships <br>
							Bronze Medal – 2010 Continental Cup <br>
							Bronze Medal – 2007 European Indoor Championships <br>
							Silver Medal – 2006 Commonwealth Games
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Professional Background
						</th>
						<td>
							B.S. Psychology
						</td>
					</tr>

				</table>
			</div>
		</div>

		<div class="row mt-60">
			<div class="col-lg-4">
				<picture>
					<img src="/assets-web/images/gallery/athletics/coach-evan.jpg" alt="">
				</picture>
			</div>

			<div class="col-lg-8">
				<table class="table --schedule-table coaches">
					<tr>
						<th class="text-left" width="220px">
							Coach
						</th>
						<td>
							Evan Snow
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Home Town
						</th>
						<td>
							Canton, Ohio, United States
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Qualifications
						</th>
						<td>
							USATF Track & Field Coach
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Professional Background
						</th>
						<td>
							B.S. Mechanical Engineering <br>
							B.S. Biomedical Engineering <br>
							Published author in the biological sciences
						</td>
					</tr>

				</table>


			</div>
		</div>

	</div>
</section>

<hr class="divider" />

{{-- Athletics Fees --}}
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">Pricing</span>
				</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				ISD Athletics Camps at Dubai Sports City can be booked in either daily, weekly or bi-weekly sessions! 

			</p>
		</div>

		<table class="table --schedule-table text-left">
			<tr>
				<th>
					Duration
				</th>
				<th>
					Fees
				</th>
			</tr>

			<tr>
				<td>
					Daily
				</td>
				<td>
					AED 150
				</td>
			</tr>

			<tr>
				<td>
					One Week
				</td>
				<td>
					AED 600
				</td>
			</tr>

			<tr>
				<td>
					Two Weeks
				</td>
				<td>
					AED 1,000
				</td>
			</tr>
		</table>
		
		<div class="text-center mt-40">
			<a href="#" class="btn --btn-primary" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<hr class="divider" />

<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<h2 class="maintitle">gallery</h2>

		<div class="gallery-section">

			<div class="gallery-slider">
				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img1.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img1.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img2.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img2.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img3.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img3.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img4.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img4.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img5.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img5.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img6.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img6.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img7.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img7.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img8.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img8.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img9.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img9.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img10.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img10.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img11.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img11.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img12.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img12.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img13.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img13.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img14.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img14.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/athletics/img15.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/athletics/img15.jpg" alt="">
					</a>
				</div>
			</div>
		</div>

	</div>
</section>

@endsection