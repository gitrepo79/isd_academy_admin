@inject('arrays','App\Http\Utilities\Arrays')
@extends('layouts.app')
@section('pageClass', 'sportsdetailpg inner-page')
@section('title', 'ISD Athletics Academy')
@section('description', 'Athletics Academy in Dubai')
@section('keywords', 'ultimate athletics dubai, athletics training in dubai')
@section('content')
<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/running-banner.jpg');">

	<div class="inner-content --registration d-none d-xl-block">
    </div>

	<div class="inner-content --running">
		<div class="vertical-heading">
			<h1 class="title">Athletics</h1>
		</div>
	</div>

	<div class="content-section --scroll-page">
		<article>
			<h2 class="title">ISD Athletics Classic Meet <br class="d-none d-md-block" /> At Dubai Sports City</h2>

			<p class="mobile-content">
				Come and compete alongside the Olympic hopefuls.<br><br>
				Last day of registration Monday, 22 nd March 2021.
			</p>

			<h4 class="fc-white mb-40">AED 60 for 1 event <br>AED 90 for 2 events</h4>

			<div class="row">
				<section class="col-xl-5 col-lg-6">
					
					<div class="box --registration-box padd-mobile">
						{!! Form::open(['route' => 'form.submission', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form --registration-form"]) !!}
						
						
						<div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
							{!! Form::text('name', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Full Name']) !!}
							@if ($errors->has('name'))
							<span class="form-error">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>

						 <div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            
                            {!! Form::text('email', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Email Address']) !!}
                            @if ($errors->has('email'))
                            <span class="form-error">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="control-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            
                            {!! Form::text('mobile', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Mobile Number']) !!}
                            @if ($errors->has('mobile'))
                            <span class="form-error">
                                <strong>{{ $errors->first('mobile') }}</strong>
                            </span>
                            @endif
                        </div>


						<div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
							{!! Form::select('nationality',$arrays::countries(),null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Nationality']) !!}
							@if ($errors->has('nationality'))
							<span class="form-error">
								<strong>{{ $errors->first('nationality') }}</strong>
							</span>
							@endif
						</div>
						
						
						<div class="control-group{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
							{!! Form::text('date_of_birth', null, ['class'=>'form-field','required' => 'required','id'=>'dob', 'placeholder'=>'Date of Birth']) !!}
							@if ($errors->has('date_of_birth'))
							<span class="form-error">
								<strong>{{ $errors->first('date_of_birth') }}</strong>
							</span>
							@endif
						</div>

						<div class="control-group{{ $errors->has('event_date') ? ' has-error' : '' }}">
							<label class="fc-white h6 mb-10 d-block">Event Dates:</label>
							@foreach($event_dates as $key=>$date)
								<div class="checkbox">
									{{ Form::checkbox('event_date[]',$key,null,null) }}
									<span class="label"></span>
									<span class="text fc-white">
										{!! Form::label($date) !!}
									</span>
								</div>
							@endforeach
							@if ($errors->has('event_date'))
							<span class="form-error" role="alert">
								<strong>{{ $errors->first('event_date') }}</strong>
							</span>
							@endif
						</div>
						<div class="control-group{{ $errors->has('event_id') ? ' has-error' : '' }}">
							<label class="fc-white h6 mb-10 d-block">Events to enter:</label>

							@foreach($events as $key=>$event)
								<div class="checkbox">
									{{ Form::checkbox('events[]',$event,null,null) }}
									<span class="label"></span>
									<span class="text fc-white">
										{!! Form::label($event) !!}
									</span>
								</div>
							@endforeach
							@if ($errors->has('event_id'))
							<span class="form-error" role="alert">
								<strong>{{ $errors->first('event_id') }}</strong>
							</span>
							@endif
						</div>
						<div class="control-group">
							<button type="submit" class="btn --btn-primary">
							{{ __('Register Now') }}
							</button>
						</div>
						<input type="hidden" name="form_type_id" value="5">
						{!! Form::close() !!}

					</div>
				</section>
			</div>
			
			
		</article>
	</div>
</section>
@endsection