@extends('layouts.app')
@section('pageClass', 'sportsdetailpg inner-page')
@section('title', 'Spring Camp with ISD Athletics Academy')
@section('description', 'Athletics Academy in Dubai')
@section('keywords', 'ultimate athletics dubai, athletics training in dubai')
@section('content')

<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/athletics-detail-banner.webp'); background-position: center;">

	<div class="inner-content --running">
		<div class="vertical-heading">
			<h1 class="title">Athletics</h1>
		</div>
	</div>

	<div class="content-section --scroll-page">
		<article>
			<h2 class="title --big">ISD Athletics Spring Program</h2>
			<p class="mobile-content">
				Build your speed and agility with coach Andy Turner three-time medalist and Olympian, Commonwealth and European Champion, supported by coach Alex Al-Ameen International medalist. <br><br>Our unique track and field spring program is customized for the different age groups to keep children active, improving their speed, jumping and throwing techniques while acquiring new skills to build their confidence and stamina.
			</p>

			<div class="bottom-section">
				
				<p class="subtitle">
					Join our Field and Track Spring Program and Compete with Yourself!
				</p>

				<a href="/academyRegistration" class="btn bg-running tt-uppercase fw-bold mb-20 mt-20">Register Now</a>
				
				<span class="fc-white pr-2 pl-2 fs-large">OR</span>

				<a href="tel:+971582858239" class="btn bg-primary tt-uppercase fw-bold mb-20 mt-20 fc-white">Call Us: 058 2858239</a>

				<p class="introductive-title --athletics">
					ISD Athletics Academy <br /> 9 am – 12 pm (5 - 18 years old)
				</p>

				<div class="table-section --athletics">

					<p class="table-title">
						spring program (15% off Early Bird offer)
					</p>

					<p class="fc-white h4">March 28, 2021 – April 8, 2021</p>

					<div class="Rtable --collapse --3cols detail-table mb-40 text-center">
						<div class="Rtable-cell --head">day</div>
						<div class="Rtable-cell --head">week</div>
						<div class="Rtable-cell --head">two weeks</div>

						<!-- First Row -->
						<div class="Rtable-cell">
							<div class="body justify-content-center">
								<div class="header">
									Daily
								</div>
								<div class="content">
									150 AED
								</div>
							</div>
						</div>

						<div class="Rtable-cell">
							<div class="body justify-content-center">
								<div class="header">
									One Week
								</div>
								<div class="content">
									600 AED
								</div>
							</div>
						</div>

						<div class="Rtable-cell">
							<div class="body justify-content-center">
								<div class="header">
									Two Weeks
								</div>
								<div class="content">
									1000 AED
								</div>
							</div>
						</div>

					</div>


					<p class="table-title">
						isd athletics Academy schedule
					</p>

					<div class="table-responsive">
						<table class="table --schedule-table athletics">
							<tr>
								<th>Days</th>
								<th>time</th>
								<th>detail</th>
							</tr>

							<!-- Sunday Tuesday -->
							<tr>
								<td rowspan="7">
									<span class="fc-running">Sundays and Tuesdays</span>
								</td>
								<td>
									9 AM
								</td>
								<td>
									Welcome
								</td>
							</tr>

							<tr>
								<td>
									9:05 AM - 9:15 AM
								</td>
								<td>
									Warm up and conditioning exercises
								</td>
							</tr>

							<tr>
								<td>
									9:15 AM – 10 AM
								</td>
								<td>
									Agility Obstacle course
								</td>
							</tr>

							<tr>
								<td>
									10 AM – 10:45 AM
								</td>
								<td>
									Sprints
								</td>
							</tr>

							<tr>
								<td>
									10:45 Am – 11 AM
								</td>
								<td>
									Athletics Circuit
								</td>
							</tr>

							<tr>
								<td>
									11 AM – 11:15 AM
								</td>
								<td>
									Snack Break
								</td>
							</tr>

							<tr>
								<td>
									11:15 AM – 12 PM
								</td>
								<td>
									Throws (Javelin, shot put, discus)
								</td>
							</tr>

							<!-- Monday Wednesday -->
							<tr>
								<td rowspan="6">
									<span class="fc-running">Mondays and Wednesdays</span>
								</td>
								<td>
									9 AM
								</td>
								<td>
									Welcome
								</td>
							</tr>

							<tr>
								<td>
									9:05 AM - 9:15 AM
								</td>
								<td>
									Warm up and conditioning exercises
								</td>
							</tr>

							<tr>
								<td>
									9:15 AM – 10 AM
								</td>
								<td>
									Long and Triple jump
								</td>
							</tr>

							<tr>
								<td>
									10 AM – 11 AM
								</td>
								<td>
									Sprints/Hurdles
								</td>
							</tr>

							<tr>
								<td>
									11 AM – 11:15 AM
								</td>
								<td>
									Snack Break
								</td>
							</tr>

							<tr>
								<td>
									11:15 AM – 12 PM
								</td>
								<td>
									High Jump
								</td>
							</tr>

							<!-- Thursday -->
							<tr>
								<td rowspan="3">
									<span class="fc-running">Thursdays – Compete with yourself!</span>
								</td>
								<td>
									9 AM
								</td>
								<td>
									Welcome
								</td>
							</tr>

							<tr>
								<td>
									9:05 AM - 9:30 AM
								</td>
								<td>
									Warm up and conditioning exercises
								</td>
							</tr>

							<tr>
								<td>
									9:30 AM – 12 AM
								</td>
								<td>
									Competitions
								</td>
							</tr>

						</table>
					</div>

				</div>

			</div>
		
		</article>
	</div>

</section>

@include('elements.athletics-popup')

@endsection