@extends('layouts.app')
@section('pageClass', 'athletics innerpage')
@section('title', 'Athletics Academy in Dubai')
@section('description', 'Athletics Academy in Dubai')
@section('keywords', 'Track and field, running track, sprints runs, middle distance runs, long distance runs, hurdles, shot put, discus, javelin, long jump, triple jump, high jump, strength and conditioning, ultimate athletics dubai, athletics training in dubai')
@section('content')

<section class="hero-banner --video-banner">

	<video playsinline autoplay muted loop poster="https://img.youtube.com/vi/WdWL47LW7jw/maxresdefault.jpg" class="video">
		<!-- <source src="polina.webm" type="video/webm"> -->
		<source src="/assets-web/videos/ISD_Athletics_Academy.mp4" type="video/mp4">
	</video>

</section>

<!-- About Athletics -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-2 order-lg-last d-none d-lg-block">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdathletics.svg" alt="">
				</picture>
			</div>

			<div class="col-lg-10">
				<h2 class="maintitle">ISD Athletics Academy</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Join ISD Athletics Academy at Dubai Sports City today  and enjoy a unique track and field program  developed by our Olympian head coach Andy Turner,  three-time medalist and Commonwealth and European Champion.
 				<br><br>
				Experience the ISD Athletics Academy difference and  see your child build speed, agility, confidence and stamina, while having lots of fun! 
 				<br><br>
				We look forward to seeing you on the track!
			</p>
		</div>

		<!-- Pricing Table -->
		<h2 class="maintitle">Pricing</h2>

		<div class="Rtable --collapse --3cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				Number of Sessions
			</div>

			<div class="Rtable-cell --head">
				Rates per Session
			</div>

			<div class="Rtable-cell --head">
				Full Term
			</div>

			<!-- 1x per week -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Number of Sessions
					</div>

					<div class="content">
						1x per week
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Rates per Session
					</div>

					<div class="content">
						AED 100
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full Term
					</div>

					<div class="content">
						AED 1,400
					</div>
				</div>
			</div>

			<!-- 2x per week -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Number of Sessions
					</div>

					<div class="content">
						2x per week
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Rates per Session
					</div>

					<div class="content">
						AED 85
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full Term
					</div>

					<div class="content">
						AED 2,380
					</div>
				</div>
			</div>

			<!-- 3x per week -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Number of Sessions
					</div>

					<div class="content">
						3x per week
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Rates per Session
					</div>

					<div class="content">
						AED 75
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full Term
					</div>

					<div class="content">
						AED 3,150
					</div>
				</div>
			</div>

		</div>
		
		<div class="text-center mt-40">
			<a href="#" class="btn --btn-primary tt-capital" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<hr class="divider" />

<!-- Athletics Camp -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-2 order-lg-last d-none d-md-block">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdathletics.svg" alt="">
				</picture>
			</div>

			<div class="col-lg-10">
				<h2 class="maintitle">ISD Athletics Academy Camps</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Join us during Spring, Summer and Winter breaks for an incredible array of track and field activities! <br><br> Our unique programs are customized for the age groups to keep children active, improving their speed, jumping and throwing skills while acquiring new knowledge and techniques. At the same time developing their agility, confidence and leadership skills to compete with themselves!
			</p>
		</div>

		<!-- Pricing Table -->
		<h2 class="maintitle">Pricing</h2>

		<div class="table-responsive">
			<table class="table --schedule-table text-left">
				<tr>
					<th>
						Duration
					</th>
					<th>
						Fees
					</th>
				</tr>

				<tr>
					<td>
						Daily
					</td>
					<td>
						AED 150
					</td>
				</tr>

				<tr>
					<td>
						One Week
					</td>
					<td>
						AED 600
					</td>
				</tr>

				<tr>
					<td>
						Two Weeks
					</td>
					<td>
						AED 1,000
					</td>
				</tr>
			</table>
		</div>

		
		<div class="text-center mt-40">
			<a href="#" class="btn --btn-primary tt-capital" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

@endsection