@extends('layouts.app')
@section('pageClass', 'athletics innerpage')
@section('title', 'Athletics Academy in Dubai')
@section('description', 'Athletics Academy in Dubai')
@section('keywords', 'Track and field, running track, sprints runs, middle distance runs, long distance runs, hurdles, shot put, discus, javelin, long jump, triple jump, high jump, strength and conditioning, ultimate athletics dubai, athletics training in dubai')
@section('content')

<section class="hero-banner --video-banner">

	<video playsinline autoplay muted loop poster="https://img.youtube.com/vi/WdWL47LW7jw/maxresdefault.jpg" class="video">
		<!-- <source src="polina.webm" type="video/webm"> -->
		<source src="/assets-web/videos/ISD_Athletics_Academy.mp4" type="video/mp4">
	</video>

</section>

<!-- About Athletics -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-2 order-lg-last d-none d-lg-block">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdathletics.svg" alt="">
				</picture>
			</div>

			<div class="col-lg-10">
				<h2 class="maintitle">ISD Athletics Academy</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Join ISD Athletics Academy at Dubai Sports City today  and enjoy a unique track and field program  developed by our Olympian head coach Andy Turner,  three-time medalist and Commonwealth and European Champion.
 				<br><br>
				Experience the ISD Athletics Academy difference and  see your child build speed, agility, confidence and stamina, while having lots of fun! 
 				<br><br>
				We look forward to seeing you on the track!
			</p>
		</div>

		<h2 class="maintitle">Schedule</h2>
		<div class="table-responsive">
			<table class="table --schedule-table athletics text-left">
				<tr>
					<th>
						Days
					</th>
					<th>
						Time
					</th>
					<th>
						Age Group
					</th>
				</tr>

				<tr>
					<td>
						Sunday to Thursday
					</td>
					<td>
						5 pm to 6 pm
					</td>
					<td>
						5-8 years old
					</td>
				</tr>

				<tr>
					<td>
						Sunday to Thursday
					</td>
					<td>
						6 pm to 7 pm
					</td>
					<td>
						9-12 years old
					</td>
				</tr>

				<tr>
					<td>
						Sunday to Thursday
					</td>
					<td>
						7 pm to 8 pm
					</td>
					<td>
						13-18 years old
					</td>
				</tr>

				<tr>
					<td>
						Saturday
					</td>
					<td>
						5 pm to 6 pm
					</td>
					<td>
						5-8 years old
					</td>
				</tr>

				<tr>
					<td>
						Saturday
					</td>
					<td>
						6 pm to 7 pm
					</td>
					<td>
						9-12 &amp; 13-18 years old
					</td>
				</tr>
			</table>
		</div>

		
		<div class="text-center mt-40">
			<a href="#" class="btn --btn-primary tt-capital" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<!-- Athletics Camp -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-2 order-lg-last d-none d-md-block">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdathletics.svg" alt="">
				</picture>
			</div>

			<div class="col-lg-10">
				<h2 class="maintitle">ISD Athletics Academy Camps</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Join us during Spring, Summer and Winter breaks for an incredible array of track and field activities! <br><br> Our unique programs are customized for the age groups to keep children active, improving their speed, jumping and throwing skills while acquiring new knowledge and techniques. At the same time developing their agility, confidence and leadership skills to compete with themselves!
			</p>
		</div>

		<!-- Schedule Table -->
		<h2 class="maintitle">Schedule</h2>
		<h3>
			Sundays and Tuesdays
		</h3>
		<div class="table-responsive">
			<table class="table --schedule-table athletics text-left">
				<tr>
					<th>
						Days
					</th>
					<th>
						Detail
					</th>
				</tr>

				<tr>
					<td>
						9:00 am
					</td>
					<td>
						Welcome
					</td>
				</tr>

				<tr>
					<td>
						9:05 am
					</td>
					<td>
						Warm up and conditioning exercises
					</td>
				</tr>

				<tr>
					<td>
						9:15 am
					</td>
					<td>
						Agility Obstacle course
					</td>
				</tr>

				<tr>
					<td>
						10:00 am
					</td>
					<td>
						Sprints
					</td>
				</tr>

				<tr>
					<td>
						10:45 am
					</td>
					<td>
						Athletics Circuit
					</td>
				</tr>

				<tr>
					<td>
						11:00 am
					</td>
					<td>
						Snack Break
					</td>
				</tr>

				<tr>
					<td>
						11:15 am
					</td>
					<td>
						Throws (Javelin, shot put, discus)
					</td>
				</tr>

				<tr>
					<td>
						12:00 pm
					</td>
					<td>
						Pick Up
					</td>
				</tr>
			</table>
		</div>


		<h3>
			Mondays and Wednesdays
		</h3>
		<div class="table-responsive">
			<table class="table --schedule-table athletics text-left">
				<tr>
					<th>
						Days
					</th>
					<th>
						Detail
					</th>
				</tr>

				<tr>
					<td>
						9:00 am
					</td>
					<td>
						Welcome
					</td>
				</tr>

				<tr>
					<td>
						9:05 am
					</td>
					<td>
						Warm up and conditioning exercises
					</td>
				</tr>

				<tr>
					<td>
						9:15 am
					</td>
					<td>
						Long and Triple jump
					</td>
				</tr>

				<tr>
					<td>
						10:00 am
					</td>
					<td>
						Sprint/Hurdles
					</td>
				</tr>

				<tr>
					<td>
						11:00 am
					</td>
					<td>
						Snack Break
					</td>
				</tr>

				<tr>
					<td>
						11:15 am
					</td>
					<td>
						High jump
					</td>
				</tr>

				<tr>
					<td>
						12:00 pm
					</td>
					<td>
						Pick Up
					</td>
				</tr>
			</table>
		</div>



		<h3>
			Thursdays: Compete with yourself!
		</h3>
		<div class="table-responsive">
			<table class="table --schedule-table athletics text-left">
				<tr>
					<th>
						Days
					</th>
					<th>
						Detail
					</th>
				</tr>

				<tr>
					<td>
						9:00 am
					</td>
					<td>
						Welcome
					</td>
				</tr>

				<tr>
					<td>
						9:05 am
					</td>
					<td>
						Warm up and conditioning exercises
					</td>
				</tr>

				<tr>
					<td>
						9:30 am
					</td>
					<td>
						Competitions
					</td>
				</tr>

				<tr>
					<td>
						12:00 pm
					</td>
					<td>
						Pick Up
					</td>
				</tr>
			</table>
		</div>

		
		<div class="text-center mt-40">
			<a href="#" class="btn --btn-primary tt-capital" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

@endsection