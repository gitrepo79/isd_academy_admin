@extends('layouts.app')
@section('pageClass', 'sportsdetailpg innerpage')
@section('title', "ISD Athletics | Home to Dubai's best track and field facility")
@section('description', 'Experience the best track & field venue in Dubai at ISD Athletics, Dubai Sports City. Featuring a 9-lane Olympic-grade track, long jump and high jump facilities and all throwing disciplines. Fast and easy booking 7-days-a-week, convenient location, free parking and full amenities at your fingertips. ')
@section('keywords', '')
@section('content')

<section class="hero-banner --inner-banner" style="background-image: url('/assets-web/images/banners/athletics-venue.webp');">


</section>

<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">

			<div class="col-lg-10">
				<h2 class="maintitle">
					<span class="fc-football">ISD ATHLETICS</span>
				</h2>
			</div>
		</div>

		<hr class="divider">

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Stay active with ISD Athletics boasting the best track & field venue located in the heart of the city. Enjoy the 9-lane Olympic standard track, long jump and high jump facilities as well capacity for all throwing disciplines.  <br><br> Membership packages are daily, monthly or 3 months. <br><br> Get your membership today. <br><br>
				The ISD Athletics is also available for event bookings 7 days a week. Call us today for more information. 
			</p>
		</div>

		<div class="text-center">
			<a href="#" class="btn --btn-primary ">CALL ON 058 285 8239 TO BOOK</a>
		</div>

	</div>
</section>


<hr class="divider" />

<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<h2 class="maintitle">Pricing</h2>


		{{-- Outdoor Rate --}}

		<h2 class="fc-football">Athletics Track</h2>
		<h6>Use from 07:00 AM - 03:00 PM & from 07:30 PM - 11:00 PM, Track is for individual use only, those wishing to book to run training programs with or without equipment please call for a quote.</h6>


		<div class="Rtable --collapse --2cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				Detail
			</div>

			<div class="Rtable-cell --head">
				Price
			</div>

			<!-- Track -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Daily Rate
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Price
					</div>

					<div class="content">
						AED 45
					</div>
				</div>
			</div>

			<!-- Track -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Monthly Rate
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Price
					</div>

					<div class="content">
						AED 270
					</div>
				</div>
			</div>

			<!-- Track -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						3 Month Rate
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Price
					</div>

					<div class="content">
						AED 526.5
					</div>
				</div>
			</div>
		</div>


		<h2 class="fc-football">Athletics Track &amp; Gym</h2>
		<h6>Use from 07:00 AM - 03:00 PM & from 07:30 PM - 11:00 PM, Track is for individual use only, those wishing to book to run training programs with or without equipment please call for a quote.</h6>


		<div class="Rtable --collapse --2cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				Detail
			</div>

			<div class="Rtable-cell --head">
				Price
			</div>

			<!-- Track -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Daily Rate
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Price
					</div>

					<div class="content">
						AED 65
					</div>
				</div>
			</div>

			<!-- Track -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Monthly Rate
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Price
					</div>

					<div class="content">
						AED 390
					</div>
				</div>
			</div>

			<!-- Track -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						3 Month Rate
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Price
					</div>

					<div class="content">
						AED 760.5
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

@endsection