  @extends('layouts.user')
  @section('content')
  <h3 class="pagetitle">Dashboard</h3>
  <section class="box">
    <div class="box-header">
      <p class="box-title">Bookings</p>
    </div>
    <div class="box-body">
      <form class="default-form">
        <div class="row">
          
          <div class="col-md-3">
            <div class="control-group">
              <label class="form-label">Select the Date</label>
              <input type="text" name="date" class="form-field datepicker" autocomplete="off" data-row-id="1" id="date" required>
            </div>
          </div>
          <div class="col-md-3">
            <div class="control-group">
              <label class="form-label">Time From</label>
              <input type="time" name="time" class="form-field" autocomplete="off" data-row-id="1" id="time" required>
            </div>
          </div>
          <div class="col-md-3">
            <div class="control-group">
              <label class="form-label">Select Duration</label>
              <select  class="form-field " required name="duration" id="duration">
                <option value=" ">Select Duration</option>
                <option value="60">60</option>
                <option value="90">90</option>
                <option value="120">120</option>
                
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="control-group">
              <label class="form-label">Select Sports</label>
              <select  class="form-field " required name="sport" id="sport">
                <option value="">Select Sports</option>
                @foreach($sports['data'] as $sport)
                <option value="{{ $sport['id'] }}">{{ $sport['name'] }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <button type="button" class="btn --btn-primary" id="confirm">Confirm Booking</button>
          </div>
      </div>
      </form>
      <div class="box-body">
      
      <div id="court"></div>
   
      </div>
    </div>
    <script>
    var dateToday = new Date();
    $( "#date" ).datepicker({
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
    
    $('#confirm').click(function(){
    var id=$("#sport").val();
    var time=$('#time').val();
    var duration=$("#duration").val();
    var date=$('#date').val();
    $.ajax({
    type: 'POST',
    url: 'https://bookings.isddubai.com/api/v1/check/courts',
    data: {id:id,time:time,duration:duration,date:date},
    dataType: 'json',
    success: function (response) {
    var len = 0;
    if (response.data != null) {
    len = response.data.length;
    }
    if (len>0) {
    var option ="";
    option+='<table class="table --bordered --hover">';
    option+='<tr>';
    option+='<th><center>Court Name</center></th>';
    option+='<th><center>Rate</center></th>';
    option+='<th><center>Action</center></th>';
    option+='</tr>';
    
    for (var i = 0; i<len; i++) {
    var id = response.data[i].id;
    var name = response.data[i].name;
    var rate = response.data[i].rate;
    
    option+='<tr>';
    option+= "<td><center>"+name+"</center></td>";
    option+= "<td><center>"+rate+"</center></td>";
    option+= "<td><center><a href=''><button type='button' class='btn --btn-small bg-info fc-white'>Book</button></a></center></td>";
    option+='</tr>';
    
   
    }
     $("#court").append(option);
    }
    },
    error: function (response) {
    console.log(response);
    }
    });
    });
    });
    </script>
  </section>
  @endsection