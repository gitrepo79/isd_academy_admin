@extends('layouts.app')
@section('pageClass', 'athleticpg inner-page')
@section('title', 'Email Verification')
@section('pageClass', 'athleticpg inner-page')
@section('content')
<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/default-banner.jpg');">
    <div class="inner-content --registration">
    </div>
    <div class="vertical-heading">
        <h2 class="title">Verify Your Email</h2>
    </div>
    <div class="content-section">
        <article class="container-wrapper">
            <div class="clearfix">
                <section class="col-xl-5 offset-xl-4 col-lg-6 offset-lg-3">
                <div class="box --registration-box">
                    <h4 class="fc-white mb-40">Enter Verification Code before Continuing</h4>
                    <form class="default-form --registration-form">
                        
                        <div class="control-group{{ $errors->has('code') ? ' has-error' : '' }}">
                            {!! Form::text('code', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Enter Your Verfication Code here ','id'=>'code','autocomplete'=>'off']) !!}
                            @if ($errors->has('player_nam'))
                            <span class="form-error">
                                <strong>{{ $errors->first('code') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="control-group">
                          <button type="button" id="verify" class="btn --btn-primary">
                            {{ __('Submit') }}
                            </button>
                        </div>
                </section>
            </div>
        </article>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
    
    $('#verify').click(function(){

    var code=$("#code").val();
    var id={{$id}};
    $.ajax({
    type: 'POST',
    url: 'https://bookings.isddubai.com/api/v1/users/register/confirm',
    data: {id:id,code:code},
    dataType: 'json',
    success: function (data) {
    console.log(data);
    var status = data.status;
    var errormsg = data.msg;
    if(status==200) 
    {
        window.location.href = "{{ route('user.bookings')}}";
    }
    else if(status==500){
      
      alert(errormsg);
    }
    },
    error: function (data) {
    console.log(data);
    }
    });

    });

    });
    </script>
   </section>
<!-- Event snippet for Submit lead form - Popup conversion page -->

@endsection