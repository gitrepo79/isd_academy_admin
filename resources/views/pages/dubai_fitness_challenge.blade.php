@extends('layouts.app')
@section('pageClass', 'innerpage')
@section('title', 'Dubai 30x30 Challenge')
@section('description', 'Dubai 30x30 Challenge')
@section('keywords', '')
@section('content')


<!-- About Tennis -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<!-- <div class="col-lg-2 order-lg-last">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdtennis.svg" alt="">
				</picture>
			</div> -->

			<div class="col-lg-10">
				<h2 class="maintitle">
					<span class="fc-football">Dubai 30x30 Challenge</span>
				</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				A week filled with exciting sports and fitness activities for the fitness community across all abilities; adults, youth and families - all under one roof at Inspiratus Sports District, Dubai Sports City. <br><br>

				As part of Dubai 30x30 Challenge, ISD offers the community a fitness-focused hub that inspires them to be active with an array of different fitness programs. <br><br>

				Individuals and families alike can enjoy a series of 30-minute daily activities. 
			</p>
		</div>

		
		<div class="text-center mb-20">
			<h2 class="fc-primary">CALL US NOW FOR TO BOOK YOUR SPOT: <span class="fc-football">04 4481650</span></h2>
		</div>

		<div class="text-center mt-0">
			<a href="mailto:sports@isddubai.com	" class="btn --btn-primary" data-targetmodal="popup-form">or Email: sports@isddubai.com</a>
		</div>

	</div>
</section>

<hr class="divider" />

<!-- Schedule -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">The Schedule</span>
				</h2>
			</div>
		</div>

		<div class="Rtable --schedule-table --collapse --7cols text-center mt-0">

	        <div class="Rtable-cell --head text-center">
	            Sunday <br> 7 Nov
	        </div>

	        <div class="Rtable-cell --head text-center">
	            Monday <br> 8 Nov
	        </div>

	        <div class="Rtable-cell --head text-center">
	            Tuesday <br> 9 Nov
	        </div>

	        <div class="Rtable-cell --head text-center">
	            Wednesday <br> 10 Nov
	        </div>

	        <div class="Rtable-cell --head text-center">
	            Thursday <br> 11 Nov
	        </div>

	        <div class="Rtable-cell --head text-center">
	            Friday <br> 12 Nov
	        </div>

	        <div class="Rtable-cell --head text-center">
	            Saturday <br> 13 Nov
	        </div>


	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Sunday
	                </div>

	                <div class="content">
	                    Power Training  Strength & Conditioning) <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Monday
	                </div>

	                <div class="content">
	                    Athletics Yoga <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Tuesday
	                </div>

	                <div class="content">
	                    Stretch and Mobility Fitness Session <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Wednesday
	                </div>

	                <div class="content">
	                   Power Training (Strength & Conditioning) <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Thursday
	                </div>

	                <div class="content">
	                    Athletics Yoga <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Friday
	                </div>

	                <div class="content">
	                    Family Fitness <br>5-6am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Saturday
	                </div>

	                <div class="content">
	                    Stretch and Mobility Fitness Session <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Sunday
	                </div>

	                <div class="content">
	                    Power Training  Strength & Conditioning) <br>8-9am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Monday
	                </div>

	                <div class="content">
	                    Athletics Yoga <br>8-9am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Tuesday
	                </div>

	                <div class="content">
	                    Stretch and Mobility Fitness Session <br>8-9am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Wednesday
	                </div>

	                <div class="content">
	                   Power Training (Strength & Conditioning) <br>8-9am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Thursday
	                </div>

	                <div class="content">
	                    Athletics Yoga <br>8-9am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Friday
	                </div>

	                <div class="content">
	                    Family Fitness <br>6-7am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Saturday
	                </div>

	                <div class="content">
	                    Stretch and Mobility Fitness Session <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>



	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Sunday
	                </div>

	                <div class="content">
	                     4 x 100m Relay Runs <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Monday
	                </div>

	                <div class="content">
	                    3k Run <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Tuesday
	                </div>

	                <div class="content">
	                    30m Dash (Laser Timed) <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Wednesday
	                </div>

	                <div class="content">
	                    4 x 100m Relay Runs <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Thursday
	                </div>

	                <div class="content">
	                    3k Run <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Friday
	                </div>

	                <div class="content">
	                    Family Run <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Saturday
	                </div>

	                <div class="content">
	                    30m Dash (Laser Timed) <br>7-8am <br>ISD Sports Park
	                </div>
	            </div>
	        </div>

	    </div>

	</div>
</section>

<hr class="divider" />

@endsection