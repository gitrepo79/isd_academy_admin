@extends('layouts.app')
@section('pageClass', 'innerpage eupepsia')
@section('title', "Eupepsia Sports Science & Wellness | Dubai's best sports injury specialists")
@section('description', "Eupepsia Sports Science & Wellness is Dubai's leading sports performance, injury and physiotherapy specialist clinic located at ISD, Dubai Sports City. Elite level training and recovery programs and innovative wellness therapies bring you to your best self in the shortest time possible.")
@section('content')

<!-- About Eupepsia -->
<section class="aboutpage-section --eupepsia-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-2 order-lg-last">
				<picture class="logo-icon --big">
					<img src="/assets-web/images/logos/eupepsia.svg" alt="">
				</picture>
			</div>

			<div class="col-lg-10">
				<h2 class="maintitle">EUPEPSIA <br>SPORTS SCIENCE & WELLNESS</h2>
			</div>
		</div>

		<div class="content-section">
			<p class="maindesc --big">
				Eupepsia Sports Science and Wellness clinic offers an integrated approach to help you achieve your highest potential with elite level strengthening, conditioning and recovery programs featuring cutting edge therapies and treatments. <br><br> Experience Eupepsia’s award-winning wellness programs that combine advanced health screenings with traditional as well as progressive therapies to support you in achieving your health and well-being goals and boost your performance in every aspect of your life. <br><br> Located in the heart of Dubai Sports City at ISD and specialising in Sports Physiotherapy, Diet & Nutrition, Holistic Medicine and Wellness Therapies, Eupepsia draws on sports science as well as the science of Ayurveda, combining technology-advanced and natural therapies for accelerated recovery and healing, treating the root cause of imbalances and conditions.
			</p>
		</div>

		<div class="row">
			<div class="col-lg-4">
				<div class="box --eupepsia-services-box">
					<h4 class="title">
						physiotherapy
					</h4>
					<picture>
						<img src="/assets-web/images/gallery/eupepsia/physiotherapy.jpg" alt="">
					</picture>
					<article class="inner-content">
						<p class="subtitle">
							Sports Physiotherapy & Recovery
						</p>

						<p class="desc">
							Advanced technologies, innovative techniques & exercise therapies to treat injuries allowing faster pain relief, healing and recovery.
						</p>

						<a href="https://eupepsia.ae/public/sports-science#physiotherapy" target="_blank" class="btn --btn-primary">read more</a>
					</article>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="box --eupepsia-services-box">
					<h4 class="title">
						SPORTS RECOVERY
					</h4>
					<picture>
						<img src="/assets-web/images/gallery/eupepsia/recovery.jpg" alt="">
					</picture>
					<article class="inner-content">
						<p class="subtitle">
							Strength & Conditioning
						</p>

						<p class="desc">
							Sports Conditioning builds athletic endurance, strength and mental aptitude to withstand prolong training and to delay fatigue onset. 
						</p>

						<a href="https://eupepsia.ae/public/sports-science#recovery" target="_blank" class="btn --btn-primary">read more</a>
					</article>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="box --eupepsia-services-box">
					<h4 class="title">
						YOUNG ATHLETE
					</h4>
					<picture>
						<img src="/assets-web/images/gallery/eupepsia/young.jpg" alt="">
					</picture>
					<article class="inner-content">
						<p class="subtitle">
							Young Athlete Development
						</p>

						<p class="desc">
							Improve your child's performance in nearly any sport, from athletics, football, tennis to gymnastics or dance.
						</p>

						<a href="https://eupepsia.ae/public/sports-science#athletics" target="_blank" class="btn --btn-primary">read more</a>
					</article>
				</div>
			</div>
		</div>

	</div>
</section>

<hr class="divider" />

<!-- About Eupepsia -->
<section class="aboutpage-section --eupepsia-section">
	<div class="container-wrapper">

		<div class="row">
			<div class="col-lg-4">
				<div class="box --eupepsia-services-box --brown">
					<h4 class="title">
						healing
					</h4>
					<picture>
						<img src="/assets-web/images/gallery/eupepsia/healing.jpg" alt="">
					</picture>
					<article class="inner-content">
						<p class="subtitle">
							Recharge your Immunity and Heal
						</p>

						<p class="desc">
							Anti-Stress and Anti-Anxiety Treatments <br> Restoration of Good Digestion <br> Immunity Boosting
						</p>

						<a href="https://eupepsia.ae/public/wellness#therapies" target="_blank" class="btn --btn-primary">read more</a>
					</article>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="box --eupepsia-services-box --green">
					<h4 class="title">
						RENEWING
					</h4>
					<picture>
						<img src="/assets-web/images/gallery/eupepsia/rejuvenate.jpg" alt="">
					</picture>
					<article class="inner-content">
						<p class="subtitle">
							Rejuvenate & Restore Balance
						</p>

						<p class="desc">
							Body Rejuvenation Programs <br> Anti-Aging Marma Facials <br> Hair and Skin Renewal Therapies
						</p>

						<a href="https://eupepsia.ae/public/wellness#therapies" target="_blank" class="btn --btn-primary">read more</a>
					</article>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="box --eupepsia-services-box --blue">
					<h4 class="title">
						slimming
					</h4>
					<picture>
						<img src="/assets-web/images/gallery/eupepsia/detox.jpg" alt="">
					</picture>
					<article class="inner-content">
						<p class="subtitle">
							Detox & Reset Your Weight
						</p>

						<p class="desc">
							Clinical Dietitian supervised <br> Bio-Individual Nutrition <br> Detox & Slimming Therapies <br> Ayurvedic Panchakarma Packages
						</p>

						<a href="https://eupepsia.ae/public/wellness#nutrition" target="_blank" class="btn --btn-primary">read more</a>
					</article>
				</div>
			</div>
		</div>

	</div>
</section>

<hr class="divider" />

<section class="aboutpage-section --eupepsia-section">
	<div class="container-wrapper">
		<div class="more-detail">
			<div class="logo">
				<img src="/assets-web/images/logos/eupepsia.svg" alt="">
			</div>

			<div class="detail">
				<p class="text">For more info please visit:</p>
				<a href="https://eupepsia.ae/" target="_blank" class="btn --btn-primary --eupepsia">EUPEPSIA.AE</a>
			</div>
		</div>
	</div>
</section>

@endsection