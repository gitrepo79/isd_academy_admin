@extends('layouts.app')
@section('pageClass', 'footballpg inner-page')
@section('title', ' LaLiga Academy UAE | Best Football Academy in Dubai')
@section('description', "LaLiga Academy is Dubai's best Football Academy with professional UEFA Pro-certified Spanish coaches delivering the highest standard of football training for boys and girls aged 4-19 of all abilities in Dubai Sports City.")
@section('keywords', 'football academy, football dubai, football academy dubai, football clubs in dubai, dubai football, best football academy in dubai, football coaching in dubai, football training in dubai')
@section('content')

<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/football-inner-banner.webp');">

	<div class="inner-content --football">
		<div class="vertical-heading">
			<h1 class="title">Football</h1>
		</div>
	</div>

	<div class="content-section --scroll-page">
		<article>
			<p class="mobile-content">
				Boasting a selection of world-class pitches to rent with friends, the strongest football academy in the country, LaLiga Academy, and hosting local and international tournaments, ISD Football at Dubai Sports City is your football venue of choice.
			</p>
			<div class="inner-page-content">

				<div class="box --round-box">
					<article>
						<h2 class="title">rental</h2>

						<p class="desc"> Want to play a match or host your own tournament? You can rent one of ISD Football’s world-class indoor or outdoor pitches.</p>

						<a href="#" class="book-btn animsition-link">052 519 3472</a>
					</article>
				</div>

				<div class="box --round-box">
					<article>
						<h2 class="title">academy</h2>

						<p class="desc">LaLiga Academy offers young players across the UAE an immersive, professional football experience, with a superior methodology recognized as the gold standard across the world.</p>

						<a href="https://ae.laligacademy.com/registration" class="book-btn animsition-link">Register</a>
					</article>
				</div>

				<div class="box --round-box">
					<article>
						<h2 class="title"> leagues <small class="fs-large">&amp; tournaments</small></h2>

						<p class="desc">ISD Football boasts the best facilities in the region to make your event an unforgettable experience.</p>

						<a href="#" class="book-btn animsition-link">052 519 3472</a>
					</article>
				</div>

			</div>

			<div class="gallery-section">
				<h2 class="maintitle">Gallery</h2>

				<div class="gallery-slider">
					<div class="item">
						<a href="/assets-web/images/gallery/football/img1.webp" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/img1.webp">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/img2.webp" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/img2.webp">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/img3.webp" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/img3.webp">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/img4.webp" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/img4.webp">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/img5.webp" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/img5.webp">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/img6.webp" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/img6.webp">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/img7.webp" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/img7.webp">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/img8.webp" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/img8.webp">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/img9.webp" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/img9.webp">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/img10.webp" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/img10.webp">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/img11.webp" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/img11.webp">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/img12.webp" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/img12.webp">
						</a>
					</div>

				</div>
			</div>
		</article>
	</div>

</section>

@endsection