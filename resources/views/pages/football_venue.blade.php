@extends('layouts.app')
@section('pageClass', 'sportsdetailpg innerpage')
@section('title', 'ISD Football | Home to the best football pitches in Dubai')
@section('description', 'Play on the best football pitches in the region at ISD Football, Dubai Sports City with four FIFA standard and UAE FA approved full-size grass pitches and the UAE’s only indoor astroturf 3G football pitch. Fast and easy booking 7-days-a-week convenient location, free parking and full amenities at your fingertips.')
@section('keywords', '')
@section('content')

<section class="hero-banner --inner-banner" style="background-image: url('/assets-web/images/banners/football-venue.webp');">


</section>

<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">

			<div class="col-lg-10">
				<h2 class="maintitle">
					<span class="fc-football">ISD FOOTBALL PITCH HIRE</span>
				</h2>
			</div>
		</div>

		<hr class="divider">

		<div class="content-section mb-40">
			<p class="maindesc --big">
				ISD Football at Dubai Sports City is a new world-class hub for football players and athletes and is the playing venue of choice for recreational and professional tournaments. <br><br>
				ISD Football at Dubai Sports City boasts one of the region’s best sports facilities with world-class football pitches and extensive indoor and outdoor grounds. <br><br> It has the UAE’s only indoor 3G football pitch and three outdoor FIFA Standard and UAE FA approved full-size pitches, two of them natural grass pitches and one artificial turf pitch. The facility also features full-sized changing rooms to accommodate international teams. <br><br> Book your pitch today.
			</p>
		</div>

		<div class="text-center">
			<a href="#" class="btn --btn-primary ">CALL ON 052 519 3472 TO BOOK</a>
		</div>

	</div>
</section>


<hr class="divider" />

<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<h2 class="maintitle">Pricing</h2>


		{{-- Artificial Rate --}}
		<h2 class="fc-football">Artificial</h2>

		<h6><span class="fc-football">Peak Times &amp; Prices:</span> Sunday - Thursday 7:30 PM - 10:30 PM & Friday 4:30 PM - 10:30 PM</h6>

		<div class="Rtable --collapse --4cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				Pitch Detail
			</div>

			<div class="Rtable-cell --head">
				Full
			</div>

			<div class="Rtable-cell --head">
				Half
			</div>

			<div class="Rtable-cell --head">
				Quarter
			</div>

			<!-- Artificial Indoor -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Artificial - Indoor
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full
					</div>

					<div class="content">
						AED 1380
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Half
					</div>

					<div class="content">
						AED 735
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Quarter
					</div>

					<div class="content">
						AED 510
					</div>
				</div>
			</div>


			<!-- Artificial Outdoor -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Artificial - Outdoor
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full
					</div>

					<div class="content">
						AED 1250
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Half
					</div>

					<div class="content">
						AED 625
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Quarter
					</div>

					<div class="content">
						AED 450
					</div>
				</div>
			</div>
		</div>

		<h6 class="mt-40"><span class="fc-football">Off Peak Times &amp; Prices:</span> Sunday - Saturday 7:30 AM - 3:30 PM</h6>

		<div class="Rtable --collapse --4cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				Pitch Detail
			</div>

			<div class="Rtable-cell --head">
				Full
			</div>

			<div class="Rtable-cell --head">
				Half
			</div>

			<div class="Rtable-cell --head">
				Quarter
			</div>

			<!-- Artificial Indoor -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Artificial - Indoor
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full
					</div>

					<div class="content">
						AED 980
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Half
					</div>

					<div class="content">
						AED 490
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Quarter
					</div>

					<div class="content">
						AED 380
					</div>
				</div>
			</div>


			<!-- Artificial Outdoor -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Artificial - Outdoor
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full
					</div>

					<div class="content">
						AED 950
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Half
					</div>

					<div class="content">
						AED 475
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Quarter
					</div>

					<div class="content">
						AED 360
					</div>
				</div>
			</div>
		</div>

		{{-- Grass Rate --}}
		<h2 class="fc-football">Natural Grass</h2>

		<h6><span class="fc-football">Peak Times &amp; Prices:</span> Sunday - Thursday 7:30 PM - 10:30 PM & Friday 4:30 PM - 10:30 PM</h6>

		<div class="Rtable --collapse --3cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				Pitch Detail
			</div>

			<div class="Rtable-cell --head">
				Full
			</div>

			<div class="Rtable-cell --head">
				Half
			</div>

			<!-- Grass 1 -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Grass 1 - ISD Football
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full
					</div>

					<div class="content">
						AED 1360
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Half
					</div>

					<div class="content">
						AED 680
					</div>
				</div>
			</div>


			<!-- Grass 2 -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Grass 2 - ISD Football
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full
					</div>

					<div class="content">
						AED 1360
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Half
					</div>

					<div class="content">
						AED 680
					</div>
				</div>
			</div>

			<!-- Grass 3 -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Grass 3 - ISD Sports Park Stadium
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full
					</div>

					<div class="content">
						AED 1480
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Half
					</div>

					<div class="content">
						AED 740
					</div>
				</div>
			</div>


			<!-- Grass 4 -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Grass 4 - ISD Sports Park
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full
					</div>

					<div class="content">
						AED 1260
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Half
					</div>

					<div class="content">
						AED 630
					</div>
				</div>
			</div>

		</div>

		<h6 class="mt-40"><span class="fc-football">Off Peak Times &amp; Prices:</span> Sunday - Saturday 7:30 AM - 3:30 PM</h6>

		<div class="Rtable --collapse --3cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				Pitch Detail
			</div>

			<div class="Rtable-cell --head">
				Full
			</div>

			<div class="Rtable-cell --head">
				Half
			</div>

			<!-- Grass 1 -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Grass 1 - ISD Football
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full
					</div>

					<div class="content">
						AED 1080
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Half
					</div>

					<div class="content">
						AED 540
					</div>
				</div>
			</div>


			<!-- Grass 2 -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Grass 2 - ISD Football
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full
					</div>

					<div class="content">
						AED 1080
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Half
					</div>

					<div class="content">
						AED 540
					</div>
				</div>
			</div>

			<!-- Grass 3 -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Grass 3 - ISD Sports Park Stadium
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full
					</div>

					<div class="content">
						AED 1260
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Half
					</div>

					<div class="content">
						AED 630
					</div>
				</div>
			</div>


			<!-- Grass 4 -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Grass 4 - ISD Sports Park
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Full
					</div>

					<div class="content">
						AED 1000
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Half
					</div>

					<div class="content">
						AED 510
					</div>
				</div>
			</div>

		</div>

	</div>
</section>

<hr class="divider" />


<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="gallery-section">
				<h2 class="maintitle">Gallery</h2>

				<div class="gallery-slider">
					<div class="item">
						<a href="/assets-web/images/gallery/football/gallery1.jpg" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/gallery1.jpg">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/gallery2.jpg" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/gallery2.jpg">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/gallery3.jpg" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/gallery3.jpg">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/gallery4.jpg" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/gallery4.jpg">
						</a>
					</div>

					<div class="item">
						<a href="/assets-web/images/gallery/football/gallery5.jpg" data-fancybox="gallery">
							<img src="/assets-web/images/gallery/football/gallery5.jpg">
						</a>
					</div>

				</div>
			</div>
	</div>
</section>


@endsection