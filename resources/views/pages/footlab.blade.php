@extends('layouts.app')
@section('pageClass', 'innerpage footlab')
@section('title', "Footlab Dubai | World's first Indoor performance and entertainment park")
@section('description', "Welcome to the future of football at Footlab Dubai. Located at ISD, Dubai Sports City, Footlab's tech super-charged stations empower players, teams and groups of friends with challenges to test and boost their football skills, speed, technique, strength, precision and awareness!")
@section('content')

<section class="hero-banner --inner-banner" style="background-image: url('/assets-web/images/banners/footlab.webp');">


</section>

<!-- About Footlab -->
<section class="aboutpage-section --laliga-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-2 order-lg-last d-none d-lg-block">
				<picture class="logo-icon --laliga">
					<img src="/assets-web/images/logos/footlab.svg" alt="">
				</picture>
			</div>

			<div class="col-lg-10">
				<h2 class="maintitle">what is footlab?</h2>
			</div>
		</div>

		<div class="content-section">
			<p class="maindesc --big">
				Welcome to the future of Football at FOOTLAB, the world's first indoor performance and entertainment park. With its world unique technology, FOOTLAB empowers players, teams and groups of friends with challenges to test and boost their football skills, speed, technique, strength, precision and awareness! FOOTLAB’s exclusive patented software and hardware provides unique sports analysis data that can be applied equally to professional and everyday athletes!
			</p>
		</div>
	</div>
</section>

<hr class="divider" />

<section class="aboutpage-section --laliga-section">
	<div class="container-wrapper">
		<h2 class="maintitle text-center">gallery</h2>

		<div class="gallery-section">

			<div class="gallery-slider">
				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img1.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img1.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img2.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img2.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img3.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img3.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img4.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img4.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img5.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img5.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img6.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img6.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img7.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img7.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img8.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img8.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img9.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img9.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img10.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img10.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img11.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img11.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img12.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img12.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img13.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img13.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/footlab/img14.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/footlab/img14.jpg" alt="">
					</a>
				</div>
			</div>
		</div>

		<div class="text-center mt-40">
			<a href="https://footlabworld.com/en/footlab/dubai" class="btn --btn-primary register-now" target="_blank">visit footlab world</a>
		</div>

	</div>
</section>

@endsection