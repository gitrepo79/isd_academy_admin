@extends('layouts.app')
@section('pageClass', 'innerpage')
@section('title', 'ISD Dubai | The Best Sports Facility in Dubai')
@section('description', 'ISD is Dubai’s best sports venue and the home of world-class academies, football pitches, tennis courts, Olympic-grade running track, and world class padel courts, located in Dubai Sports City.')
@section('keywords', 'academy sports, sports academy dubai, Inspiratus Sports District, ISD Academy, ISD Dubai, ISD Athletics')
@section('content')


<!-- Hero Banner -->
<section class="hero-banner home-banner --video-banner">

	<video playsinline autoplay muted loop poster="https://img.youtube.com/vi/jka4apGFnzc/maxresdefault.jpg" class="video">
		<!-- <source src="polina.webm" type="video/webm"> -->
		<source src="/assets-web/videos/ISD_Dubai_Home.mp4" type="video/mp4">
	</video>

</section>

<!-- About Section -->
<section class="aboutpage-section">
	<div class="container-wrapper">
		<h2 class="maintitle text-center">compete with yourself</h2>
		<p class="maindesc --big text-center">
			ISD Academy offers a world-class year-round training and education program customized for sports players from all around the world to experience living in cosmopolitan Dubai, learn and receive football training at the highest international standards. The program is open to boys and girls aged 11-16 years old.
		</p>

	</div>
</section>

<section class="aboutpage-section">
	<div class="container-wrapper">
		<div class="row">
			<div class="col-lg-4">
				<div class="box --academy-programs-box">
					
					<article class="inner-content">
						<picture>
							<img src="/assets-web/images/academy-sports.jpg" alt="">
						</picture>

						<p class="maindesc pt-20">
							ISD Academy offers personalized sports programs across football, tennis, padel, rugby and athletics.Players will enjoy first-rate comprehensive sports training delivered by international coaches with the highest certifications, ensuring the superior standard of coaching that has created the world’s finest players hailing from the best  clubs from US, Europe and across the world.
						</p>

						<a href="#" class="fc-primary td-underline fw-bold fs-medium">Read more</a>

					</article>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="box --academy-programs-box">

					<article class="inner-content">
						<picture>
							<img src="/assets-web/images/academy-education.jpg" alt="">
						</picture>

						<p class="maindesc pt-20">
							Students will enjoy a professional program and will gain valuable life experiences both on and off the pitch. With customized programs delivered by exceptional educators, students can choose either a British or American curriculum and complement it with language lessons, providing them with the best pathway to higher education or professional careers.
						</p>
						<a href="#" class="fc-primary td-underline fw-bold fs-medium">Read more</a>
												
					</article>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="box --academy-programs-box">
					
					<article class="inner-content">
						<picture>
							<img src="/assets-web/images/academy-living.jpg" alt="">
						</picture>

						<p class="maindesc pt-20">
							Student-athletes will stay in Dubai, one of the world’s safest, most progressive and cosmopolitan cities where they will enjoy year-round sunshine, and plenty of cultural and tourism experiences, with modern accommodations, in fully furnished and serviced apartments located right on the campus of ISD, Dubai Sports City.
						</p>
						<a href="#" class="fc-primary td-underline fw-bold fs-medium">Read more</a>
												
					</article>
				</div>
			</div>
		</div>
	</div>
</section>



<!-- Join an Academy -->
<section class="sec-padding pt-0 join-academy-section">
	<div class="container-wrapper">

		<div class="academy-section scrollme">
			<a href="#" class="box --academy-box animateme" data-when="enter" data-from="0.2" data-to="0" data-opacity="0" data-translatey="400">
				<picture>
					<img src="/assets-web/images/icons/football.svg" alt="Football" class="m-auto">
				</picture>

				<p class="text">football</p>
			</a>

			<a href="#" class="box --academy-box animateme" data-when="enter" data-from="0.4" data-to="0" data-opacity="0" data-translatey="400">
				<picture>
					<img src="/assets-web/images/icons/tennis.svg" alt="Tennis" class="m-auto">
				</picture>

				<p class="text">tennis</p>
			</a>

			<a href="#" class="box --academy-box animateme" data-when="enter" data-from="0.6" data-to="0" data-opacity="0" data-translatey="400">
				<picture>
					<img src="/assets-web/images/icons/rugby.svg" alt="Rugby" class="m-auto">
				</picture>

				<p class="text">rugby</p>
			</a>

			<a href="#" class="box --academy-box animateme" data-when="enter" data-from="0.6" data-to="0" data-opacity="0" data-translatey="400">
				<picture>
					<img src="/assets-web/images/icons/athletics.svg" alt="Athletics" class="m-auto">
				</picture>

				<p class="text">athletics</p>
			</a>

			<a href="#" class="box --academy-box animateme" data-when="enter" data-from="0.9" data-to="0" data-opacity="0" data-translatey="400">
				<picture>
					<img src="/assets-web/images/icons/padel.svg" alt="Padel" class="m-auto">
				</picture>

				<p class="text">padel</p>
			</a>
		</div>

	</div>
</section>

<!-- Divider -->
<hr class="divider" />

<section class="sec-padding">
	<div class="container-wrapper">
		<h2 class="text-center fc-primary tt-uppercase">Request a consultation or more information </h2>

		<section class="mt-20">
			<form action="{{route('inquiryform.submission')}}" class="default-form" method="POST">
			    @csrf
				<div class="row">
					<div class="col-md-2">
						<div class="control-group">
							<div class="radiobox">
								<input type="radio" id="football" value="1" name="sport" required>
								<span class="label" for="football"></span>
								<span class="text">Football</span>
							</div>
						</div>
					</div>

					<div class="col-md-2">
						<div class="control-group">
							<div class="radiobox">
								<input type="radio" id="tennis" value="4" name="sport" required>
								<span class="label" for="tennis"></span>
								<span class="text">Tennis</span>
							</div>
						</div>
					</div>

					<div class="col-md-2">
						<div class="control-group">
							<div class="radiobox">
								<input type="radio" id="rugby" value="2" name="sport" required>
								<span class="label" for="rugby"></span>
								<span class="text">Rugby</span>
							</div>
						</div>
					</div>

					<div class="col-md-2">
						<div class="control-group">
							<div class="radiobox">
								<input type="radio" id="athletics" value="3" name="sport" required> 
								<span class="label" for="athletics"></span>
								<span class="text">Athletics</span>
							</div>
						</div>
					</div>

					<div class="col-md-2">
						<div class="control-group">
							<div class="radiobox">
								<input type="radio" id="padel" value="5" name="sport" required>
								<span class="label" for="padel"></span>
								<span class="text">Padel</span>
							</div>
						</div>
					</div>

				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="control-group">
							<input type="text" class="form-field" placeholder="Enter your Name" name="name" required>
						</div>
					</div>

					<div class="col-md-6">
						<div class="control-group">
							<input type="text" class="form-field" placeholder="Enter your Email" name="email" required>
						</div>
					</div>

					<div class="col-md-6">
						<div class="control-group">
							<input type="text" class="form-field" placeholder="Enter Phone Number" name="number" required>
						</div>
					</div>

					<div class="col-md-6">
						<div class="control-group">
							<input type="date" class="form-field" placeholder="Select Date" name="date" required>
						</div>
					</div>

					<div class="col-md-12">
						<div class="control-group">
							<textarea class="form-field" placeholder="Message" name="message" required></textarea>
						</div>
					</div>

					<div class="col-md-12">
						<div class="control-group">
							<input type="submit" value="Submit" class="btn --btn-primary">
						</div>
					</div>

				</div>
			</form>
		</section>
	</div>
</section>

<!-- Book a Pitch or Court -->
<section class="sec-padding">
	<div class="container-wrapper">

		<picture>
			<a href="https://www.google.com/maps/place/Inspiratus+Sports+District+(ISD)/@25.0385842,55.2238939,17z/data=!3m1!4b1!4m5!3m4!1s0x3e5f6e0aaa4c89e1:0x619b4232a4f2f937!8m2!3d25.0385794!4d55.2260826" target="_blank">
				<img src="/assets-web/images/academy-map.jpg" alt="">
			</a>
		</picture>

	</div>
</section>

@endsection