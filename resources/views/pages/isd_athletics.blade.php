@extends('layouts.app')
@section('pageClass', 'isd-athleticspg inner-page')
@section('title', 'Athletics')
@section('content')

<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/athletics-landing.jpg');">

	<div class="inner-content --running">
		<div class="vertical-heading">
			<h1 class="title">ISD Athletics</h1>
		</div>
	</div>

	<div class="content-section">
		<article class="mobile-content">
			<h2 class="maintitle mb-10">Once-in-a-lifetime traning experience with International & Team GB Olympic Athletes</h2>
			<p class="fs-large mb-30">
				The best track & field venue located in the heart of the city, the Athletics Center has a 9-lane Olympic standard track, long jump and high jump facilities as well capacity for all throwing disciplines. 
			</p>

			<h2 class="maintitle mb-10">WHAT’S HAPPENING?</h2>
			<p class="fs-large mb-30">ISD Athletics is launching at Dubai Sports City with a FREE once-in-a-lifetime experience held by internationally renowned athletes including top Team GB Olympic team athletes, lead by three-time Olympian Andy Turner.</p>

			<h2 class="maintitle mb-10">HOW DO I PARTICIPATE?</h2>
			<p class="fs-large mb-30">Join us this Tuesday, Wednesday and Thursday at Dubai Sports City for a FREE training session with our new team of Olympic and International athletes. <br><br>

			Meet and be trained by our new head coach and 3-time Team GB Olympic athlete, World Championship Bronze medalist and 200m hurdle record breaker Andy Turner, along with international athletes Antonio Infantino and Alex Al-Ameen who will guide youth athletes through different track and field disciplines.</p>

			<a href="/academyRegistration" class="btn bg-running animsition-link fw-bold">Click Here to Register</a>

		</article>
	</div>

</section>

@endsection