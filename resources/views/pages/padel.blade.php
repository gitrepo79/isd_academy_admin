@extends('layouts.app')
@section('pageClass', 'padelpg inner-page')
@section('title', 'Padel Academy in Dubai')
@section('description', 'Padel Academy in Dubai')
@section('keywords', 'Paddle Tennis, Padel Tennis, Padel Tennis Dubai, Padel dubai, Padel in dubai')
@section('content')

<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/padel-banner.jpg');">

	<div class="inner-content --padel">
	</div>

	<div class="vertical-heading">
		<h1 class="title">Padel</h1>
	</div>

	<div class="content-section">
		<p class="mobile-content">
			The best track & field venue located in the heart of the city, the Athletics Center has a 9-lane Olympic standard track, long jump and high jump facilities as well capacity for all throwing disciplines. 
		</p>
		<div class="container-wrapper">
			<div class="row">

				<div class="col-xl-4">
					<div class="box --round-box">
						<article>
							<h2 class="title">academy</h2>

							<p class="desc"> Delivered by international competitive athletes, the UAE’s most comprehensive program for athletic development, Ultimate Athletics offers all disciplines and caters to athletes of all levels, from beginners to those pursuing track & field professionally.</p>

							<a href="/academyRegistration" class="book-btn animsition-link">Register</a>
						</article>
					</div>
				</div>

				<div class="col-xl-4">
					<div class="box --round-box">
						<article>
							<h2 class="title">MEMBERSHIP</h2>

							<p class="desc">Stay active with a membership at the Athletics Center to use the track and outdoor gym.  Packages are monthly, 3 months and annual.</p>

							<a href="/venuHire" class="book-btn animsition-link">Book</a>
						</article>
					</div>
				</div>

				<div class="col-xl-4">
					<div class="box --round-box">
						<article>
							<h2 class="title"> leagues <small class="fs-large">&amp; tournaments</small></h2>

							<p class="desc">The Athletics Center Dubai boasts the best track & field facilities in Dubai and is strategically located in the heart of the city, accessible to all communities.  Call us today for venue bookings to host your events.</p>

							<a href="/venuHire" class="book-btn animsition-link">Enquire</a>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

@endsection