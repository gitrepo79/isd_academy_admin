@extends('layouts.app')
@section('pageClass', 'rugbypg inner-page')
@section('title', 'Rugby Academy in Dubai')
@section('description', 'Rugby Academy in Dubai')
@section('keywords', 'rugby coaching dubai, rugby classes in dubai, play rugby in dubai, rugby dubai, rugby in dubai')
@section('content')

<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/rugby-inner-banner.webp');">

	<div class="inner-content --rugby">
		<div class="vertical-heading">
			<h1 class="title">Rugby</h1>
		</div>
	</div>

	

	<div class="content-section">
		<article>
			<p class="mobile-content">
				If Rugby is your sport, get into the game at our state-of-the-art facilities at the Rugby Center.  Home to the Dubai Knight Eagles Rugby Club for competitive teams and offering High Performance Skills training for individuals, we cater to all your rugby needs, with programs for youth and adults. 
			</p>
			<div class="inner-page-content">

				<div class="box --round-box">
					<article>
						<h2 class="title">academy</h2>

						<p class="desc">From January 2021 enhance your skills with our high performance skills development program. </p>

						<a href="#" class="book-btn animsition-link">052 519 3472</a>
					</article>
				</div>

				<div class="box --round-box">
					<article>
						<h2 class="title"> leagues <small class="fs-large">&amp; tournaments</small></h2>

						<p class="desc">With top-of-the line pitches and a club house within easy access of all Dubai communities, the Rugby Center can host your next tournament or league.</p>

						<a href="#" class="book-btn animsition-link">052 519 3472</a>
					</article>
				</div>

			</div>
		</article>
	</div>

</section>

@endsection