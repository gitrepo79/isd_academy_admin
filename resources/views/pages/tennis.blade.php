@extends('layouts.app')
@section('pageClass', 'tennispg innerpage')
@section('title', 'ISD Tennis Academy | Book private or group lessons in Dubai Sports City')
@section('description', 'ISD Tennis Academy in Dubai Sports City offers a personalized approach to tennis for boys and girls aged 4-18 delivered by accredited European pro-player coaches delivering a distinctive modern tennis experience with expert methodology.')
@section('keywords', 'dubai tennis, tennis dubai, tennis in dubai, tennis academy dubai, tennis lessons dubai, tennis classes dubai, tennis club dubai, tennis coaching in dubai')
@section('content')

<section class="hero-banner --video-banner">

	<video playsinline autoplay muted loop poster="https://img.youtube.com/vi/QT3xTIRMkV8/maxresdefault.jpg" class="video">
		<!-- <source src="polina.webm" type="video/webm"> -->
		<source src="/assets-web/videos/ISD_Tennis_Academy.mp4" type="video/mp4">
	</video>

</section>

<!-- About Tennis -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<!-- <div class="col-lg-2 order-lg-last">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdtennis.svg" alt="">
				</picture>
			</div> -->

			<div class="col-lg-10">
				<h2 class="maintitle">
					<span class="fc-football">ISD TENNIS ACADEMY</span>
				</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				ISD Tennis Academy at Dubai Sports City is back for a new season! With 3 of the top European certified coaches in Dubai, this year’s season promises to be the best yet with personalised programs for boys and girls ages 4 to 18, of all abilities. <br><br>
				With hands-on coaching, the Academy offers beginners a fun and uplifting entry into the game, whilst providing the more experienced players a chance to build on their strengths, and focus on their development areas, with personalised programs. <br><br>
				Players enjoy a fun, challenging and world-class experience on our brand new outdoor, hard-court, floodlit tennis courts at ISD’s premier facilities. <br><br> 
				ISD Tennis also offers a pathway to professional tennis careers and international scholarship opportunities at US universities.
			</p>
		</div>

		<div class="row">
			<div class="col-lg-4">
				<div class="box --programs-box">
					
					<article class="inner-content">
						<h2 class="title mb-0">
							<span class="fc-primary">TERM 1</span>
						</h2>
						<p class="subtitle">
							<span class="fc-football">SEPT 05 - DEC 09</span>
						</p>
												
					</article>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="box --programs-box">

					<article class="inner-content">
						<h2 class="title mb-0">
							<span class="fc-primary">TERM 2</span>
						</h2>
						<p class="subtitle">
							<span class="fc-football">JAN 02 - MAR 24</span>
						</p>
												
					</article>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="box --programs-box">
					
					<article class="inner-content">
						<h2 class="title mb-0">
							<span class="fc-primary">TERM 3</span>
						</h2>
						<p class="subtitle">
							<span class="fc-football">APR 11 - JUN 30</span>
						</p>
												
					</article>
				</div>
			</div>
		</div>

		<div class="text-center mb-20">
			<h2 class="fc-primary">CALL US NOW FOR A FREE TRIAL: <span class="fc-football">058 260 8408</span></h2>
		</div>

		<div class="text-center mt-0">
			<a href="#" class="btn --btn-primary" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<hr class="divider" />

<!-- Schedule -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">The Schedule</span>
				</h2>
			</div>
		</div>

		<div class="Rtable --schedule-table --collapse --7cols text-center mt-0">
	        <div class="Rtable-cell --head">
	            Age Category
	        </div>

	        <div class="Rtable-cell --head">
	            Sunday
	        </div>

	        <div class="Rtable-cell --head">
	            Monday
	        </div>

	        <div class="Rtable-cell --head">
	            Tuesday
	        </div>

	        <div class="Rtable-cell --head">
	            Wednesday
	        </div>

	        <div class="Rtable-cell --head">
	            Thursday
	        </div>

	        <div class="Rtable-cell --head">
	            Saturday
	        </div>


	        <!-- 4-7 Category -->
	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Age Category
	                </div>

	                <div class="content">
	                    4-7 years old

	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Sunday
	                </div>

	                <div class="content">
	                    3:30pm - 4:30pm <br>4:30pm - 5:30pm

	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Monday
	                </div>

	                <div class="content">
	                    4:00pm - 5:00pm
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Tuesday
	                </div>

	                <div class="content">
	                    3:30pm - 4:30pm <br>4:30pm - 5:30pm

	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Wednesday
	                </div>

	                <div class="content">
	                   4:00pm - 5:00pm

	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Thursday
	                </div>

	                <div class="content">
	                    4:00pm - 5:00pm

	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Saturday
	                </div>

	                <div class="content">
	                    3:30pm - 4:30pm <br>4:30pm - 5:30pm

	                </div>
	            </div>
	        </div>

	        <!-- 8-10 Category -->
	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Age Category
	                </div>

	                <div class="content">
	                    8-10 years old


	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Sunday
	                </div>

	                <div class="content">
	                    5:30pm - 6:30pm


	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Monday
	                </div>

	                <div class="content">
	                    5:00pm - 6:00pm

	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Tuesday
	                </div>

	                <div class="content">
	                    5:30pm - 6:30pm


	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Wednesday
	                </div>

	                <div class="content">
	                   5:00pm - 6:00pm


	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Thursday
	                </div>

	                <div class="content">
	                    5:00pm - 6:00pm


	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Saturday
	                </div>

	                <div class="content">
	                    5:30pm - 6:30pm

	                </div>
	            </div>
	        </div>

	        <!-- 10-12 Category -->
	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Age Category
	                </div>

	                <div class="content">
	                    10-12 years old


	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Sunday
	                </div>

	                <div class="content">
	                    6:30pm - 7:30pm


	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Monday
	                </div>

	                <div class="content">
	                    6:00pm - 7:00pm

	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Tuesday
	                </div>

	                <div class="content">
	                    6:30pm - 7:30pm

	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Wednesday
	                </div>

	                <div class="content">
	                   6:00pm - 7:00pm


	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Thursday
	                </div>

	                <div class="content">
	                    6:00pm - 7:00pm

	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Saturday
	                </div>

	                <div class="content">
	                    6:30pm - 7:30pm

	                </div>
	            </div>
	        </div>

	        <!-- 13-15 Category -->
	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Age Category
	                </div>

	                <div class="content">
	                    13-15 years old
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Sunday
	                </div>

	                <div class="content">
	                    7:30pm - 8:30pm

	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Monday
	                </div>

	                <div class="content">
	                    -

	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Tuesday
	                </div>

	                <div class="content">
	                    -

	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Wednesday
	                </div>

	                <div class="content">
	                   -
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Thursday
	                </div>

	                <div class="content">
	                    7:00pm - 8:00pm
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Saturday
	                </div>

	                <div class="content">
	                    7:30pm - 8:30pm
	                </div>
	            </div>
	        </div>

	        <!-- 16-18 Category -->
	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Age Category
	                </div>

	                <div class="content">
	                    16-18 years old
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Sunday
	                </div>

	                <div class="content">
	                    7:30pm - 8:30pm
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Monday
	                </div>

	                <div class="content">
	                    -
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Tuesday
	                </div>

	                <div class="content">
	                    -
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Wednesday
	                </div>

	                <div class="content">
	                   -
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Thursday
	                </div>

	                <div class="content">
	                    7:00pm - 8:00pm
	                </div>
	            </div>
	        </div>

	        <div class="Rtable-cell">
	            <div class="body">
	                <div class="header">
	                    Saturday
	                </div>

	                <div class="content">
	                    7:30pm - 8:30pm
	                </div>
	            </div>
	        </div>


	    </div>

	</div>
</section>

<hr class="divider" />

<!-- Tennis Coaches -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">Our Coaches</span>
				</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Our certified, international professional coaches offer highly personalized programs for adults and children  of all ages and abilities. We invite you to experience with them ISD Tennis’ curated training philosophy and methodology to boost your skills in modern and creative tennis in a fun, challenging environment!
			</p>
		</div>
		
		<div class="row">
			<div class="col-lg-4">
				<picture>
					<img src="/assets-web/images/gallery/tennis/coach-ciara.jpg" alt="">
				</picture>
			</div>

			<div class="col-lg-8">

				<table class="table --schedule-table coaches">
					<tr>
						<th class="text-left" width="220px">
							Coach
						</th>
						<td>
							Ciara Haggarty
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Qualifications
						</th>
						<td>
							LTA level 1, 2, and 3
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Professional Background
						</th>
						<td>
							Professional coach for 15 years in England, America, Australia, and Dubai
						</td>
					</tr>

				</table>
			</div>
		</div>

		<div class="row mt-60">
			<div class="col-lg-4">
				<picture>
					<img src="/assets-web/images/gallery/tennis/coach-alana.jpg" alt="">
				</picture>
			</div>

			<div class="col-lg-8">
				<table class="table --schedule-table coaches">
					<tr>
						<th class="text-left" width="220px">
							Coach
						</th>
						<td>
							Alana Casey
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Qualifications
						</th>
						<td>
							LTA level 1, 2, and 3
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Professional Background
						</th>
						<td>
							Professional coach for 11 years in the UK, Australia and Dubai. Sport & Exercise Sciences Degree: Sports psychology, nutrition, exercise, health & lifestyle, anatomy & physiology, biomechanics, and sports coaching.
						</td>
					</tr>

				</table>


			</div>
		</div>

		<div class="row mt-60">
			<div class="col-lg-4">
				<picture>
					<img src="/assets-web/images/gallery/tennis/coach-lucy.jpg" alt="">
				</picture>
			</div>

			<div class="col-lg-8">
				<table class="table --schedule-table coaches">
					<tr>
						<th class="text-left" width="220px">
							Coach
						</th>
						<td>
							Lucy Tameru
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Qualifications
						</th>
						<td>
							PTR Accredited <br>
							ITF Accredited <br>
							Trainer in C performance type
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Professional Background
						</th>
						<td>
							Professional coach for 10 years in Germany, Ethiopia and Dubai; Represented Ethiopia in international competitions.
						</td>
					</tr>

				</table>


			</div>
		</div>

	</div>
</section>

<hr class="divider" />

<!-- Tennis Pricing -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">Pricing</span>
				</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Each ISD Tennis season is made up of 3 terms: Term 1 - (14 weeks), Term 2 – (12 weeks), and Term 3 - (12 weeks). All terms are charged pro rata and can be booked as per term or for the full season with each group capped at a maximum number of 4 or 6 people.
			</p>
		</div>
		
		<h2 class="maintitle">Development</h2>

		<table class="table --schedule-table athletics text-left">
			<tr>
				<th>
					Age
				</th>
				<th>
					1x per week
				</th>
				<th>
					2x per week
				</th>
				<th>
					3x per week
				</th>
			</tr>

			<tr>
				<td>
					4-7 years old
				</td>
				<td>
					110
				</td>
				<td>
					100
				</td>
				<td>
					90
				</td>
			</tr>

			<tr>
				<td>
					8-10 years old
				</td>
				<td>
					120
				</td>
				<td>
					120
				</td>
				<td>
					90
				</td>
			</tr>

			<tr>
				<td>
					10-12 years old
				</td>
				<td>
					120
				</td>
				<td>
					110
				</td>
				<td>
					95
				</td>
			</tr>

			<tr>
				<td>
					13-15 years old
				</td>
				<td>
					145
				</td>
				<td>
					135
				</td>
				<td>
					115
				</td>
			</tr>

			<tr>
				<td>
					16-18 years old
				</td>
				<td>
					145
				</td>
				<td>
					135
				</td>
				<td>
					115
				</td>
			</tr>

		</table>


		<h2 class="maintitle">Advanced</h2>

		<table class="table --schedule-table athletics text-left">
			<tr>
				<th>
					Age
				</th>
				<th>
					1x per week
				</th>
				<th>
					2x per week
				</th>
				<th>
					3x per week
				</th>
			</tr>


			<tr>
				<td>
					8-10 years old
				</td>
				<td>
					135
				</td>
				<td>
					125
				</td>
				<td>
					110
				</td>
			</tr>

			<tr>
				<td>
					10-12 years old
				</td>
				<td>
					135
				</td>
				<td>
					125
				</td>
				<td>
					110
				</td>
			</tr>

			<tr>
				<td>
					13-15 years old
				</td>
				<td>
					175
				</td>
				<td>
					155
				</td>
				<td>
					140
				</td>
			</tr>

			<tr>
				<td>
					16-18 years old
				</td>
				<td>
					175
				</td>
				<td>
					155
				</td>
				<td>
					140
				</td>
			</tr>

		</table>

	</div>
</section>

<hr class="divider" />

<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<h2 class="maintitle">gallery</h2>

		<div class="gallery-section">

			<div class="gallery-slider">
				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img1.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img1.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img2.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img2.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img3.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img3.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img4.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img4.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img5.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img5.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img6.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img6.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img7.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img7.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img8.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img8.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img9.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img9.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img10.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img10.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img11.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img11.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img12.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img12.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img13.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img13.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img14.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img14.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img15.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img15.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img16.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img16.jpg" alt="">
					</a>
				</div>
			</div>
		</div>

	</div>
</section>

@endsection