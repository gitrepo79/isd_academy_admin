@extends('layouts.app')
@section('pageClass', 'tennispg innerpage')
@section('title', 'ISD Tennis Academy | Book private or group lessons in Dubai Sports City')
@section('description', 'ISD Tennis Academy in Dubai Sports City offers a personalized approach to tennis for boys and girls aged 4-18 delivered by accredited European pro-player coaches delivering a distinctive modern tennis experience with expert methodology.')
@section('keywords', 'dubai tennis, tennis dubai, tennis in dubai, tennis academy dubai, tennis lessons dubai, tennis classes dubai, tennis club dubai, tennis coaching in dubai')
@section('content')

<section class="hero-banner --video-banner">

	<video playsinline autoplay muted loop poster="https://img.youtube.com/vi/QT3xTIRMkV8/maxresdefault.jpg" class="video">
		<!-- <source src="polina.webm" type="video/webm"> -->
		<source src="/assets-web/videos/ISD_Tennis_Academy.mp4" type="video/mp4">
	</video>

</section>

<!-- About Tennis -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<!-- <div class="col-lg-2 order-lg-last">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdtennis.svg" alt="">
				</picture>
			</div> -->

			<div class="col-lg-10">
				<h2 class="maintitle">
					<span class="fc-football">ISD Tennis</span>
				</h2>
			</div>
		</div>

		<hr class="divider">

		<h4 class="maintitle --small mt-50">
			Private or Group Lessons
		</h4>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				ISD Tennis at Dubai Sports City offers the perfect place for you to build on your passion! Our brand new and state-of-the-art, floodlit, hard courts are within a few minutes drive from Dubai’s biggest communities with easy access from all major roads, making getting here easier than ever! <br><br>
				You can book private lessons for yourself, for a group of friends or for the whole family!
			</p>
		</div>

		<table class="table --schedule-table athletics text-left">
			<tr>
				<th>
					Description
				</th>
				<th>
					1 Lesson
				</th>
				<th>
					5 Lesson
				</th>
				<th>
					10 Lesson
				</th>
			</tr>

			<tr>
				<td>
					Private Lessons 45mins
				</td>
				<td>
					AED 325
				</td>
				<td>
					AED 260
				</td>
				<td>
					AED 250
				</td>
			</tr>

			<tr>
				<td>
					Semi Private Lessons (Price per player) 
				</td>
				<td>
					AED 275
				</td>
				<td>
					AED 220
				</td>
				<td>
					AED 220
				</td>
			</tr>

			<tr>
				<td>
					Group Lessons
				</td>
				<td>
					AED 180
				</td>
				<td>
					AED 170
				</td>
				<td>
					AED 160
				</td>
			</tr>

		</table>

		<div class="text-center mb-20">
			<h2 class="fc-primary">CALL US NOW FOR A FREE TRIAL: <span class="fc-football">058 260 8408</span></h2>
		</div>

		<div class="text-center mt-0">
			<a href="#" class="btn --btn-primary" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<hr class="divider" />

<!-- Coaches -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">Our Coaches</span>
				</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Our certified, international professional coaches offer highly personalized programs for adults and children  of all ages and abilities. We invite you to experience with them ISD Tennis’ curated training philosophy and methodology to boost your skills in modern and creative tennis in a fun, challenging environment!
			</p>
		</div>
		
		<div class="row">
			<div class="col-lg-4">
				<picture>
					<img src="/assets-web/images/gallery/tennis/coach-ciara.jpg" alt="">
				</picture>
			</div>

			<div class="col-lg-8">

				<table class="table --schedule-table coaches">
					<tr>
						<th class="text-left" width="220px">
							Coach
						</th>
						<td>
							Ciara Haggarty
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Qualifications
						</th>
						<td>
							LTA level 1, 2, and 3
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Professional Background
						</th>
						<td>
							Professional coach for 15 years in England, America, Australia, and Dubai
						</td>
					</tr>

				</table>
			</div>
		</div>

		<div class="row mt-60">
			<div class="col-lg-4">
				<picture>
					<img src="/assets-web/images/gallery/tennis/coach-alana.jpg" alt="">
				</picture>
			</div>

			<div class="col-lg-8">
				<table class="table --schedule-table coaches">
					<tr>
						<th class="text-left" width="220px">
							Coach
						</th>
						<td>
							Alana Casey
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Qualifications
						</th>
						<td>
							LTA level 1, 2, and 3
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Professional Background
						</th>
						<td>
							Professional coach for 11 years in the UK, Australia and Dubai. Sport & Exercise Sciences Degree: Sports psychology, nutrition, exercise, health & lifestyle, anatomy & physiology, biomechanics, and sports coaching.
						</td>
					</tr>

				</table>


			</div>
		</div>

		<div class="row mt-60">
			<div class="col-lg-4">
				<picture>
					<img src="/assets-web/images/gallery/tennis/coach-lucy.jpg" alt="">
				</picture>
			</div>

			<div class="col-lg-8">
				<table class="table --schedule-table coaches">
					<tr>
						<th class="text-left" width="220px">
							Coach
						</th>
						<td>
							Lucy Tameru
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Qualifications
						</th>
						<td>
							PTR Accredited <br>
							ITF Accredited <br>
							Trainer in C performance type
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Professional Background
						</th>
						<td>
							Professional coach for 10 years in Germany, Ethiopia and Dubai; Represented Ethiopia in international competitions.
						</td>
					</tr>

				</table>


			</div>
		</div>

	</div>
</section>

<hr class="divider" />

<!-- About Social Tennis -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		
		<h4 class="maintitle --small">
			<span class="fc-football">Adult Social tennis at ISD</span>
		</h4>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Join ISD Social Tennis, meet and play singles or doubles with other tennis players in a fun, rally format, led by our team of highly certified and experienced European tennis coaches. <br><br>
				All you have to do is book a spot and we’ll get you started. It’s the easiest way to meet new members and have some guaranteed tennis fun!  Limited spots available!
			</p>
		</div>

		<table class="table --schedule-table athletics text-left">
			<tr>
				<th>
					Date
				</th>
				<th>
					Time
				</th>
				<th>
					Price (excluding VAT)
				</th>
			</tr>

			<tr>
				<td>
					Thursday evening
				</td>
				<td>
					8:00pm - 10:00pm
				</td>
				<td>
					AED 100/ person
				</td>
			</tr>

			<tr>
				<td>
					Friday morning
				</td>
				<td>
					7:30am - 9:30am
				</td>
				<td>
					AED 80/ person
				</td>
			</tr>

		</table>

		<div class="text-center mt-0">
			<a href="#" class="btn --btn-primary" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<hr class="divider" />

<!-- About Cardio Tennis -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		
		<h4 class="maintitle --small">
			<span class="fc-football">Cardio Tennis</span>
		</h4>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Join ISD Cardio Tennis, our fun group fitness session, led by coaches Ciara, Alana and Lucy. It’s open to adults of all ages and abilities! <br><br>
				Get fit and burn calories with this full-body workout featuring tennis drills, games, and skills to the heart pumping  sound of upbeat music!
			</p>
			<h2 class="fc-football">TIMINGS</h2>
			<p class="maindesc --big">Every Monday from 8:30pm to 9:30pm</p>

			<h2 class="fc-football">PRICES</h2>
			<p class="maindesc --big">AED 110 per session or AED 450 for a package of 5</p>
		</div>

		<div class="text-center mt-0">
			<a href="#" class="btn --btn-primary" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<hr class="divider" />

<!-- About Ladies Morning -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		
		<h4 class="maintitle --small">
			<span class="fc-football">Ladies Morning</span>
		</h4>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Whether you come alone or with with a group of your girlfriends, and want to meet the ISD Tennis community, join us on Sundays, Tuesdays or Thursdays for a ladies only morning tennis session! It’s a great way to hone your skills, stamina and experience, have fun and build your passion for tennis!
			</p>
		</div>

		<div class="text-center mt-0">
			<a href="#" class="btn --btn-primary" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

@endsection