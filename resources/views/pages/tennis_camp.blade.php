@extends('layouts.app')
@section('pageClass', 'tennispg innerpage')
@section('title', 'ISD Tennis Academy | Book private or group lessons in Dubai Sports City')
@section('description', 'ISD Tennis Academy in Dubai Sports City offers a personalized approach to tennis for boys and girls aged 4-18 delivered by accredited European pro-player coaches delivering a distinctive modern tennis experience with expert methodology.')
@section('keywords', 'dubai tennis, tennis dubai, tennis in dubai, tennis academy dubai, tennis lessons dubai, tennis classes dubai, tennis club dubai, tennis coaching in dubai')
@section('content')

<section class="hero-banner --video-banner">

	<video playsinline autoplay muted loop poster="https://img.youtube.com/vi/QT3xTIRMkV8/maxresdefault.jpg" class="video">
		<!-- <source src="polina.webm" type="video/webm"> -->
		<source src="/assets-web/videos/ISD_Tennis_Academy.mp4" type="video/mp4">
	</video>

</section>

<!-- About Athletics -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<!-- <div class="col-lg-2 order-lg-last">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdathletics.svg" alt="">
				</picture>
			</div> -->

			<div class="col-lg-10">
				<h2 class="maintitle">
					<span class="fc-football">ISD TENNIS ACADEMY CAMPS</span>
				</h2>
			</div>
		</div>

		{{-- <h4 class="maintitle --small mt-50">
			the academy
		</h4> --}}

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Join us for Spring, Summer and Winter camps at Dubai Sports City! Our programs will help your children can sharpen their game, strengthen their focus, get active, improve their stamina, and build leadership qualities on and off the court.

				<br><br>
				ISD Tennis Academy’s camps are fun, engaging, and delivered to the highest standards by pro European coaches keeping your children active in a, safe, positive, and athlete-centred environment.

			</p>
		</div>

		{{-- <div class="text-center">
			<a href="/athleticsacademy-fees" class="btn --btn-primary ">click here for pricing</a>

			<a href="/athleticsacademy-schedule" class="btn --btn-primary ">click here for schedule</a>
		</div> --}}

	</div>
</section>

<hr class="divider" />

{{-- Athletics Schedule --}}
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">THE schedule</span>
				</h2>
			</div>
		</div>

		<!-- Schedule Table -->
		<h3>
			Daily
		</h3>
		<div class="table-responsive">
			<table class="table --schedule-table athletics text-left">
				<tr>
					<th>
						Days
					</th>
					<th>
						Detail
					</th>
				</tr>

				<tr>
					<td>
						8:30 am
					</td>
					<td>
						Arrival and warm up, strength & conditioning on the track
					</td>
				</tr>

				<tr>
					<td>
						9:15 am
					</td>
					<td>
						Individual tennis skills & tennis-based games
					</td>
				</tr>

				<tr>
					<td>
						10:15 am
					</td>
					<td>
						Break and Snack
					</td>
				</tr>

				<tr>
					<td>
						10:45 am
					</td>
					<td>
						Match Time - Put your Skills into Practice 
					</td>
				</tr>

				<tr>
					<td>
						11:45 am
					</td>
					<td>
						Cool Down
					</td>
				</tr>

				<tr>
					<td>
						12:00 am
					</td>
					<td>
						Pick Up
					</td>
				</tr>


			</table>
		</div>

		
		<div class="text-center mt-40">
			<a href="#" class="btn --btn-primary" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<hr class="divider" />

<!-- Tennis Coaches -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">Our Coaches</span>
				</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Our certified, international professional coaches offer highly personalized programs for adults and children  of all ages and abilities. We invite you to experience with them ISD Tennis’ curated training philosophy and methodology to boost your skills in modern and creative tennis in a fun, challenging environment!
			</p>
		</div>
		
		<div class="row">
			<div class="col-lg-4">
				<picture>
					<img src="/assets-web/images/gallery/tennis/coach-ciara.jpg" alt="">
				</picture>
			</div>

			<div class="col-lg-8">

				<table class="table --schedule-table coaches">
					<tr>
						<th class="text-left" width="220px">
							Coach
						</th>
						<td>
							Ciara Haggarty
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Qualifications
						</th>
						<td>
							LTA level 1, 2, and 3
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Professional Background
						</th>
						<td>
							Professional coach for 15 years in England, America, Australia, and Dubai
						</td>
					</tr>

				</table>
			</div>
		</div>

		<div class="row mt-60">
			<div class="col-lg-4">
				<picture>
					<img src="/assets-web/images/gallery/tennis/coach-alana.jpg" alt="">
				</picture>
			</div>

			<div class="col-lg-8">
				<table class="table --schedule-table coaches">
					<tr>
						<th class="text-left" width="220px">
							Coach
						</th>
						<td>
							Alana Casey
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Qualifications
						</th>
						<td>
							LTA level 1, 2, and 3
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Professional Background
						</th>
						<td>
							Professional coach for 11 years in the UK, Australia and Dubai. Sport & Exercise Sciences Degree: Sports psychology, nutrition, exercise, health & lifestyle, anatomy & physiology, biomechanics, and sports coaching.
						</td>
					</tr>

				</table>


			</div>
		</div>

		<div class="row mt-60">
			<div class="col-lg-4">
				<picture>
					<img src="/assets-web/images/gallery/tennis/coach-lucy.jpg" alt="">
				</picture>
			</div>

			<div class="col-lg-8">
				<table class="table --schedule-table coaches">
					<tr>
						<th class="text-left" width="220px">
							Coach
						</th>
						<td>
							Lucy Tameru
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Qualifications
						</th>
						<td>
							PTR Accredited <br>
							ITF Accredited <br>
							Trainer in C performance type
						</td>
					</tr>

					<tr>
						<th class="text-left" width="220px">
							Professional Background
						</th>
						<td>
							Professional coach for 10 years in Germany, Ethiopia and Dubai; Represented Ethiopia in international competitions.
						</td>
					</tr>

				</table>


			</div>
		</div>

	</div>
</section>

<hr class="divider" />

{{-- Athletics Fees --}}
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-12">
				<h2 class="maintitle">
					<span class="fc-football">Pricing</span>
				</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				ISD Tennis Academy Camps at Dubai Sports City can be booked in either daily, weekly or bi-weekly sessions! 


			</p>
		</div>

		<table class="table --schedule-table text-left">
			<tr>
				<th>
					Duration
				</th>
				<th>
					Fees
				</th>
			</tr>

			<tr>
				<td>
					Daily
				</td>
				<td>
					AED 150
				</td>
			</tr>

			<tr>
				<td>
					One Week
				</td>
				<td>
					AED 600
				</td>
			</tr>

			<tr>
				<td>
					Two Weeks
				</td>
				<td>
					AED 1,000
				</td>
			</tr>
		</table>
		
		<div class="text-center mt-40">
			<a href="#" class="btn --btn-primary" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<hr class="divider" />
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<h2 class="maintitle">gallery</h2>

		<div class="gallery-section">

			<div class="gallery-slider">
				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img1.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img1.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img2.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img2.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img3.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img3.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img4.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img4.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img5.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img5.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img6.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img6.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img7.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img7.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img8.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img8.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img9.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img9.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img10.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img10.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img11.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img11.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img12.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img12.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img13.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img13.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img14.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img14.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img15.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img15.jpg" alt="">
					</a>
				</div>

				<div class="item">
					<a href="/assets-web/images/gallery/tennis/img16.jpg" data-fancybox="gallery">
						<img src="/assets-web/images/gallery/tennis/img16.jpg" alt="">
					</a>
				</div>
			</div>
		</div>

	</div>
</section>

@endsection