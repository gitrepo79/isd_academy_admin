@extends('layouts.app')
@section('pageClass', 'sportsdetailpg inner-page')
@section('title', 'Tennis Academy in Dubai')
@section('description', 'Tennis Academy in Dubai')
@section('keywords', 'dubai tennis, tennis dubai, tennis in dubai, tennis academy dubai, tennis lessons dubai, tennis classes dubai, tennis club dubai, tennis coaching in dubai')
@section('content')

<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/tennis-banner.webp'); background-position: center;">

	<div class="inner-content --tennis">
		<div class="vertical-heading">
			<h1 class="title">Tennis</h1>
		</div>
	</div>

	<div class="content-section --scroll-page">
		<article>
			<h2 class="title --big">ISD TENNIS ACADEMY'S SPRING PROGRAM</h2>
			<p class="mobile-content">
				Join us this Spring break to Play Tennis! Sharpen your game, strengthen your focus, get active and improve stamina, and build leadership qualities on and off the pitch. ISD Tennis Academy's Spring break Program is delivered to the highest standards by pro European coaches keeping your children active in a fun, safe, positive, and athlete-centered environment.

			</p>

			<div class="bottom-section">
				
				<p class="subtitle">
					Join ISD Tennis Academy and be part of Dubai’s tennis community at ISD, Dubai Sports City!
				</p>

				<a href="/academyRegistration" class="btn bg-tennis fc-white tt-uppercase fw-bold mb-20 mt-20">Register Here</a>

				<p class="introductive-title --tennis">
					Program
				</p>

				<div class="table-section --tennis">

				

					<div class="table-responsive">
						<table class="table --schedule-table tennis">
							<tr>
								<th>Days</th>
							</tr>

							<tr>
								<td>
									Week 1 (28 March - 1 April 2021)
								</td>
							</tr>

							<tr>
								<td>
									Week 2 (4 April - 8 April 2021)
								</td>
							</tr>

						</table>
					</div>

					<div class="table-responsive">
						<table class="table --schedule-table tennis">
							<tr>
								<th>Fees</th>
								<th>1 Week price</th>
								<th>2 week price</th>
							</tr>

							<tr>
								<td>
									150 AED
								</td>
								<td>
									600 AED
								</td>
								<td>
									1020 AED
								</td>
							</tr>

						</table>
					</div>


					<p class="table-title">
						isd tennis Academy schedule
					</p>

					<div class="table-responsive">
						<table class="table --schedule-table tennis">
							<tr>
								<th>time</th>
								<th>detail</th>
							</tr>

							<!-- Time -->
							<tr>
								<td>
									8:30 AM
								</td>
								<td>
									Arrival and warm up, strength & conditioning on the track
								</td>
							</tr>

							<tr>
								<td>
									9:15 AM
								</td>
								<td>
									Individual tennis skills &amp; tennis-based games
								</td>
							</tr>

							<tr>
								<td>
									10:15 AM
								</td>
								<td>
									Break &amp; Snack
								</td>
							</tr>

							<tr>
								<td>
									10:45 AM
								</td>
								<td>
									Match Time - Put your Skills into Practice
								</td>
							</tr>

							<tr>
								<td>
									11:45 AM
								</td>
								<td>
									Cool Down
								</td>
							</tr>

							<tr>
								<td>
									12:00 AM
								</td>
								<td>
									Pick Up
								</td>
							</tr>

						</table>
					</div>


				</div>

			</div>
		
		</article>
	</div>

</section>

@endsection