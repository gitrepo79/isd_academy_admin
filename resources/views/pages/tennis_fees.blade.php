@extends('layouts.app')
@section('pageClass', 'sportsdetailpg innerpage')
@section('title', 'Tennis Academy in Dubai')
@section('description', 'Tennis Academy in Dubai')
@section('keywords', 'dubai tennis, tennis dubai, tennis in dubai, tennis academy dubai, tennis lessons dubai, tennis classes dubai, tennis club dubai, tennis coaching in dubai')
@section('content')

<section class="hero-banner --video-banner">

	<video playsinline autoplay muted loop poster="https://img.youtube.com/vi/QT3xTIRMkV8/maxresdefault.jpg" class="video">
		<!-- <source src="polina.webm" type="video/webm"> -->
		<source src="/assets-web/videos/ISD_Tennis_Academy.mp4" type="video/mp4">
	</video>

</section>

<!-- About Tennis -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-2 order-lg-last d-none d-lg-block">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdtennis.svg" alt="">
				</picture>
			</div>

			<div class="col-lg-10">
				<h2 class="maintitle">ISD Tennis Academy</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				ISD Tennis Academy offers best in-class personalised programs to boys and girls ages 4 to 18, of all abilities. Delivered by a team of highly certified and experienced European coaches, ISD Tennis academy programs are focused on the development of technical skills and leadership qualities, on and off the court. Players enjoy a fun, immersive experience at ISD’s premier facilities with brand new, state-of-the-art outdoor tennis courts. ISD Tennis also offers a pathway to professional tennis careers and international scholarship opportunities at US universities.
			</p>
		</div>

		<!-- Pricing Table -->
		<h2 class="maintitle">Pricing</h2>

		<div class="Rtable --collapse --4cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				details
			</div>

			<div class="Rtable-cell --head">
				1 Lesson
			</div>

			<div class="Rtable-cell --head">
				5 Lessons
			</div>

			<div class="Rtable-cell --head">
				10 Lessons
			</div>

			<!-- Morning Groups -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Details
					</div>

					<div class="content">
						Ladies Morning Groups
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						1 Lesson
					</div>

					<div class="content">
						AED 80
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						5 Lessons
					</div>

					<div class="content">
						AED 360
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						10 Lessons
					</div>

					<div class="content">
						AED 680
					</div>
				</div>
			</div>

			<!-- Private Lesson 45 min -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Details
					</div>

					<div class="content">
						Private Lesson 45 Minutes
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						1 Lesson
					</div>

					<div class="content">
						AED 325
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						5 Lessons
					</div>

					<div class="content">
						AED 1300
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						10 Lessons
					</div>

					<div class="content">
						AED 2500
					</div>
				</div>
			</div>

			<!-- Semi Private Lesson -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Details
					</div>

					<div class="content">
						Semi Private Lesson 60 minutes (price per player)
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						1 Lesson
					</div>

					<div class="content">
						AED 275
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						5 Lessons
					</div>

					<div class="content">
						AED 1100
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						10 Lessons
					</div>

					<div class="content">
						AED 2200
					</div>
				</div>
			</div>

		</div>

		{{-- Term 1 --}}
		<h4>Term 1 (14 Weeks)</h4>

		<div class="Rtable --collapse --4cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				details
			</div>

			<div class="Rtable-cell --head">
				1X Per Week
			</div>

			<div class="Rtable-cell --head">
				2X Per Week
			</div>

			<div class="Rtable-cell --head">
				3X Per Week
			</div>

			<!-- 4 60 minutes -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Details
					</div>

					<div class="content">
						Tennis Group of 4 60mins Contact Time
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						1X Per Week
					</div>

					<div class="content">
						AED 2,030
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						2X Per Week
					</div>

					<div class="content">
						AED 3,500
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						3X Per Week
					</div>

					<div class="content">
						AED 4,830
					</div>
				</div>
			</div>

			<!-- 6 60 minutes -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Details
					</div>

					<div class="content">
						Tennis Group of 6 60mins Contact Time
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						1X Per Week
					</div>

					<div class="content">
						AED 1,680
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						2X Per Week
					</div>

					<div class="content">
						AED 3,080
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						3X Per Week
					</div>

					<div class="content">
						AED 3,990
					</div>
				</div>
			</div>

			<!-- 60 minutes -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Details
					</div>

					<div class="content">
						Tennis Minis 60mins
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						1X Per Week
					</div>

					<div class="content">
						AED 1,540
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						2X Per Week
					</div>

					<div class="content">
						AED 2,800
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						3X Per Week
					</div>

					<div class="content">
						AED 3,780
					</div>
				</div>
			</div>
		</div>

		{{-- Term 2 --}}
		<h4>Term 2 (12 Weeks)</h4>


		<div class="Rtable --collapse --4cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				details
			</div>

			<div class="Rtable-cell --head">
				1X Per Week
			</div>

			<div class="Rtable-cell --head">
				2X Per Week
			</div>

			<div class="Rtable-cell --head">
				3X Per Week
			</div>

			<!-- 4 60 minutes -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Details
					</div>

					<div class="content">
						Tennis Group of 4 60mins Contact Time
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						1X Per Week
					</div>

					<div class="content">
						AED 1,740
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						2X Per Week
					</div>

					<div class="content">
						AED 3,000
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						3X Per Week
					</div>

					<div class="content">
						AED 4,175
					</div>
				</div>
			</div>

			<!-- 6 60 minutes -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Details
					</div>

					<div class="content">
						Tennis Group of 6 60mins Contact Time
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						1X Per Week
					</div>

					<div class="content">
						AED 1,440
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						2X Per Week
					</div>

					<div class="content">
						AED 2,640
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						3X Per Week
					</div>

					<div class="content">
						AED 3,455
					</div>
				</div>
			</div>

			<!-- 60 minutes -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Details
					</div>

					<div class="content">
						Tennis Minis 60mins
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						1X Per Week
					</div>

					<div class="content">
						AED 1,320
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						2X Per Week
					</div>

					<div class="content">
						AED 2,400
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						3X Per Week
					</div>

					<div class="content">
						AED 3,365
					</div>
				</div>
			</div>
		</div>

		{{-- Term 3 --}}
		<h4>Term 3 (10 Weeks)</h4>


		<div class="Rtable --collapse --4cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				details
			</div>

			<div class="Rtable-cell --head">
				1X Per Week
			</div>

			<div class="Rtable-cell --head">
				2X Per Week
			</div>

			<div class="Rtable-cell --head">
				3X Per Week
			</div>

			<!-- 4 60 minutes -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Details
					</div>

					<div class="content">
						Tennis Group of 4 60mins Contact Time
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						1X Per Week
					</div>

					<div class="content">
						AED 1,450
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						2X Per Week
					</div>

					<div class="content">
						AED 2,500
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						3X Per Week
					</div>

					<div class="content">
						AED 3,450
					</div>
				</div>
			</div>

			<!-- 6 60 minutes -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Details
					</div>

					<div class="content">
						Tennis Group of 6 60mins Contact Time
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						1X Per Week
					</div>

					<div class="content">
						AED 1,200
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						2X Per Week
					</div>

					<div class="content">
						AED 2,200
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						3X Per Week
					</div>

					<div class="content">
						AED 2,850
					</div>
				</div>
			</div>

			<!-- 60 minutes -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Details
					</div>

					<div class="content">
						Tennis Minis 60mins
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						1X Per Week
					</div>

					<div class="content">
						AED 1,100
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						2X Per Week
					</div>

					<div class="content">
						AED 2,000
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						3X Per Week
					</div>

					<div class="content">
						AED 2,700
					</div>
				</div>
			</div>
		</div>

		<p class="fs-large">Exclusive of 5% VAT</p>

		
		<div class="text-center mt-40">
			<a href="#" class="btn --btn-primary tt-capital" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<hr class="divider" />

<!-- Tennis Camp -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-2 order-lg-last d-none d-lg-block">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdtennis.svg" alt="">
				</picture>
			</div>

			<div class="col-lg-10">
				<h2 class="maintitle">ISD Tennis Academy Camps</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Join us during Spring, Summer and Winter breaks to Play Tennis! Sharpen your game, strengthen your focus, get active and improve stamina, and build leadership qualities on and off the pitch. ISD Tennis Academy’s camps are delivered to the highest standards by pro European coaches keeping your children active in a fun, safe, positive, and athlete-centered environment.
			</p>
		</div>

		<!-- Pricing Table -->
		<h2 class="maintitle">Pricing</h2>

		<div class="table-responsive">
			<table class="table --schedule-table text-left">
				<tr>
					<th>
						Duration
					</th>
					<th>
						Fees
					</th>
				</tr>

				<tr>
					<td>
						Daily
					</td>
					<td>
						AED 150
					</td>
				</tr>

				<tr>
					<td>
						One Week
					</td>
					<td>
						AED 600
					</td>
				</tr>

				<tr>
					<td>
						Two Weeks
					</td>
					<td>
						AED 1,020
					</td>
				</tr>
			</table>
		</div>

		
		<div class="text-center mt-40">
			<a href="#" class="btn --btn-primary tt-capital" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

@endsection