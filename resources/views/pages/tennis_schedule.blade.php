@extends('layouts.app')
@section('pageClass', 'sportsdetailpg innerpage')
@section('title', 'Tennis Academy in Dubai')
@section('description', 'Tennis Academy in Dubai')
@section('keywords', 'dubai tennis, tennis dubai, tennis in dubai, tennis academy dubai, tennis lessons dubai, tennis classes dubai, tennis club dubai, tennis coaching in dubai')
@section('content')

<section class="hero-banner --video-banner">

	<video playsinline autoplay muted loop poster="https://img.youtube.com/vi/QT3xTIRMkV8/maxresdefault.jpg" class="video">
		<!-- <source src="polina.webm" type="video/webm"> -->
		<source src="/assets-web/videos/ISD_Tennis_Academy.mp4" type="video/mp4">
	</video>

</section>

<!-- About Tennis -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-2 order-lg-last d-none d-lg-block">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdtennis.svg" alt="">
				</picture>
			</div>

			<div class="col-lg-10">
				<h2 class="maintitle">ISD Tennis Academy</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				ISD Tennis Academy offers best in-class personalised programs to boys and girls ages 4 to 18, of all abilities. Delivered by a team of highly certified and experienced European coaches, ISD Tennis academy programs are focused on the development of technical skills and leadership qualities, on and off the court. Players enjoy a fun, immersive experience at ISD’s premier facilities with brand new, state-of-the-art outdoor tennis courts. ISD Tennis also offers a pathway to professional tennis careers and international scholarship opportunities at US universities.
			</p>
		</div>

		
		<div class="text-center mt-40">
			<a href="#" class="btn --btn-primary tt-capital" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

<!-- Tennis Camp -->
<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">
			<div class="col-lg-2 order-lg-last d-none d-lg-block">
				<picture class="logo-icon">
					<img src="/assets-web/images/logos/isdtennis.svg" alt="">
				</picture>
			</div>

			<div class="col-lg-10">
				<h2 class="maintitle">ISD Tennis Academy Camps</h2>
			</div>
		</div>

		<div class="content-section mb-40">
			<p class="maindesc --big">
				Join us during Spring, Summer and Winter breaks to Play Tennis! Sharpen your game, strengthen your focus, get active and improve stamina, and build leadership qualities on and off the pitch. ISD Tennis Academy’s camps are delivered to the highest standards by pro European coaches keeping your children active in a fun, safe, positive, and athlete-centered environment.
			</p>
		</div>

		<!-- Schedule Table -->
		<h2 class="maintitle">Schedule</h2>
		<p class="maindesc --big">
			<strong>
				Daily
			</strong>
		</p>
		<div class="table-responsive">
			<table class="table --schedule-table athletics text-left">
				<tr>
					<th>
						Time
					</th>
					<th>
						Details
					</th>
				</tr>

				<tr>
					<td>
						8:30 am
					</td>
					<td>
						Arrival and warm up, strength & conditioning on the track
					</td>
				</tr>

				<tr>
					<td>
						9:15 am
					</td>
					<td>
						Individual tennis skills & tennis-based games
					</td>
				</tr>

				<tr>
					<td>
						10:15 am
					</td>
					<td>
						Break & Snack
					</td>
				</tr>

				<tr>
					<td>
						10:45 am
					</td>
					<td>
						Match Time - Put your Skills into Practice
					</td>
				</tr>

				<tr>
					<td>
						11:45 am
					</td>
					<td>
						Cool Down
					</td>
				</tr>

				<tr>
					<td>
						12:00 pm
					</td>
					<td>
						Pick Up
					</td>
				</tr>
			</table>
		</div>

		
		<div class="text-center mt-40">
			<a href="#" class="btn --btn-primary tt-capital" data-targetmodal="popup-form">Register Now</a>
		</div>

	</div>
</section>

@endsection