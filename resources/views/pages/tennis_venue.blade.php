@extends('layouts.app')
@section('pageClass', 'sportsdetailpg innerpage')
@section('title', 'ISD Tennis | Home to the best tennis courts in Dubai')
@section('description', 'Play your best tennis on our new, state-of-the-art and outdoor floodlit hard-court tennis courts located in Dubai Sports City. Fast and easy booking 7-days-a-week, convenient location, free parking and full amenities at your fingertips.')
@section('keywords', '')
@section('content')

<section class="hero-banner --inner-banner" style="background-image: url('/assets-web/images/banners/tennis-venue.webp');">


</section>

<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<div class="row align-items-center">

			<div class="col-lg-10">
				<h2 class="maintitle">
					<span class="fc-football">ISD TENNIS COURT HIRE</span>
				</h2>
			</div>
		</div>

		<hr class="divider">

		<div class="content-section mb-40">
			<p class="maindesc --big">
				With four brand new, state-of-the-art and flood lit outdoor tennis courts to enjoy in world-class surroundings,  full changing room facilities and easy parking, ISD is your playing venue of choice in the UAE. Ideal for recreational or professional play and tournaments, ISD’s tennis courts are available for daytime and nighttime booking, 7 days a week! <br><br> Book your court today.
			</p>
		</div>

		<div class="text-center">
			<a href="#" class="btn --btn-primary ">CALL ON 058 260 8408 TO BOOK</a>
		</div>

	</div>
</section>


<hr class="divider" />

<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">
		<h2 class="maintitle">Pricing</h2>


		{{-- Outdoor Rate --}}

		<h6><span class="fc-football">Peak Times &amp; Prices:</span> Monday - Friday 7:30 PM - 10:30 PM & Saturday - Sunday 4:30 PM - 10:30 PM</h6>

		<div class="Rtable --collapse --3cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				Court Detail
			</div>

			<div class="Rtable-cell --head">
				Singles
			</div>

			<div class="Rtable-cell --head">
				Doubles
			</div>

			<!-- Court 1 -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Outdoor Court
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Singles
					</div>

					<div class="content">
						AED 160
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Doubles
					</div>

					<div class="content">
						AED 300
					</div>
				</div>
			</div>
		</div>

		<h6 class="mt-40"><span class="fc-football">Off Peak Times &amp; Prices:</span> Friday 7:30 AM - 3:30 PM</h6>

		<div class="Rtable --collapse --3cols --schedule-table athletics mt-0">
			<div class="Rtable-cell --head">
				Court Detail
			</div>

			<div class="Rtable-cell --head">
				Singles
			</div>

			<div class="Rtable-cell --head">
				Doubles
			</div>

			<!-- Court 1 -->
			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Detail
					</div>

					<div class="content">
						Outdoor Court
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Singles
					</div>

					<div class="content">
						AED 100
					</div>
				</div>
			</div>

			<div class="Rtable-cell">
				<div class="body">
					<div class="header">
						Doubles
					</div>

					<div class="content">
						AED 260
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

@endsection