@extends('layouts.thankyou')
@section('pageClass', 'registrationpg innerpage')
@section('title', 'Thank You')
@section('content')

<section class="sec-padding">
    <div class="container-wrapper">
        <h2 class="maintitle">Thank You</h2>

        <div class="content-section">
            <p class="maindesc --big">
                Thank you for reaching out. A member of our team will contact you shortly.
            </p>

        </div>
    </div>
</section>

{{--<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/default-banner.jpg');">
    <div class="inner-content --registration">
    </div>
    <div class="vertical-heading">
        <h2 class="title"></h2>
    </div>
    <div class="content-section">
        <article class="container-wrapper">
            <div class="clearfix">
                <section class="col-xl-5 offset-xl-4 col-lg-6 offset-lg-3">
                    @if($formType==TYPE_ACADEMY)
                    <h2 style="color: #ffffff">Academy enquiry:</h2><br>
                    <p style="color: #ffffff">Thank you for reaching out. A member of our team team will contact you shortly.
                        Feel free to call us on 055 509 9489 to follow up or for support.
                    Please let me know when you have had chance to implement</p>
                    @elseif($formType==TYPE_VENUE)
                    <h2 style="color: #ffffff">Venue hire enquiry:</h2><br>
                    <p style="color: #ffffff">Thank you for reaching out. A member of our team team will contact you shortly.
                        Feel free to call us on 04 448 1555 to follow up or for support.
                    Please let me know when you have had chance to implement.</p>
                    @elseif($formType==TYPE_CONTACT)
                    <h2 style="color: #ffffff">Reach Out Enquiry:</h2><br>
                    <p style="color: #ffffff">Thank you for reaching out. A member of our team team will contact you shortly.
                    </p>
                    @elseif($formType==TYPE_ATHLETES_POPUP_FORM)
                    <h2 class="title">Thank You</h2><br>
                    <p class="desc">Thank you for your interest. The ISD Athletics team will contact you shortly.
                    </p>
                    @elseif($formType==TYPE_ATHLETES_MEET_FORM)
                    <h2 style="color: #ffffff">Thank You</h2><br>
                    <p style="color: #ffffff">Thank you for your interest. The ISD Athletics team will contact you shortly.
                        For more information
                        Call: 058 2858239
                        Email: sports@isddubai.com
                        isddubai.com
                    </p>
                    @endif
                </section>
            </div>
        </article>
    </div>
</section>--}}
<!-- Event snippet for Submit lead form - Popup conversion page -->
<script>
gtag('event', 'conversion', {'send_to': 'AW-432844860/UOTeCPTP0vMBELzgss4B'});
</script>
@endsection