@extends('layouts.app')

@section('pageClass', 'sportsdetailpg inner-page')

@section('title', 'Athletics Academy in Dubai')

@section('description', 'Athletics Academy in Dubai')

@section('keywords', 'dubai athletics, athletics dubai, athletics in dubai, athletics academy dubai, athletics lessons dubai, athletics classes dubai, athletics club dubai, athletics coaching in dubai')

@section('content')



<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/athletics-inner-banner.webp'); background-position: center;">



	<div class="inner-content --running">

		<div class="vertical-heading">

			<h2 class="title">Athletics</h2>

		</div>

	</div>



	<div class="content-section --scroll-page">

		<article>

			<h2 class="title --big">ISD Athletics Academy Term 3</h2>

			



			<div class="bottom-section">

				

				<p class="subtitle">

					Get back on Track with ISD Athletics Academy at Dubai Sports City

				</p>



				<p class="mobile-content">

					Registration is now open for Term 3 with a 20% offer on term and kit fees. <br><br> ISD Athletics offers Dubai’s best athletics development recreational and professional programs for boys and girls ages 5 to 18, of all abilities and levels. <br><br>Developed by three-time Team GB Olympian gold medalist, and European and Commonwealth champion Andy Turner and his team, ISD Athletics Academy offers a wide variety of disciplines, including sprints, middle and long distance runs, hurdles, shot put, discus, javelin, long jump, triple jump, and high jump as well as strength and conditioning to ensure each athlete gets a well-rounded development.

				</p>



				<p class="introductive-title --athletics">

					Term 3 - 11th April to 15th June 2021 (Eid Week off)

				</p>



				<div class="table-section --athletics">


					<p class="table-title">

						Schedule:

					</p>



					<div class="table-responsive">

						<table class="table --schedule-table athletics">

							<tr>

								<th>Days</th>

								<th>Time</th>

								<th>Age Group</th>

							</tr>



							<!-- Time -->

							<tr>

								<td>

									Sunday to Thursday

								</td>

								<td>

									5pm to 6pm

								</td>



								<td>

									5-8 years old

								</td>

							</tr>



							<tr>

								<td>

									Sunday to Thursday

								</td>

								<td>

									6pm to 7pm

								</td>



								<td>

									9-12 years old

								</td>

							</tr>



							<tr>

								<td>

									Sunday to Thursday

								</td>

								<td>

									7pm to 8pm

								</td>



								<td>

									13-18 years old

								</td>

							</tr>



							<tr>

								<td>

									Saturday

								</td>

								<td>

									9pm to 10pm

								</td>



								<td>

									5-8 years old

								</td>

							</tr>



							<tr>

								<td>

									Saturday

								</td>

								<td>

									10pm to 11pm

								</td>



								<td>

									9-12 &amp; 13-18 years old

								</td>

							</tr>



						</table>

					</div>

				</div>



			</div>



			<div id="registration" class="inner-regiration-box">

				<h2 class="title">Register now for your FREE trial!</h2>

				@include('includes.academy_form')

			</div>

		

		</article>

	</div>



</section>



@endsection