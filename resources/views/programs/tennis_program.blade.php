@extends('layouts.app')

@section('pageClass', 'sportsdetailpg inner-page')

@section('title', 'Tennis Academy in Dubai')

@section('description', 'Tennis Academy in Dubai')

@section('keywords', 'dubai tennis, tennis dubai, tennis in dubai, tennis academy dubai, tennis lessons dubai, tennis classes dubai, tennis club dubai, tennis coaching in dubai')

@section('content')



<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/tennis-banner.webp'); background-position: center;">



	<div class="inner-content --tennis">

		<div class="vertical-heading">

			<h2 class="title">Tennis</h2>

		</div>

	</div>



	<div class="content-section --scroll-page">

		<article>

			<h2 class="title --big">ISD Tennis Academy </h2>

			<div class="bottom-section">
				<p class="mobile-content">
					ISD Tennis Academy offers best in-class personalised programs to boys and girls ages 4 to 18, of all abilities. Delivered by a team of highly certified and experienced European coaches, ISD Tennis academy programs are focused on the development of technical skills and leadership qualities, on and off the court. Players enjoy a fun, immersive experience at ISD’s premier facilities with brand new, state-of-the-art outdoor tennis courts.
					<br><br>
					ISD Tennis Academy offers 60 minute group lessons of 4 or 6 players. Private and semi private lessons are also available for youth and adults.
				</p>
			</div>



			<?php /* <div class="bottom-section">

				

				<p class="subtitle">

					Join ISD Tennis Academy and be part of Dubai’s tennis community at ISD, Dubai Sports City!

				</p>


				<p class="introductive-title --tennis">

					Term 3:  11 April - 1 July 2021 (12 weeks) <br> Saturday - Wednesday

				</p>



				<div class="table-section --tennis">





					<div class="table-responsive">

						<table class="table --schedule-table tennis">

							<tr>

								<th>Fees</th>

								<th>1 Lesson</th>

								<th>5 Lessons</th>

								<th>10 Lessons</th>

							</tr>



							<tr>

								<td>

									Private Lesson <br> 45 Minutes

								</td>

								<td>

									325 AED

								</td>

								<td>

									1300 AED

								</td>

								<td>

									2500 AED

								</td>

							</tr>



							<tr>

								<td>

									Semi Private Lesson <br> 60 Minutes <br> (price per player)

								</td>

								<td>

									275 AED

								</td>

								<td>

									1100 AED

								</td>

								<td>

									2200 AED

								</td>

							</tr>



						</table>

					</div>





					<p class="table-title">

						Academy Training Program

					</p>



					<div class="table-responsive">

						<table class="table --schedule-table tennis">

							<tr>

								<th>Term 3</th>

								<th>1X per Week</th>

								<th>2X per Week</th>

								<th>3X per Week</th>

							</tr>



							<!-- Time -->

							<tr>

								<td>

									"Tennis Group of 4 <br> 60 Mins Contact Time"

								</td>

								<td>

									1740 AED

								</td>



								<td>

									3000 AED

								</td>



								<td>

									4175 AED

								</td>

							</tr>



							<tr>

								<td>

									"Tennis Group of 6 <br> 60 Mins Contact Time"

								</td>

								<td>

									1440 AED

								</td>



								<td>

									2640 AED

								</td>



								<td>

									3455 AED

								</td>

							</tr>



							<tr>

								<td>

									"Tennis Minis <br> 60 Minutes"

								</td>

								<td>

									1320 AED

								</td>



								<td>

									2400 AED

								</td>



								<td>

									3365 AED

								</td>

							</tr>



						</table>

					</div>

				</div>



				<p class="maindesc fc-white">

					Fees are subject to VAT 5%

				</p>



			</div> */ ?>


			<div id="registration" class="inner-regiration-box">
				<h2 class="title">Register now for your FREE trial!</h2>
				@include('includes.academy_form')
			</div>
		

		</article>

	</div>

<!-- @include('includes.academy_form') -->


</section>



@endsection