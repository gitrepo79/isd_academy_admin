@extends('layouts.app')
@section('pageClass', 'athleticpg inner-page')
@section('title', 'Venue Registration')
@section('content')
<section class="inner-page-section" style="background-image: url('/assets-web/images/banners/default-banner.webp');">
    <div class="inner-content --registration">
    </div>
    <div class="vertical-heading">
        <h1 class="title">Venue Rental</h1>
    </div>
    <div class="content-section">
        <article class="container-wrapper">
            <div class="row">
                <section class="col-xl-5 offset-xl-4 col-lg-6 offset-lg-3">
                    
                    <div class="box --registration-box">
                        {!! Form::open(['route' => 'form.submission', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form --registration-form",'id'=>"contactForm"]) !!}
                        <div class="control-group{{ $errors->has('sport_id') ? ' has-error' : '' }}">
                            {!! Form::select('sport_id',$sports,null, ['class'=>'form-field','required' => 'required','placeholder' => 'Select Sports']) !!}
                            @if ($errors->has('sport_id'))
                            <span class="form-error" role="alert">
                                <strong>{{ $errors->first('sport_id') }}</strong>
                            </span>
                            @endif
                        </div>
                        
                        <div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {!! Form::text('name', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Full Name']) !!}
                            @if ($errors->has('name'))
                            <span class="form-error">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            
                            {!! Form::text('email', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Email Address']) !!}
                            @if ($errors->has('email'))
                            <span class="form-error">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="control-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            
                            {!! Form::text('mobile', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Mobile Number']) !!}
                            @if ($errors->has('mobile'))
                            <span class="form-error">
                                <strong>{{ $errors->first('mobile') }}</strong>
                            </span>
                            @endif
                        </div>
                        
                        <div class="control-group{{ $errors->has('date_of_requirement') ? ' has-error' : '' }}">
                            {!! Form::text('date_of_requirement', null, ['class'=>'form-field','required' => 'required','id'=>'date_of_requirement', 'placeholder'=>'Booking Date','autocomplete'=>'off']) !!}
                            @if ($errors->has('date_of_requirement'))
                            <span class="form-error">
                                <strong>{{ $errors->first('date_of_requirement') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="control-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                       
                                <div class="g-recaptcha" data-sitekey="{{env('NOCAPTCHA_SITEKEY')}}"></div>
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                            
                        </div>
                       
                        <?php /*<div class="control-group{{ $errors->has('time_of_requirement') ? ' has-error' : '' }}">
                            {!! Form::time('time_of_requirement', null, ['class'=>'form-field','required' => 'required','id'=>'time_of_requirement']) !!}
                            @if ($errors->has('time_of_requirement'))
                            <span class="form-error">
                                <strong>{{ $errors->first('time_of_requirement') }}</strong>
                            </span>
                            @endif
                        </div>*/?>

                    
                        
                        <div class="control-group">
                            <button type="submit" class="btn --btn-primary">
                            {{ __('Submit') }}
                            </button>
                        </div>
                        <?php /*<p class="maindesc fc-white mb-0">Already have an account? <a href="/login" class="fc-white td-underline">Login</a></p> */?>
                        <input type="hidden" name="form_type_id" value="1">
                        {!! Form::close() !!}
                    </div>
                </section>
            </div>
        </article>
    </div>
</section>

@endsection