<?php



use Illuminate\Support\Facades\Route;



/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

*/



Auth::routes();

Route::get('/register-api', function()
{
    include public_path().'/register_api.php';
});


Route::get('/redirectToUserHome', 'Auth\LoginController@redirectToUserHome');

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('pages.about');

Route::get('/terms-conditions', 'HomeController@terms')->name('pages.terms');
Route::get('/privacy-policy', 'HomeController@policy')->name('pages.policy');

Route::get('/footlab', 'HomeController@footlab')->name('pages.footlab');
Route::get('/thankyou/{formType}', 'HomeController@thankYou')->name('form.submission.thankyou');


// Sports
Route::get('/isd-academy', 'HomeController@isdAcademy')->name('pages.isd_academy');

// Route::get('/football', 'HomeController@football')->name('pages.football');
Route::get('/football-venuehire', 'HomeController@footballVenue')->name('pages.football_venue');

Route::get('/tennisacademy', 'HomeController@tennis')->name('pages.tennis');

Route::get('/tennis', function() {
    return redirect('/tennisacademy');
});

Route::get('/tennisacademycamps', 'HomeController@tennisCamp')->name('pages.tennis_camp');

Route::get('/tennisacademy-details', 'HomeController@tennisDetails')->name('pages.tennis_details');

Route::get('/tennisacademy-fees', 'HomeController@tennisFee')->name('pages.tennis_fees');

Route::get('/tennisacademy-schedule', 'HomeController@tennisSchedule')->name('pages.tennis_schedule');

Route::get('/tennis-venuehire', 'HomeController@tennisVenue')->name('pages.tennis_venue');

Route::get('/tennisadults', 'HomeController@tennisAdults')->name('pages.tennis_adults');


Route::get('/dubai30x30', 'HomeController@dubaiFitnessChallenge')->name('pages.dubai_fitness_challenge');

// Route::get('/programs/tennis-program', 'HomeController@tennisProgram')->name('programs.tennis_program');

Route::get('/rugby', 'HomeController@rugby')->name('pages.rugby');

Route::get('/padel', 'HomeController@padel')->name('pages.padel');

Route::get('/athleticsacademy', 'HomeController@athletics')->name('pages.athletics');

Route::get('/athleticsacademycamps', 'HomeController@athleticsCamp')->name('pages.athletics_camp');

// Route::get('/athletics', 'HomeController@athletics')->name('pages.athletics');

// Route::get('/programs/athletics-program', 'HomeController@athleticsProgram')->name('programs.athletics_program');

// Route::get('/isd-athletics', 'HomeController@isdAthletics')->name('pages.isd_athletics');

// Route::get('/athletics-classic-meets', 'HomeController@athleticsClassicMeets')->name('pages.athletics.classic.meets');

// Route::get('/athleticsacademy-details', 'HomeController@athleticsDetails')->name('pages.athletics_details');

Route::get('/athletics', function() {
    return redirect('/athleticsacademy');
});

Route::get('/athleticsacademy-fees', 'HomeController@athleticsFee')->name('pages.athletics_fees');

Route::get('/athleticsacademy-schedule', 'HomeController@athleticsSchedule')->name('pages.athletics_schedule');

Route::get('/athletics-venuehire', 'HomeController@athleticsVenue')->name('pages.athletics_venue');
Route::get('/eupepsia', 'HomeController@eupepsia')->name('pages.eupepsia');



Route::get('/venueHire', 'HomeController@venueHire')->name('venu.hire');

Route::get('/academyRegistration', 'HomeController@academyRegistration')->name('academy.registration');

Route::post('/formSubmission', 'HomeController@registration')->name('form.submission');

Route::post('/inquiryformSubmission', 'HomeController@inquiryform')->name('inquiryform.submission');


Route::name('admin.')->prefix('admin/')->middleware(['admin'])->group(function(){

 Route::get('/', 'SignUpController@index')->name('dashboard');
 Route::get('/searchSignUps', 'SignUpController@searchSignUps')->name('signUps.search');
 Route::get('/{signUp}/edit', 'SignUpController@edit')->name('signUps.edit');
 Route::put('/{signUp}/update', 'SignUpController@update')->name('signUps.update');
 Route::delete('/{signUp}', 'SignUpController@destroy')->name('signUps.delete');
 Route::post('/exportData', 'SignUpController@exportData')->name('export.data');
 Route::post('/export-all-data', 'SignUpController@exportALLData')->name('export.all.data');

 Route::group(['prefix' => 'athletics-meet'], function () {
 Route::get('/', 'SignUpController@indexAthleticsMeet')->name('athletics.meet.index');
  Route::delete('/{signUp}', 'SignUpController@destroy')->name('signUps.delete');
 Route::get('/search', 'SignUpController@searchAthleticsMeet')->name(
 	'athletics.meet.search');

 });

 Route::group(['prefix' => 'tennis'], function () {
 Route::get('/', 'SignUpController@indexTennis')->name('tennis.index');
 Route::post('/export/sign-ups','SignUpController@exportData')->name('tennis.export.data');
 Route::delete('/{signUp}', 'SignUpController@destroy')->name('signUps.delete');
 Route::get('/searchSignUps', 'SignUpController@searchTennisSignUps')->name('tennis.search');

 });

  Route::group(['prefix' => 'pop-up'], function () {
  Route::get('/', 'SignUpController@indexPopUp')->name('popup.index');
 // Route::post('/export/sign-ups','SignUpController@exportData')->name('tennis.export.data');
  Route::delete('/{signUp}', 'SignUpController@destroy')->name('popup.delete');
 // Route::get('/searchSignUps', 'SignUpController@searchTennisSignUps')->name('tennis.search');

 });

  Route::group(['prefix' => 'inquiry'], function () {
 Route::get('/', 'InquiriesController@index')->name('inquiry.index');
  Route::get('/{id}/edit', 'InquiriesController@edit')->name('inquiry.edit');
 Route::put('/{id}/update', 'InquiriesController@update')->name('inquiry.update');
//  Route::post('/export/sign-ups','SignUpController@exportData')->name('tennis.export.data');
 Route::delete('/{id}', 'InquiriesController@destroy')->name('inquiry.delete');
//  Route::get('/searchSignUps', 'SignUpController@searchTennisSignUps')->name('tennis.search');
Route::get('/searchInquiries', 'InquiriesController@searchInquiries')->name('inquiry.search');

 });


 // mr wick
 Route::group(['prefix' => 'contact-leads'], function () {
    Route::get('/', 'ContactLeadsController@index')->name('lead.index');
//      Route::get('/{id}/edit', 'InquiriesController@edit')->name('inquiry.edit');
//     Route::put('/{id}/update', 'InquiriesController@update')->name('inquiry.update');
//    //  Route::post('/export/sign-ups','SignUpController@exportData')->name('tennis.export.data');
//     Route::delete('/{id}', 'InquiriesController@destroy')->name('inquiry.delete');
//    //  Route::get('/searchSignUps', 'SignUpController@searchTennisSignUps')->name('tennis.search');
//    Route::get('/searchInquiries', 'InquiriesController@searchInquiries')->name('inquiry.search');

    });


 });



